ActiveAdmin.register Building do
	menu false
	menu label: 'Building Data'

	preserve_default_filters!
	remove_filter :project_id
	filter :project, as: :select, collection: ->{Project.order("id DESC").collect {|project| [project.project_name, project.id] }}
	filter :building_data_entries, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [building_data_entry.building_name, building_data_entry.id] }}

	index do
    selectable_column
    id_column
    column "Project" do |building|
	  	link_to building.project_id, admin_project_path(building.project_id)
	  end
    column :number_of_buildings
    column :have_floorplan
    column :updated_at
    column :created_at
    actions
  end
	
	actions :index, :show
end
 
