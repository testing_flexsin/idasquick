ActiveAdmin.register BuildingDataEntry do
  menu false
	menu label: 'Building Data Entry - DB6'
	
	preserve_default_filters!
	filter :building, as: :select, collection: ->{Building.order("id DESC").collect {|building| [building.id] }}
	filter :link_budget, as: :select, collection: ->{LinkBudget.order("id DESC").collect {|link_budget| [link_budget.id] }}
	filter :system_dimension, as: :select, collection: ->{SystemDimension.order("id DESC").collect {|system_dimension| [system_dimension.id] }}
	filter :design_information, as: :select, collection: ->{DesignInformation.order("id DESC").collect {|design_information| [design_information.id] }}
	filter :part_quantity_informations, as: :select, collection: ->{PartQuantityInformation.order("id DESC").collect {|part_quantity_information| [part_quantity_information.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |building_data_entry|
	  	link_to building_data_entry.building.project_id, admin_project_path(building_data_entry.building.project_id)
	  end
	  column :building do |building_data_entry|
	  	link_to building_data_entry.building_id, admin_building_path(building_data_entry.building_id)
	  end
	  column :building_name
	  column :building_number
	  column :floor_number
	  column :number_of_floors
	  column :area_this_floor
	  column "Total Area of Building", :total_area_building
	  column "Antenna Coverage Radius (ft)", :antenna_coverage_radius
	  column "Estimated Path Loss (dB)", :estimated_path_loss
	  column "Frequency (GHz)", :frequency
	  column "Building Slope (PE)", :building_slope
	  column :created_at
	  column :updated_at
	  actions
	end
	
	show do
    attributes_table do
      row :id
		  row "Project" do |building_data_entry|
		  	link_to "Project ##{building_data_entry.building.project_id}", admin_project_path(building_data_entry.building.project_id)
		  end
      row :building_id
		  row :building_name
		  row :building_number
		  row :floor_number
		  row :number_of_floors
		  row :area_this_floor
		  row "Total Area of Building" do |building_data_entry|
		  	building_data_entry.total_area_building
		  end
		  row "Antenna Coverage Radius (ft)" do |building_data_entry|
		  	building_data_entry.antenna_coverage_radius
		  end
		  row "Estimated Path Loss (db)" do |building_data_entry|
		  	building_data_entry.estimated_path_loss
		  end
		  row "Frequency (GHZ)" do |building_data_entry|
		  	building_data_entry.frequency
		  end
		  row "Building Slope (PE)" do |building_data_entry|
		  	building_data_entry.building_slope
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
