ActiveAdmin.register BuildingEnvironment do
	menu false
	menu label: 'Building Environment - DB8'
	
	permit_params :environment, :building_indoor_factor

	preserve_default_filters!
	filter :building_data_entries, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [building_data_entry.building_name, building_data_entry.id] }}
end
