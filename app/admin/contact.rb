ActiveAdmin.register Contact do
	menu false
	
	permit_params :first_name, :last_name, :email, :contact_number, :message

	index do
	  selectable_column
	  id_column
	  column :first_name
	  column :last_name
	  column :email
	  column :contact_number
	  column :message
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row :first_name
		  row :last_name
		  row :email
		  row :contact_number
		  row :message
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
		f.inputs do
	    f.input :first_name
	    f.input :last_name
	    f.input :email
	    f.input :contact_number
	    f.input :message
		end
    f.actions
  end
end
