ActiveAdmin.register Credit do
	menu false
	menu label: 'Credits'

	permit_params :product_number, :credits, :amount

	index do
	  selectable_column
	  id_column
	  column "Product Number / SKU", :product_number
	  column :credits
	  column :amount
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
		  row "Product Number / SKU" do |credit|
		  	credit.product_number
		  end
	    row :credits
	    row :amount
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :product_number, label: "Product Number / SKU"
	    f.input :credits
	    f.input :amount
		end
    f.actions
  end
end
