ActiveAdmin.register FiberMaterialQuantity do
  menu false
  menu label: 'Fiber Material Quantity - DB13a'

  permit_params :report_material_list_id, :quantity

  preserve_default_filters!
  remove_filter :report_material_list_id
  filter :report_material_list, as: :select, collection: ->{ReportMaterialList.order("id ASC").collect {|report_material_list| ["#{report_material_list.material_category.try(:category)} - #{report_material_list.description}", report_material_list.id] }}, label: "Category - Description"

  index do
    selectable_column
    id_column
    column "Category" do |fiber_material_quantity|
      fiber_material_quantity.report_material_list.material_category.try(:category)
    end
    column "Description" do |fiber_material_quantity|
      fiber_material_quantity.report_material_list.try(:description)
    end
    column :quantity
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Category" do |fiber_material_quantity|
        fiber_material_quantity.report_material_list.material_category.try(:category)
      end
      row "Description" do |fiber_material_quantity|
        fiber_material_quantity.report_material_list.try(:description)
      end
      row :quantity
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :description, as: :select, collection: MaterialCategory.order("id ASC").collect {|material_category| [material_category.category, material_category.id] }, label: "Category", input_html: { onchange: remote_request(:post, :change_report_material_list, {material_category_id: "$('#fiber_material_quantity_description').val()"}, :fiber_material_quantity_report_material_list_id) }
      f.input :report_material_list_id, as: :select, collection: [], label: "Description"
      f.input :quantity
    end
    f.actions
  end

  collection_action :change_report_material_list, method: :post do
    @report_material_lists = ReportMaterialList.where(material_category_id: params[:material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
  end

  member_action :change_report_material_list, method: :post do
    @report_material_lists = ReportMaterialList.where(material_category_id: params[:material_category_id]).order("id ASC")
    render text: view_context.options_from_collection_for_select(@report_material_lists, :id, :description)
  end
end
