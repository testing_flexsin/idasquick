ActiveAdmin.register HighestFrequencyBand do
	menu false
	menu label: 'Highest Frequency Band - DB10'
	
	permit_params :frequency

	preserve_default_filters!
	remove_filter :cable_losses
	remove_filter :tunnel_cable_c_losses
	remove_filter :tunnel_cable_i_losses
end
