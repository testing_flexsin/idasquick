ActiveAdmin.register LinkBudget do
	menu false
	menu label: 'Link Budget‐DL - DB18'

	preserve_default_filters!
	remove_filter :building_data_entry_id
  filter :building_data_entry, as: :select, collection: ->{BuildingDataEntry.order("id DESC").collect {|building_data_entry| [building_data_entry.building_name, building_data_entry.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |link_budget|
	  	link_to link_budget.building_data_entry.building.project_id, admin_project_path(link_budget.building_data_entry.building.project_id)
	  end
	  column "Building" do |link_budget|
	  	link_to link_budget.building_data_entry.building_id, admin_building_path(link_budget.building_data_entry.building_id)
	  end
	  column "Building Data Entry" do |link_budget|
	  	link_to link_budget.building_data_entry_id, admin_building_data_entry_path(link_budget.building_data_entry_id)
	  end  
	  column "Building Number" do |link_budget|
	  	link_budget.building_data_entry.try(:building_number)
	  end
	  column "Floor Number" do |link_budget|
	  	link_budget.building_data_entry.try(:floor_number)
	  end
	  column "Antenna Number", :antenna_number
	  column "BDA Number", :bda_number
	  column "BDA Channel Power (dBm)", :channel_power
	  column "(L) Cable Length (ft)", :cable_length
	  column "Cable Loss/ft (dB) @ (F) GHz", :cable_loss
	  column "(S) Splitter Loss (dB)", :splitter_loss
	  column "Jumper Loss (dB)", :jumper_loss
	  column "Connector Loss (dB)", :connector_loss
	  column "Antenna Gain (average) (F) MHz(dBd)", :antenna_gain
	  column "Antenna ERP (dBm)", :antenna_erp
	  column "DL Margin (dB)", :dl_margin
	  column "Allowed PL (dB)", :allowed_pl
	  column "RSSI at Portable (dBm)", :rssi_at_portable
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Project" do |link_budget|
	  		link_to "Project ##{link_budget.building_data_entry.building.project_id}", admin_project_path(link_budget.building_data_entry.building.project_id)
	  	end
	    row "Building" do |link_budget|
	  	 link_to "Building ##{link_budget.building_data_entry.building_id}", admin_building_path(link_budget.building_data_entry.building_id)
	  	end
      row :building_data_entry_id
		  row "Building Number" do |link_budget|
		  	link_budget.building_data_entry.try(:building_number)
		  end
		  row "Floor Number" do |link_budget|
		  	link_budget.building_data_entry.try(:floor_number)
		  end
		  row "Antenna Number" do |link_budget|
		  	link_budget.antenna_number
		  end
		  row "BDA Number" do |link_budget|
		  	link_budget.bda_number
		  end
		  row "BDA Channel Power (DBM)" do |link_budget|
		  	link_budget.channel_power
		  end
		  row "(L) Cable Length (ft)" do |link_budget|
		  	link_budget.cable_length
		  end
		  row "Cable Loss/ft (DB) @ (F) GHZ" do |link_budget|
		  	link_budget.cable_loss
		  end
		  row "(S) Splitter Loss (DB)" do |link_budget|
		  	link_budget.splitter_loss
		  end
		  row "Jumper Loss (DB)" do |link_budget|
		  	link_budget.jumper_loss
		  end
		  row "Connector Loss (DB)" do |link_budget|
		  	link_budget.connector_loss
		  end
		  row "Antenna Gain (average) (F) MHZ(DBD)" do |link_budget|
		  	link_budget.antenna_gain
		  end
		  row "Antenna ERP (DBM)" do |link_budget|
		  	link_budget.antenna_erp
		  end
		  row "DL Margin (DB)" do |link_budget|
		  	link_budget.dl_margin
		  end
		  row "Allowed PL (DB)" do |link_budget|
		  	link_budget.allowed_pl
		  end
		  row "RSSI at Portable (DBM)" do |link_budget|
		  	link_budget.rssi_at_portable
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
