ActiveAdmin.register LinkBudgetAdmin do
	menu false
	menu label: 'Link Budget Admin‐DL - DB18'
	
	permit_params :cable_length, :antenna_gain, :dl_margin
	
	index do
	  selectable_column
	  id_column
	  column "(L) Cable Length (ft)", :cable_length
	  column "Antenna Gain (average) (F) MHz(dBd)", :antenna_gain
	  column "DL Margin (dB)", :dl_margin
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "(L) Cable Length (ft)" do |link_budget_admin|
      	link_budget_admin.cable_length
      end
      row "Antenna Gain (average) (F) MHz(dBd)" do |link_budget_admin|
      	link_budget_admin.antenna_gain
      end
      row "DL Margin (dB)" do |link_budget_admin|
      	link_budget_admin.dl_margin
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :cable_length, label: "(L) Cable Length (ft)"
	    f.input :antenna_gain, label: "Antenna Gain (average) (F) MHz(dBd)"
	    f.input :dl_margin, label: "DL Margin (dB)"
		end
    f.actions
  end
end
