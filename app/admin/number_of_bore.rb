ActiveAdmin.register NumberOfBore do
  menu false
  menu label: 'Number of Bores - DB3b'

  permit_params :bores
end
