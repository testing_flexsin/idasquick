ActiveAdmin.register NumberOfRadioRoom do
	menu false
  menu label: 'Number of Radio Rooms - DB6a'

  permit_params :radio_rooms
end
