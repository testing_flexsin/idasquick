ActiveAdmin.register Plan do
	menu false
	menu label: 'Plans'

	permit_params :name, :amount, :credits, :usage_credits_one_time, :product_number, :yearly, :quote_data_storage

	index do
	  selectable_column
	  id_column
	  column :name
	  column "Type", :yearly do |plan|
	  	if plan.yearly?
	  		"Yearly"
	  	else
	  		"Monthly"
	  	end
	  end
	  column :amount
	  column :credits
	  column "Usage Credits", :usage_credits_one_time do |plan|
	  	case plan.usage_credits_one_time
	  	when true
	  		"One Time"
	  	when false
	  		"Per Month"
	  	end
	  end
	  column "Product Number / SKU", :product_number
	  column "Long term Quote Data Storage (Months)", :quote_data_storage
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	    row :name
		  row "Type", :yearly do |plan|
		  	if plan.yearly?
		  		"Yearly"
		  	else
		  		"Monthly"
		  	end
		  end
		  row :amount
		  row :credits
	  	row "Usage Credits", :usage_credits_one_time do |plan|
		  	case plan.usage_credits_one_time
		  	when true
		  		"One Time"
		  	when false
		  		"Per Month"
		  	end
		  end
		  row "Product Number / SKU" do |plan|
		  	plan.product_number
		  end
		  row "Long term Quote Data Storage (Months)" do |plan|
		  	plan.quote_data_storage
		  end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :name
	    f.input :yearly, as: :select, collection: [["Yearly", true], ["Monthly", false]], label: "Type"
	    f.input :amount
	    f.input :credits
	    f.input :usage_credits_one_time, as: :select, collection: [["One Time", true], ["Per Month", false]], label: "Usage Credits"
	    f.input :product_number, label: "Product Number / SKU"
	    f.input :quote_data_storage, label: "Long term Quote Data Storage (Months)"
		end
    f.actions
  end
end
