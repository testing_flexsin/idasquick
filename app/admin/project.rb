ActiveAdmin.register Project do
	menu false
	menu label: 'Project Details - DB1'

	preserve_default_filters!
	remove_filter :product_frequency_channel_id, :marketing_id, :building_id
	filter :product_frequency_channel, as: :select, collection: ->{ProductFrequencyChannel.order("id ASC").collect {|pfc| [ pfc.number_of_channels, pfc.id ] }}
	filter :marketing, as: :select, collection: ->{Marketing.order("id ASC").collect {|marketing| [ marketing.vendor_name, marketing.id ] }}
	filter :building , as: :select, collection: ->{Building.order("id DESC").collect {|building| [building.id] }}

	index do
	  selectable_column
	  id_column
	  column :user_name
	  column :project_name
	  column :company
	  column "Building Name", :name
	  column :facility_option
	  column :number_of_services
	  column :highest_frequency_band
	  column "Product Frequency Channel" do |project|
	  	project.product_frequency_channel.try(:number_of_channels)
	  end
	  column :system_feed_method
	  column :system_architecture
	  column "Marketing" do |project|
	  	if project.marketing.present?
	  		link_to	project.marketing.try(:vendor_name), admin_marketing_path(project.marketing)
	  	elsif project.building_system?
	  		"Customer has no vendor preference"
	  	else
	  		""
	  	end
	  end
	  column :created_at
	  column :updated_at
	  actions
	  column "" do |project|
	  	link_to "Download", report_export_project_path(project) if project.report?
	  end
	end

	show title: proc{|project| "Project ##{project.id}"} do
    attributes_table do
      row :id
	  	row :user_name
		  row :project_name
		  row :company
		  row "Building Name" do |project|
		  	project.name
		  end
		  row :facility_option
		  row :number_of_services
		  row :highest_frequency_band
		  row :product_frequency_channel_id
		  row :system_feed_method
		  row :system_architecture
		  row "Marketing" do |project|
		  	if project.marketing.present?
		  		link_to	project.marketing.try(:vendor_name), admin_marketing_path(project.marketing)
		  	elsif project.building_system?
		  		"Customer has no vendor preference"
		  	else
		  		""
		  	end
		  end
		  row "Download" do |project|
		  	link_to "Download ##{project.id}", report_export_project_path(project) if project.report?
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
