ActiveAdmin.register Region do
	menu false
	
	permit_params :name

	preserve_default_filters!
	remove_filter :marketings
end
