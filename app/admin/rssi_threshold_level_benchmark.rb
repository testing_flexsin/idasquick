ActiveAdmin.register RssiThresholdLevelBenchmark do
	menu false
  menu label: 'RSSI Threshold Level Benchmark DB-21'

	permit_params :threshold_level, :default_selected

	index do
    selectable_column
    id_column
    column "RSSI Threshold Level (dBm)", :threshold_level
    column :default_selected
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "RSSI Threshold Level (dBm)" do |rssi_threshold_level_benchmark|
        rssi_threshold_level_benchmark.threshold_level
      end
      row :default_selected
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  form do |f|
    f.inputs do
      f.input :threshold_level, label: "RSSI Threshold Level (dBm)"
      f.input :default_selected, as: :select, collection: [['Yes', true], ['No', false]]
    end
    f.actions
  end
end
