ActiveAdmin.register ServicesBreakout do
	menu false
  menu label: 'Services Breakout - DB30'
  
	permit_params :number_of_services, :number_of_bdas_donor, :number_of_bdas_das, :services_breakout_in_each_bda_box

	index do
	  selectable_column
	  id_column
    column "Number of Services", :number_of_services
	  column "Number of BDAs (Connector to DONOR) Qty", :number_of_bdas_donor
    column "Number of BDAs (Connector to DAS) Qty", :number_of_bdas_das
	  column "Services Breakout in each BDA Box", :services_breakout_in_each_bda_box
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Number of Services" do |services_breakout|
        services_breakout.number_of_services
      end
      row "Number of BDAs (Connector to DONOR) Qty" do |services_breakout|
        services_breakout.number_of_bdas_donor
      end
      row "Number of BDAs (Connector to DAS) Qty" do |services_breakout|
        services_breakout.number_of_bdas_das
      end
      row "Services Breakout in each BDA Box" do |services_breakout|
      	services_breakout.services_breakout_in_each_bda_box
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :number_of_services, label: "Number of Services"
      f.input :number_of_bdas_donor, label: "Number of BDAs (Connector to DONOR) Qty"
      f.input :number_of_bdas_das, label: "Number of BDAs (Connector to DAS) Qty"
	    f.input :services_breakout_in_each_bda_box, label: "Services Breakout in each BDA Box"
		end
    f.actions
  end
end
