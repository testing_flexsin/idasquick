ActiveAdmin.register SplitterMatrix do
  menu false
  menu label: 'Splitter Matrix - DB16'
  
	permit_params :antennas, :number_of_bdas, :product_type_2w, :product_type_3w, :product_type_4w, :product_type_6w, :product_type_8w, :dc, :hc, :config_per_bda

	index do
	  selectable_column
	  id_column
    column "Antennas Per BDA", :antennas
	  column "Matrix Type", :matrix_type
    column "Number of BDAs Required", :number_of_bdas
	  column "2W", :product_type_2w
	  column "3W", :product_type_3w
	  column "4W", :product_type_4w
    column "6W", :product_type_6w
    column "8W", :product_type_8w
    column "DC", :dc
	  column "HC", :hc
	  column "Config per BDA", :config_per_bda
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Antennas Per BDA" do |splitter_matrix|
        splitter_matrix.antennas
      end
      row "Matrix Type" do |splitter_matrix|
        splitter_matrix.matrix_type
      end
      row "Number of BDAS Required" do |splitter_matrix|
        splitter_matrix.number_of_bdas
      end
      row "2W" do |splitter_matrix|
      	splitter_matrix.product_type_2w
      end
      row "3W" do |splitter_matrix|
      	splitter_matrix.product_type_3w
      end
      row "4W" do |splitter_matrix|
      	splitter_matrix.product_type_4w
      end
      row "6W" do |splitter_matrix|
        splitter_matrix.product_type_6w
      end
      row "8W" do |splitter_matrix|
        splitter_matrix.product_type_8w
      end
      row "DC" do |splitter_matrix|
      	splitter_matrix.dc
      end
      row "HC" do |splitter_matrix|
        splitter_matrix.hc
      end
      row "Config per BDA" do |splitter_matrix|
      	splitter_matrix.config_per_bda
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :antennas, label: "Antennas Per BDA"
      f.input :number_of_bdas, label: "Number of BDAs Required"
	    f.input :product_type_2w, label: "2W"
	    f.input :product_type_3w, label: "3W"
	    f.input :product_type_4w, label: "4W"
      f.input :product_type_6w, label: "6W"
      f.input :product_type_8w, label: "8W"
      f.input :dc, label: "DC"
	    f.input :hc, label: "HC"
	    f.input :config_per_bda, label: "Config per BDA"
		end
    f.actions
  end
end
