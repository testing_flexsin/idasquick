ActiveAdmin.register Subscription do
	menu false

	index do
	  selectable_column
	  id_column
	  column "User Id" do |subscription|
	  	link_to subscription.user_id, admin_user_path(subscription.user_id)
	  end
	  column "Transaction Id", :transaction_id
	  column :transaction_gateway
	  column :transaction_receipt
	  column :transaction_type
	  column :transaction_date
	  column :transaction_amount
	  column :transaction_currency
	  column :seller_paypal_email
	  column :customer_paypal_email
	  column :paypal_payer_id
	  column :product_number
	  column :product_name
	  column :product_type
	  column :product_amount
	  column :payment_type
	  column :recurring_id
	  column :next_rebill
	  column :recurring_times
	  column :recurring_status
	  column :payment_number
	  column :seller_id
	  column :seller_email
	  column :customer_firstname
	  column :customer_lastname
	  column :customer_address
	  column :customer_state
	  column :customer_city
	  column :customer_country
	  column :customer_email
	  column :credits
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
	  	row "User Id" do |subscription|
		  	link_to subscription.user_id, admin_user_path(subscription.user_id)
		  end
		  row "Transaction Id" do |subscription|
		  	subscription.transaction_id
		  end
	  	row :transaction_gateway
		  row :transaction_receipt
		  row :transaction_type
		  row :transaction_date
		  row :transaction_amount
		  row :transaction_currency
		  row :seller_paypal_email
		  row :customer_paypal_email
	  	row :paypal_payer_id
		  row :product_number
		  row :product_name
		  row :product_type
		  row :product_amount
		  row :payment_type
		  row :recurring_id
		  row :next_rebill
	  	row :recurring_times
		  row :recurring_status
		  row :payment_number
		  row :seller_id
		  row :seller_email
		  row :customer_firstname
		  row :customer_lastname
		  row :customer_address
		  row :customer_state
		  row :customer_city
		  row :customer_country
		  row :customer_email
		  row :credits
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
