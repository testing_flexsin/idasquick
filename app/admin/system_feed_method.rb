ActiveAdmin.register SystemFeedMethod do
	menu false
	menu label: 'System Feed Method - DB12'
	
	permit_params :feed_method, :feed_assigned, :default_selected
	
	index do
	  selectable_column
	  id_column
	  column :feed_method
	  column :feed_assigned
	  column :default_selected
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	  	row :feed_method
	    row :feed_assigned
		  row :default_selected
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :feed_method
	    f.input :feed_assigned, as: :select, collection: [['Yes', true], ['No', false]]
	    f.input :default_selected, as: :select, collection: [['Yes', true], ['No', false]]
		end
    f.actions
  end
end
