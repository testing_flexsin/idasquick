ActiveAdmin.register TimerSetting do
	menu false
	
	permit_params :plan_name, :report_generate_timer, :report_download_timer
	
	index do
	  selectable_column
	  id_column
	  column :plan_name
	  column "Report Generate Timer (in seconds)", :report_generate_timer
	  column "Report Download Timer (in seconds)", :report_download_timer
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
	  	row :plan_name
	    row "Report Generate Timer (in seconds)" do |timer_setting|
	    	timer_setting.report_generate_timer
	    end
	    row "Report Download Timer (in seconds)" do |timer_setting|
	    	timer_setting.report_download_timer
	    end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :plan_name, as: :select, collection: [['Basic', 'Basic'], ['Standard', 'Standard'], ['Professional', 'Professional']]
	    f.input :report_generate_timer, label: "Report Generate Timer (in seconds)"
	    f.input :report_download_timer, label: "Report Download Timer (in seconds)"
		end
    f.actions
  end
end
