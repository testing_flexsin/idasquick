ActiveAdmin.register TrialPeriod do
	menu false
	menu label: 'Trial Period'

	permit_params :period

	index do
	  selectable_column
	  id_column
	  column "Trial Period (In Days)", :period
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
	    row :id
		  row "Trial Period (In Days)" do |trial_period|
		  	trial_period.period
		  end
	    row :created_at
	    row :updated_at
  	end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :period, label: "Trial Period (In Days)"
		end
    f.actions
  end
end
