ActiveAdmin.register Tunnel do
  menu false
	menu label: 'Tunnel Data'

	preserve_default_filters!
	remove_filter :tunnel_project_id
	filter :tunnel_project, as: :select, collection: ->{TunnelProject.order("id DESC").collect {|project| [project.project_name, project.id] }}
	filter :tunnel_data_entries, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}

	index do
    selectable_column
    id_column
    column "Project" do |tunnel|
	  	link_to tunnel.tunnel_project_id, admin_tunnel_project_path(tunnel.tunnel_project_id)
	  end
    column :number_of_tunnels
    column :have_floorplan
    column :updated_at
    column :created_at
    actions
  end
	
	actions :index, :show
end
 