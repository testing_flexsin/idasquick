ActiveAdmin.register TunnelDataEntry do
 	menu false
	menu label: 'Tunnel Data Entry - DB6'
	
	preserve_default_filters!
	filter :tunnel, as: :select, collection: ->{Tunnel.order("id DESC").collect {|tunnel| [tunnel.id] }}
	filter :tunnel_link_budget, as: :select, collection: ->{TunnelLinkBudget.order("id DESC").collect {|tunnel_link_budget| [tunnel_link_budget.id] }}
	filter :tunnel_signal_level_benchmark, as: :select, collection: ->{TunnelSignalLevelBenchmark.order("id DESC").collect {|tunnel_signal_level_benchmark| [tunnel_signal_level_benchmark.id] }}
	filter :tunnel_design_information, as: :select, collection: ->{TunnelDesignInformation.order("id DESC").collect {|tunnel_design_information| [tunnel_design_information.id] }}
	filter :tunnel_part_quantity_informations, as: :select, collection: ->{TunnelPartQuantityInformation.order("id DESC").collect {|tunnel_part_quantity_information| [tunnel_part_quantity_information.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |tunnel_data_entry|
	  	link_to tunnel_data_entry.tunnel.tunnel_project_id, admin_tunnel_project_path(tunnel_data_entry.tunnel.tunnel_project_id)
	  end
	  column "Tunnel" do |tunnel_data_entry|
	  	link_to tunnel_data_entry.tunnel_id, admin_tunnel_path(tunnel_data_entry.tunnel_id)
	  end
	  column :tunnel_name
	  column :tunnel_number
		column :number_of_bores
	  column :number_of_segments
	  column :bore_number
	  column :segment_number
	  column :tunnel_length
	  column :tunnel_width
	  column :tunnel_height
	  column :environment_designator
	  column "Number of Radio Rooms or Station Locations", :number_of_radio_rooms
	  column :created_at
	  column :updated_at
	  actions
	end
	
	show do
    attributes_table do
      row :id
		  row "Project" do |tunnel_data_entry|
		  	link_to "Project ##{tunnel_data_entry.tunnel.tunnel_project_id}", admin_tunnel_project_path(tunnel_data_entry.tunnel.tunnel_project_id)
		  end
      row :tunnel_id
		  row :tunnel_name
		  row :tunnel_number
		  row :number_of_bores
		  row :number_of_segments
		  row :bore_number
		  row :segment_number
		  row :tunnel_length
		  row :tunnel_width
		  row :tunnel_height
		  row :environment_designator
		  row "Number of Radio Rooms or Station Locations" do |tunnel_data_entry|
		  	tunnel_data_entry.number_of_radio_rooms
		  end
		  row :created_at
		  row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
