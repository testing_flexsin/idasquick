ActiveAdmin.register TunnelDesignInformation do
	menu false
  menu label: 'Design Information ‐ Tunnel - DB23'

  preserve_default_filters!
  remove_filter :tunnel_data_entry_id
  filter :tunnel_data_entry, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}

  index do
    selectable_column
    id_column
    column "Project" do |design_information|
      link_to design_information.tunnel_data_entry.tunnel.tunnel_project_id, admin_tunnel_project_path(design_information.tunnel_data_entry.tunnel.tunnel_project_id)
    end
    column "Tunnel" do |design_information|
      link_to design_information.tunnel_data_entry.tunnel_id, admin_tunnel_path(design_information.tunnel_data_entry.tunnel_id)
    end
    column "Tunnel Data Entry" do |design_information|
      link_to design_information.tunnel_data_entry_id, admin_tunnel_data_entry_path(design_information.tunnel_data_entry_id)
    end
    column :tunnel_length
    column "Number of Radio Rooms or Stations", :number_of_radio_rooms
    column "Average Distance Between Radio Rooms or Stations", :average_distance_radio_rooms
    column :number_of_cable_runs
    column :cable_type do |design_information|
      case design_information.cable_type
      when "cable_loss_1_by_2"
        "1/2 in"
      when "cable_loss_7_by_8"
        "7/8 in"
      when "cable_loss_11_by_4"
        "11/4 in"
      when "cable_loss_15_by_8"
        "15/8 in"
      else
        design_information.cable_type
      end
    end
    column :total_cable_length
    column "BDA Product Category", :bda_product_category
    column "Number of BDAs", :number_of_bdas
    column :communication_type
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :id
      row "Project" do |design_information|
     		link_to "Project ##{design_information.tunnel_data_entry.tunnel.tunnel_project_id}", admin_tunnel_project_path(design_information.tunnel_data_entry.tunnel.tunnel_project_id)
    	end
      row "Tunnel" do |design_information|
    		link_to "Tunnel ##{design_information.tunnel_data_entry.tunnel_id}", admin_tunnel_path(design_information.tunnel_data_entry.tunnel_id)
      end
      row :tunnel_data_entry_id
      row :tunnel_length
      row "Number of Radio Rooms or Stations" do |design_information|
        design_information.number_of_radio_rooms
      end
      row "Average Distance Between Radio Rooms or Stations" do |design_information|
        design_information.average_distance_radio_rooms
      end
    	row :number_of_cable_runs
      row :cable_type do |design_information|
        case design_information.cable_type
        when "cable_loss_1_by_2"
          "1/2 in"
        when "cable_loss_7_by_8"
          "7/8 in"
        when "cable_loss_11_by_4"
          "11/4 in"
        when "cable_loss_15_by_8"
          "15/8 in"
        else
          design_information.cable_type
        end
      end
      row :total_cable_length
      row "BDA Product Category" do |design_information|
        design_information.bda_product_category
      end
      row "Number of BDAS" do |design_information|
        design_information.number_of_bdas
      end
      row :communication_type
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

  actions :index, :show
end
