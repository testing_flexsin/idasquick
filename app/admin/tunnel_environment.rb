ActiveAdmin.register TunnelEnvironment do
	menu false
	menu label: 'Tunnel Environment - DB8'

	permit_params :environment, :tunnel_indoor_factor

	preserve_default_filters!
  filter :tunnel_data_entries, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}
end
