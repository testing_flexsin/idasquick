ActiveAdmin.register TunnelLinkBudget do
	menu false
	menu label: 'Tunnel Link Budget‐DL - DB18'

	preserve_default_filters!
	remove_filter :tunnel_data_entry_id
  filter :tunnel_data_entry, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}

	index do
	  selectable_column
	  id_column
	  column "Project" do |link_budget|
	  	link_to link_budget.tunnel_data_entry.tunnel.tunnel_project_id, admin_tunnel_project_path(link_budget.tunnel_data_entry.tunnel.tunnel_project_id)
	  end
	  column "Tunnel" do |link_budget|
	  	link_to link_budget.tunnel_data_entry.tunnel_id, admin_tunnel_path(link_budget.tunnel_data_entry.tunnel_id)
	  end
	  column "Tunnel Data Entry" do |link_budget|
	  	link_to link_budget.tunnel_data_entry_id, admin_tunnel_data_entry_path(link_budget.tunnel_data_entry_id)
	  end
	  column "Channel Power", :channel_power
	  column "Frequency (MHz)", :frequency
	  column "Distribution Loss", :distribution_loss
	  column "Radiax Length (m)", :radiax_length
	  column "Radiating Cable Insertion Loss (dB/100m)", :radiating_cable_insertion_loss
	  column "Radiating Cable Coupling Loss (@2m)", :radiating_cable_coupling_loss
	  column "Distance From Cable (m)", :distance_from_cable
	  column "Indoor Margin", :indoor_margin
	  column "Indoor Factor", :indoor_factor
	  column "Path Loss From Cable (dB)", :path_loss_from_cable
	  column "Signal Level @ Mobile (dBm)", :signal_level_at_mobile
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Project" do |link_budget|
	  		link_to "Project ##{link_budget.tunnel_data_entry.tunnel.tunnel_project_id}", admin_project_path(link_budget.tunnel_data_entry.tunnel.tunnel_project_id)
	  	end
	    row "Tunnel" do |link_budget|
	  	 link_to "Tunnel ##{link_budget.tunnel_data_entry.tunnel_id}", admin_tunnel_path(link_budget.tunnel_data_entry.tunnel_id)
	  	end
      row :tunnel_data_entry_id
		  row "Channel Power" do |link_budget|
		   link_budget.channel_power
		 	end
		  row "Frequency (MHZ)" do |link_budget| 
		  	link_budget.frequency
			end
		  row "Distribution Loss" do |link_budget| 
		  	link_budget.distribution_loss
			end
		  row "Radiax Length (m)" do |link_budget|
		   link_budget.radiax_length
		 	end
		  row "Radiating Cable Insertion Loss (db/100m)" do |link_budget|
		  	link_budget.radiating_cable_insertion_loss
			end
		  row "Radiating Cable Coupling Loss (@2m)" do |link_budget|
		  	link_budget.radiating_cable_coupling_loss
			end
		  row "Distance From Cable (m)" do |link_budget| link_budget.distance_from_cable
			end
		  row "Indoor Margin" do |link_budget|
		   link_budget.indoor_margin
		   end
		  row "Indoor Factor" do |link_budget| 
		  	link_budget.indoor_factor
		  	end
		  row "Path Loss From Cable (db)" do |link_budget|
		   link_budget.path_loss_from_cable
		   end
		  row "Signal Level @ Mobile (dbm)" do |link_budget|
		   link_budget.signal_level_at_mobile
		   end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
