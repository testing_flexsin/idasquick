ActiveAdmin.register TunnelLinkBudgetAdmin do
	menu false
	menu label: 'Link Budget Admin‐DL - DB18'

	permit_params :indoor_margin
end
