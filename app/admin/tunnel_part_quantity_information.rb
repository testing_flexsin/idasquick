ActiveAdmin.register TunnelPartQuantityInformation do
	menu false
  menu label: 'Part Quantity Information ‐ Tunnel - DB25'

  preserve_default_filters!
	remove_filter :tunnel_data_entry_id, :tunnel_report_material_list_id
	filter :tunnel_data_entry, as: :select, collection: ->{TunnelDataEntry.order("id DESC").collect {|tunnel_data_entry| [tunnel_data_entry.tunnel_name, tunnel_data_entry.id] }}
	filter :tunnel_report_material_list, as: :select, collection: ->{TunnelReportMaterialList.order("id ASC").collect {|tunnel_report_material_list| ["#{tunnel_report_material_list.tunnel_material_category.try(:category)} - #{tunnel_report_material_list.description}", tunnel_report_material_list.id] }}, label: "Category - Description"

	index do
	  selectable_column
	  id_column
    column "Project" do |part_quantity_information|
      link_to part_quantity_information.tunnel_data_entry.tunnel.tunnel_project_id, admin_tunnel_project_path(part_quantity_information.tunnel_data_entry.tunnel.tunnel_project_id)
    end
    column "Tunnel" do |part_quantity_information|
      link_to part_quantity_information.tunnel_data_entry.tunnel_id, admin_tunnel_path(part_quantity_information.tunnel_data_entry.tunnel_id)
    end
    column "Tunnel Data Entry" do |part_quantity_information|
      link_to part_quantity_information.tunnel_data_entry_id, admin_tunnel_data_entry_path(part_quantity_information.tunnel_data_entry_id)
    end
	  column "Tunnel Name" do |part_quantity_information|
	  	part_quantity_information.tunnel_data_entry.tunnel_name
	  end
	  column "Tunnel Number" do |part_quantity_information|
	  	part_quantity_information.tunnel_data_entry.tunnel_number
	  end
    column "Bore Number" do |part_quantity_information|
    	part_quantity_information.tunnel_data_entry.bore_number
    end
    column "Segment Number" do |part_quantity_information|
    	part_quantity_information.tunnel_data_entry.segment_number
    end
	  column "Category" do |part_quantity_information|
      part_quantity_information.tunnel_report_material_list.try(:tunnel_material_category).try(:category)
    end
    column "Description" do |part_quantity_information|
      part_quantity_information.tunnel_report_material_list.try(:description)
    end
	  column :quantity
	  column :created_at
	  column :updated_at
    actions 
	end

	show do
    attributes_table do
      row :id
      row "Project" do |part_quantity_information|
     		link_to "Project ##{tunnel_part_quantity_information.tunnel_data_entry.tunnel.tunnel_project_id}", admin_tunnel_project_path(tunnel_part_quantity_information.tunnel_data_entry.tunnel.tunnel_project_id)
    	end
      row "Tunnel" do |part_quantity_information|
    		link_to "Tunnel ##{part_quantity_information.tunnel_data_entry.tunnel_id}", admin_tunnel_path(tunnel_part_quantity_information.tunnel_data_entry.tunnel_id)
      end
      row :tunnel_data_entry_id
		  row "Tunnel Name" do |part_quantity_information|
		  	part_quantity_information.tunnel_data_entry.tunnel_name
		  end
		  row "Tunnel Number" do |part_quantity_information|
		  	part_quantity_information.tunnel_data_entry.tunnel_number
		  end
	    row "Bore Number" do |part_quantity_information|
	    	part_quantity_information.tunnel_data_entry.bore_number
	    end
	    row "Segment Number" do |part_quantity_information|
	    	part_quantity_information.tunnel_data_entry.segment_number
	    end
		  row "Category" do |part_quantity_information|
	      part_quantity_information.tunnel_report_material_list.try(:tunnel_material_category).try(:category)
	    end
	    row "Description" do |part_quantity_information|
	      part_quantity_information.tunnel_report_material_list.try(:description)
	    end
	  	row :quantity
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	actions :index, :show
end
