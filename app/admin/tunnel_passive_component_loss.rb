ActiveAdmin.register TunnelPassiveComponentLoss do
	menu false
	menu label: 'Passive Component Loss - DB19'
	
	permit_params :jumper_loss, :connector_loss, :way2_splitter_loss, :way3_splitter_loss, :way4_splitter_loss, :way6_splitter_loss, :way8_splitter_loss, :directional_coupler_loss, :hybrid_coupler_loss, :quantity_of_splitters, :type_of_splitter

	index do
	  selectable_column
	  id_column
	  column :jumper_loss
	  column :connector_loss
	  column "2Way Splitter Loss", :way2_splitter_loss
	  column "3Way Splitter Loss", :way3_splitter_loss
	  column "4Way Splitter Loss", :way4_splitter_loss
	  column "6Way Splitter Loss", :way6_splitter_loss
	  column "8Way Splitter Loss", :way8_splitter_loss
	  column "DC Insertion Loss", :directional_coupler_loss
	  column :hybrid_coupler_loss
	  column :quantity_of_splitters
	  column :type_of_splitter do |tunnel_passive_component_loss|
	  	case tunnel_passive_component_loss.type_of_splitter
	  	when "way2_splitter_loss"
	  		"2Way Splitter Loss"
	  	when "way3_splitter_loss"
	  		"3Way Splitter Loss"
	  	when "way4_splitter_loss"
	  		"4Way Splitter Loss"
	  	when "way6_splitter_loss"
	  		"6Way Splitter Loss"
	  	when "way8_splitter_loss"
	  		"8Way Splitter Loss"
	  	when "directional_coupler_loss"
	  		"DC Insertion Loss"
	  	when "hybrid_coupler_loss"
	  		"Hybrid Coupler Loss"
	  	else
	  		tunnel_passive_component_loss.type_of_splitter
	  	end
	  end
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row :jumper_loss
      row :connector_loss
      row "2Way Splitter Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.way2_splitter_loss
      end
      row "3Way Splitter Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.way3_splitter_loss
      end
      row "4Way Splitter Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.way4_splitter_loss
      end
      row "6Way Splitter Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.way6_splitter_loss
      end
      row "8Way Splitter Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.way8_splitter_loss
      end
      row "DC Insertion Loss" do |tunnel_passive_component_loss|
      	tunnel_passive_component_loss.directional_coupler_loss
      end
      row :hybrid_coupler_loss
		  row :quantity_of_splitters
		  row :type_of_splitter do |tunnel_passive_component_loss|
		  	case tunnel_passive_component_loss.type_of_splitter
		  	when "way2_splitter_loss"
		  		"2Way Splitter Loss"
		  	when "way3_splitter_loss"
		  		"3Way Splitter Loss"
		  	when "way4_splitter_loss"
		  		"4Way Splitter Loss"
		  	when "way6_splitter_loss"
		  		"6Way Splitter Loss"
		  	when "way8_splitter_loss"
		  		"8Way Splitter Loss"
		  	when "directional_coupler_loss"
		  		"DC Insertion Loss"
		  	when "hybrid_coupler_loss"
		  		"Hybrid Coupler Loss"
		  	else
		  		tunnel_passive_component_loss.type_of_splitter
		  	end
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :jumper_loss
	    f.input :connector_loss
	    f.input :way2_splitter_loss, label: "2Way Splitter Loss"
	    f.input :way3_splitter_loss, label: "3Way Splitter Loss"
	    f.input :way4_splitter_loss, label: "4Way Splitter Loss"
	    f.input :way6_splitter_loss, label: "6Way Splitter Loss"
	    f.input :way8_splitter_loss, label: "8Way Splitter Loss"
	    f.input :directional_coupler_loss, label: "DC Insertion Loss"
	    f.input :hybrid_coupler_loss
	    f.input :quantity_of_splitters
	    f.input :type_of_splitter, as: :select, collection: [["2Way Splitter Loss", "way2_splitter_loss"], ["3Way Splitter Loss", "way3_splitter_loss"], ["4Way Splitter Loss", "way4_splitter_loss"], ["6Way Splitter Loss", "way6_splitter_loss"], ["8Way Splitter Loss", "way8_splitter_loss"], ["DC Insertion Loss", "directional_coupler_loss"], ["Hybrid Coupler Loss", "hybrid_coupler_loss"]]
		end
    f.actions
  end
end
