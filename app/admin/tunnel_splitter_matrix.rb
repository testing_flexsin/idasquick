ActiveAdmin.register TunnelSplitterMatrix do
  menu false
  menu label: 'Splitter Matrix - DB16'

	permit_params :number_of_bdas, :product_type_2w, :product_type_3w, :product_type_4w, :dc, :config_per_bda

	index do
	  selectable_column
	  id_column
    column "Number of BDAs", :number_of_bdas
	  column "Product Type 2W", :product_type_2w
	  column "Product Type 3W", :product_type_3w
	  column "Product Type 4W", :product_type_4w
	  column "DC", :dc
	  column "Config per BDA", :config_per_bda
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
      row "Number of BDAS" do |splitter_matrix|
        splitter_matrix.number_of_bdas
      end
      row "Product Type 2W" do |splitter_matrix|
      	splitter_matrix.product_type_2w
      end
      row "Product Type 3W" do |splitter_matrix|
      	splitter_matrix.product_type_3w
      end
      row "Product Type 4W" do |splitter_matrix|
      	splitter_matrix.product_type_4w
      end
      row "DC" do |splitter_matrix|
      	splitter_matrix.dc
      end
      row "Config per BDA" do |splitter_matrix|
      	splitter_matrix.config_per_bda
      end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
      f.input :number_of_bdas, label: "Number of BDAs"
	    f.input :product_type_2w, label: "Product Type 2W"
	    f.input :product_type_3w, label: "Product Type 3W"
	    f.input :product_type_4w, label: "Product Type 4W"
	    f.input :dc, label: "DC"
	    f.input :config_per_bda, label: "Config per BDA"
		end
    f.actions
  end
end
