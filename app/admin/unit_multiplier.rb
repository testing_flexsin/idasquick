ActiveAdmin.register UnitMultiplier do
	menu false
	menu label: 'Unit Multiplier'
	
	permit_params :multiplied_factor

	index do
	  selectable_column
	  id_column
	  column "Multiplied Factor", :multiplied_factor
	  column :created_at
	  column :updated_at
	  actions
	end

	show do
    attributes_table do
      row :id
		  row "Multiplied Factor" do |unit_multiplier|
		  	unit_multiplier.multiplied_factor
		  end
      row :created_at
      row :updated_at
    end
    active_admin_comments
  end

	form do |f|
		f.inputs do
	    f.input :multiplied_factor, label: "Multiplied Factor"
		end
    f.actions
  end
end
