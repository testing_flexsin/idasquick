//faq js
$(document).ready(function(){
  $(".faqCon strong").on("click", function(e){
    if($(this).parent().has("ul")) {
      e.preventDefault();
    }
    
    if(!$(this).hasClass("opens")) {
      // hide any open menus and remove all other classes
      $(".faqCon p").slideUp(350);
      $(".faqCon strong").removeClass("opens");
      
      // open our new menu and add the open class
      $(this).next("p").slideDown(350);
      $(this).addClass("opens");
    }
    
    else if($(this).hasClass("opens")) {
      $(this).removeClass("opens");
      $(this).next("p").slideUp(350);
    }
  });
});
//faq js

$(document).ready(function(){
	$('.select').change(function(){
		var a = $(this).find('option:selected').text();
		$(this).prev('.selectInput').val(a);
	});    

	/*if( $("body").height() > $(window).height()){$("footer").css({"position":"static"});}
	else{$("footer").css({"position":"absolute", "bottom":0});}*/

	set_footer();

  /* Function for Showing mobile Menu  */							 
	$(".mobMenu").click(function(){
		$(".navigation .col-sm-12  > ul").css("display","block");					 
		$("body").css("overflow","hidden");						 
		$(".overLayMunu").css("bottom", "0");
		setTimeout(function(){ $(".navigation .col-sm-12  > ul").css("display","block").css("top", "0");	}, 500);	
	});	
	/* Function for Hiding mobile Menu  */
	$(".closeMenu	").click(function(){
		$("body").css("overflow","auto");								  
		$(".navigation  .col-sm-12  > ul").css("top", "-100%");					
		setTimeout(function(){ $(".overLayMunu").css("bottom", "-100%");}, 500);	
		setTimeout(function(){ $(".navigation .col-sm-12  > ul").css("display","none");}, 500);	
	});	
	/* Function for Pop Up */	
	// $('.owl-carousel').owlCarousel({
	//   loop:true,
	//   items:3,
	//   margin:40,
	//   autoplay:true,
	//   nav:true,
	//   responsive:{
	// 		600:{
	// 		    items:3
	// 		},
	// 		320:{
	// 		    items:1
	// 		}
	//   }
	// });
});

$(window).resize(function(){
	set_footer();
});

function set_footer() {
	($("body").height() > $(window).height() ? $("footer").css({"position":"static"}) : $("footer").css({"position":"absolute", "bottom":0}));
}