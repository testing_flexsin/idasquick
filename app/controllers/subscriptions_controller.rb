class SubscriptionsController < ApplicationController
  ## Filters
  skip_before_action :trial_expired, :store_location, :get_new_user, :set_static_page

  def create
		subscription = Subscription.new
		if params["products"].present? && params["products"]["0"].present? && params["products"]["0"]["custom"].present?
			user = User.find_by_id(params["products"]["0"]["custom"])
			if user.present?
				subscription.user_id = user.id
			end
		end
		subscription.transaction_id = params["paypal_txn_id"]
		subscription.transaction_gateway = params["trans_gateway"]
		subscription.transaction_receipt = params["trans_receipt"]
		subscription.transaction_type = params["trans_type"]
		subscription.transaction_date = params["trans_date"].to_datetime
		subscription.transaction_amount = params["trans_amount"]
		subscription.transaction_currency = params["trans_currency"]
		subscription.seller_paypal_email = params["seller_paypal_email"]
		subscription.customer_paypal_email = params["cust_paypal_email"]
		subscription.paypal_payer_id = params["paypal_payer_id"]
		if params["products"].present? && params["products"]["0"].present?
			subscription.product_number = params["products"]["0"]["prod_number"]
			subscription.product_name = params["products"]["0"]["prod_name"]
			subscription.product_type = params["products"]["0"]["prod_type"]
			subscription.product_amount = params["products"]["0"]["amount"]
			subscription.payment_type = params["products"]["0"]["payment_type"]
			subscription.recurring_id = params["products"]["0"]["recurring_id"]
			if params["products"]["0"]["next_rebill"].present?
				subscription.next_rebill = params["products"]["0"]["next_rebill"].to_datetime
			end
			subscription.recurring_times = params["products"]["0"]["recurring_times"]
			subscription.recurring_status = params["products"]["0"]["recurring_status"]
			subscription.payment_number = params["products"]["0"]["payment_number"]
		end
		subscription.seller_id = params["seller_id"]
		subscription.seller_email = params["seller_email"]
		subscription.customer_firstname = params["cust_firstname"]
		subscription.customer_lastname = params["cust_lastname"]
		subscription.customer_address = params["cust_address"]
		subscription.customer_state = params["cust_state"]
		subscription.customer_city = params["cust_city"]
		subscription.customer_country = params["cust_country"]
		subscription.customer_email = params["cust_email"]
  	credit = Credit.where(product_number: subscription.product_number).last
  	if credit.present?
  		subscription.credits = credit.credits
  	end
		subscription.save

		user = subscription.user
		if user.present?
			user.update(credits: user.credits + subscription.credits)
		end

		redirect_to edit_user_registration_path
  end

  def plans
		subscription = Subscription.new
		subscription.transaction_id = params["paypal_txn_id"]
		subscription.transaction_gateway = params["trans_gateway"]
		subscription.transaction_receipt = params["trans_receipt"]
		subscription.transaction_type = params["trans_type"]
		subscription.transaction_date = params["trans_date"].to_datetime
		subscription.transaction_amount = params["trans_amount"]
		subscription.transaction_currency = params["trans_currency"]
		subscription.seller_paypal_email = params["seller_paypal_email"]
		subscription.customer_paypal_email = params["cust_paypal_email"]
		subscription.paypal_payer_id = params["paypal_payer_id"]
		if params["products"].present? && params["products"]["0"].present?
			subscription.product_number = params["products"]["0"]["prod_number"]
			subscription.product_name = params["products"]["0"]["prod_name"]
			subscription.product_type = params["products"]["0"]["prod_type"]
			subscription.product_amount = params["products"]["0"]["amount"]
			subscription.payment_type = params["products"]["0"]["payment_type"]
			subscription.recurring_id = params["products"]["0"]["recurring_id"]
			if params["products"]["0"]["next_rebill"].present?
				subscription.next_rebill = params["products"]["0"]["next_rebill"].to_datetime
			end
			subscription.recurring_times = params["products"]["0"]["recurring_times"]
			subscription.recurring_status = params["products"]["0"]["recurring_status"]
			subscription.payment_number = params["products"]["0"]["payment_number"]

			if params["products"]["0"]["custom"].present?
				user = User.find_by_id(params["products"]["0"]["custom"])
				if user.present?
					subscription.user_id = user.id
				end
			end
		end
		subscription.seller_id = params["seller_id"]
		subscription.seller_email = params["seller_email"]
		subscription.customer_firstname = params["cust_firstname"]
		subscription.customer_lastname = params["cust_lastname"]
		subscription.customer_address = params["cust_address"]
		subscription.customer_state = params["cust_state"]
		subscription.customer_city = params["cust_city"]
		subscription.customer_country = params["cust_country"]
		subscription.customer_email = params["cust_email"]
		plan = Plan.where(product_number: subscription.product_number).last
  	if plan.present?
  		subscription.credits = plan.credits
  	end
		saved = subscription.save

		user = subscription.user
		if saved && user.present?
		  if plan.present?
		  	if plan.name.downcase.include? "building"
		  		user.building_plan_id = plan.id
		  		user.building_last_transaction_date = subscription.transaction_date
		  		user.building_last_credits_date = subscription.transaction_date
		  	elsif plan.name.downcase.include? "tunnel"
		  		user.tunnel_plan_id = plan.id
		  		user.tunnel_last_transaction_date = subscription.transaction_date
		  		user.tunnel_last_credits_date = subscription.transaction_date
		  	end
			  unless user.active?
			  	user.active = true
			  end
		  end
	  	user.credits = user.credits + subscription.credits
			user.save
		end

		redirect_to edit_user_registration_path
  end
end
