class TunnelProjectsController < ApplicationController
  include ActionView::Helpers::NumberHelper
	## Filters
  before_action :authenticate_user!
  before_action :set_project, only: [:show, :edit, :update, :destroy_show, :destroy, :tunnel_entry, :tunnel, :tunnel_data_entry, :save_tunnel_data_entry, :project_tunnel_services, :update_tunnel_services, :project_tunnel_system, :update_tunnel_system, :project_tunnel_type, :update_tunnel_type, :disclaimer, :update_disclaimer, :report, :report_export, :show_generate_report, :show_download_report, :get_total_area_tunnel, :check_data_entries, :check_system_architecture]
  before_action :set_tunnel, only: [:tunnel_data_entry, :save_tunnel_data_entry, :check_system_architecture]
  before_action :set_page, only: [:index, :projects_pending, :destroy, :destroy_show]
  before_action :check_credits, only: [:new, :create]
  before_action :set_first_param, only: [:new, :create, :edit, :update, :destroy, :tunnel, :tunnel_entry, :project_tunnel_services, :update_tunnel_services, :project_tunnel_system, :update_tunnel_system, :project_tunnel_type, :update_tunnel_type, :report, :check_data_entries]
  before_action :set_facility_option, only: [:tunnel, :tunnel_entry, :new, :create, :edit, :update, :destroy, :project_tunnel_services, :update_tunnel_services, :project_tunnel_system, :update_tunnel_system, :project_tunnel_type, :update_tunnel_type, :disclaimer, :report, :check_data_entries]
  before_action :check_project_finished, only: [:edit, :tunnel_entry, :project_tunnel_services, :project_tunnel_system, :project_tunnel_type]
  skip_before_action :set_static_page, only: [:destroy_show, :save_tunnel_data_entry, :update_disclaimer, :show_generate_report, :get_floor_plan_message, :check_system_architecture]

  def index
    @completed = true
    @projects = TunnelProject.completed_projects(current_user.id, @page)
  end

  def projects_pending
    @completed = false
    @projects = TunnelProject.pending_projects(current_user.id, @page)
  end

  def new
    @project = current_user.tunnel_projects.new
    @project.user_name = current_user.user_name
  end

  def edit
  end

  def create
    @saved = false
    @authorized = true
    params[:tunnel_project][:step_completed] = 1
    facility_option = params[:tunnel_project][:facility_option]
    if facility_option.present? && (facility_option == "Building")
      if check_subscription(facility_option)
        @project = current_user.projects.new(project_params)
        if @project.save
          @facility = @project.facility_option rescue ""
          @saved = true
          @building = @project.build_building
        end
      else
        @authorized = false
      end
    elsif facility_option.present? && (facility_option == "Tunnel")
      if check_subscription(facility_option)
        @project = current_user.tunnel_projects.new(project_params)
        if @project.save
          @facility = @project.facility_option rescue ""
          @saved = true
          @tunnel = @project.build_tunnel
        end
      else
        @authorized = false
      end
    end
    if @saved && !current_user.admin?
      current_user.update_column(:credits, current_user.credits - 1)
    end
  end

  def update
    @saved = @project.update(project_params)
    if @saved
      if @project.tunnel.present?
        @tunnel = @project.tunnel
      else
        @tunnel = @project.build_tunnel
      end
    end
  end

  def destroy_show
    @discard = false
    if params[:discard].present?
      @discard = true
    end
  end

  def destroy
    @discard = false
    if params[:discard].present? && (params[:discard] == "true")
      @discard = true
    end
    @completed = true
    if @project.present?
      unless @project.report?
        @completed = false
      end
      @project.really_destroy!
    end
    if @completed
      @projects = TunnelProject.completed_projects(current_user.id, @page)
      set_page
      unless @projects.present?
        if @page.present? && (@page.to_i > 1)
          @page = (@page.to_i - 1).to_s
        else
          @page = 1
        end
        @projects = TunnelProject.completed_projects(current_user.id, @page)
      end
    else
      @projects = TunnelProject.pending_projects(current_user.id, @page)
      set_page
      unless @projects.present?
        if @page.present? && (@page.to_i > 1)
          @page = (@page.to_i - 1).to_s
        else
          @page = 1
        end
        @projects = TunnelProject.pending_projects(current_user.id, @page)
      end
    end
  end

  def tunnel_entry
    if @project.tunnel.present?
      @tunnel = @project.tunnel
    else
      @tunnel = @project.build_tunnel
    end
  end

  def tunnel
    @saved = false
    @tunnel = @project.tunnel
    if @tunnel.present?
      if @tunnel.update(tunnel_params)
        @saved = true
      end
    else
      @tunnel = @project.build_tunnel(tunnel_params)
      if @tunnel.save
        @saved = true
      end
    end
    if @saved && !@project.report?
      @project.update_column(:step_completed, 2)
    end
  end

  def tunnel_data_entry
    tunnel_last_data_entry = @tunnel.tunnel_data_entries.order("id ASC").last
    @tunnel_data_entry = @tunnel.tunnel_data_entries.new
    @tunnel_data_entry.tunnel_number = 1
    @tunnel_data_entry.bore_number = 1
    @tunnel_data_entry.segment_number = 1
    if tunnel_last_data_entry.present?
      if tunnel_last_data_entry.tunnel_number < @tunnel.number_of_tunnels
        @tunnel_data_entry.number_of_bores = tunnel_last_data_entry.number_of_bores
        @tunnel_data_entry.number_of_segments = tunnel_last_data_entry.number_of_segments
        @tunnel_data_entry.tunnel_name = tunnel_last_data_entry.tunnel_name
        if tunnel_last_data_entry.bore_number < tunnel_last_data_entry.number_of_bores
          if tunnel_last_data_entry.segment_number < tunnel_last_data_entry.number_of_segments
            @tunnel_data_entry.tunnel_number = tunnel_last_data_entry.tunnel_number
            @tunnel_data_entry.bore_number = tunnel_last_data_entry.bore_number
            @tunnel_data_entry.segment_number = tunnel_last_data_entry.segment_number + 1
          else
            @tunnel_data_entry.tunnel_number = tunnel_last_data_entry.tunnel_number
            @tunnel_data_entry.bore_number = tunnel_last_data_entry.bore_number + 1
          end
        else
          @tunnel_data_entry.tunnel_number = tunnel_last_data_entry.tunnel_number + 1
        end
      end
    end
  end

  def save_tunnel_data_entry
    tunnel_data_entry = @tunnel.tunnel_data_entries.where(tunnel_number: tunnel_data_entry_params[:tunnel_number], bore_number: tunnel_data_entry_params[:bore_number], segment_number: tunnel_data_entry_params[:segment_number]).last
    if tunnel_data_entry.present?
      tunnel_data_entry.destroy
    end
    params[:tunnel_data_entry][:environment_designator] = TunnelEnvironment.find_by_id(tunnel_data_entry_params[:tunnel_environment_id]).try(:tunnel_indoor_factor)
    @tunnel_data_entry = @tunnel.tunnel_data_entries.new(tunnel_data_entry_params)
    @saved = @tunnel_data_entry.save
  end

  def project_tunnel_services
  end

  def update_tunnel_services
    if params[:autoselect_channel].present?
      tunnel_product_frequency_channel_id = TunnelProductFrequencyChannel.where(default_selected: true).last.try(:id)
      params[:tunnel_project][:tunnel_product_frequency_channel_id] = tunnel_product_frequency_channel_id
    end
    unless @project.report?
      params[:tunnel_project][:step_completed] = 3
    end
    @saved = @project.update(tunnel_services_params)
    if @saved
      @system_feed_methods = SystemFeedMethod.order("id ASC")
      @system_architectures = SystemArchitecture.order("id ASC")
    end
  end

  def project_tunnel_system
    @system_feed_methods = SystemFeedMethod.order("id ASC")
    @system_architectures = SystemArchitecture.order("id ASC")
  end

  def update_tunnel_system
    if params[:tunnel_project][:system_feed_method].present? && (params[:tunnel_project][:system_feed_method] == "Auto Select")
      params[:tunnel_project][:system_feed_method] = SystemFeedMethod.where(default_selected: true).last.try(:feed_method)
    end
    if params[:tunnel_project][:system_architecture].present? && (params[:tunnel_project][:system_architecture] == "Auto Select")
      length = TunnelLength.last.length

      total_tunnel_bore_length = 0.0
      tunnel_data_entries = @project.tunnel.tunnel_data_entries
      tunnel_numbers = tunnel_data_entries.pluck(:tunnel_number).uniq.sort
      tunnel_numbers.each do |tn|
        bore_numbers = tunnel_data_entries.where(tunnel_number: tn).order("id ASC").pluck(:bore_number).uniq.sort
        tunnel_lengths = tunnel_data_entries.where(tunnel_number: tn, bore_number: bore_numbers).sum(:tunnel_length)
        total_tunnel_bore_length += tunnel_lengths
      end
      if total_tunnel_bore_length < length
        params[:tunnel_project][:system_architecture] = "Coax"
      elsif total_tunnel_bore_length >= length
        params[:tunnel_project][:system_architecture] = "Fiber and Coax"
      end
    end
    if params[:autoselect_rssi].present?
      expected_rssi_at_mobile = RssiThresholdLevelBenchmark.where(default_selected: true).last.try(:threshold_level)
      params[:tunnel_project][:expected_rssi_at_mobile] = expected_rssi_at_mobile
    end
    unless @project.report?
      params[:tunnel_project][:step_completed] = 4
    end
    @saved = @project.update(tunnel_system_params)
  end

  def project_tunnel_type
  end

  def update_tunnel_type
    if params[:autoselect_technology].present?
      technology_type = TechnologyType.where(default_selected: true).last
      params[:tunnel_project][:technology_type] = technology_type.try(:technology)
      params[:tunnel_project][:papr] = technology_type.try(:papr)
    elsif params[:tunnel_project][:technology_type].present?
      technology_type = TechnologyType.where(technology: params[:tunnel_project][:technology_type]).last
      params[:tunnel_project][:papr] = technology_type.try(:papr)
    end
    if params[:tunnel_project][:communication_type].present? && (params[:tunnel_project][:communication_type] == "Auto Select")
      params[:tunnel_project][:communication_type] = CommunicationType.where(default_selected: true).last.try(:communication)
    end
    unless @project.report?
      params[:tunnel_project][:step_completed] = 5
      params[:tunnel_project][:report] = true
    end
    @saved = @project.update(tunnel_type_params)
  end

  def disclaimer
    if @project.disclaimer_acknowledgement?
      redirect_to report_tunnel_project_path(@project, facility: @facility)
    end
  end

  def update_disclaimer
    @saved = false
    if params[:disclaimer_acknowledge].present? && (params[:disclaimer_acknowledge] == "true")
      @saved = @project.update_column(:disclaimer_acknowledgement, true)
    end
  end

  def report
    unless @project.disclaimer_acknowledgement?
      redirect_to disclaimer_tunnel_project_path(@project, facility: @facility)
    end
    @tunnel_plan = current_user.tunnel_plan
    if @tunnel_plan.present?
      tunnel_plan_name = @tunnel_plan.name.split(" ").first
      @timer_setting = TimerSetting.where(plan_name: tunnel_plan_name).last
    else
      @timer_setting = TimerSetting.order("id ASC").first
    end
  end

  def show_generate_report
    unless @project.visited_report?
      @project.update_column(:visited_report, true)
    end
  end

  def show_download_report
  end

  def report_export
    @tunnel_plan = current_user.tunnel_plan
    @images_path = "#{request.protocol}#{request.host_with_port}/assets"
    pdf = render_to_string  pdf: "project_report.pdf",
      layout: 'pdf_mode.html.erb',
      show_as_html: false,
      encoding: "UTF-8",
      template: 'tunnel_projects/report_export.html.erb',
      footer: { html: { template: 'shared/pdf_footer.html.erb' } }
    send_data pdf, filename: "project_report#{@project.id}.pdf", type: "application/pdf", disposition: "attachment"
  end

  def get_floor_plan_message
    @message = ""
    if params[:floor_plan].present?
      @message = TunnelHaveFloorPlan.where(operator: params[:floor_plan]).first.try(:operand)
    end
  end

  def check_data_entries
  end

  def check_system_architecture
    @selected = params[:system_architecture]
    if @selected.present?
      @system_architecture = ""
      @tunnels = 0
      @total_tunnels = 0
      total_tunnel_bore_length = 0.0
      length = TunnelLength.last.length
      tunnel_data_entries = @tunnel.tunnel_data_entries
      if tunnel_data_entries.present?
        @total_tunnels = tunnel_data_entries.pluck(:tunnel_number).uniq.sort
        @total_tunnels.each do |tn|
          bore_numbers = tunnel_data_entries.where(tunnel_number: tn).order("id ASC").pluck(:bore_number).uniq.sort
          tunnel_lengths = tunnel_data_entries.where(tunnel_number: tn, bore_number: bore_numbers).sum(:tunnel_length)
          total_tunnel_bore_length += tunnel_lengths
          if @selected == "Fiber and Coax"
            if tunnel_lengths < length
              @tunnels += 1
              @system_architecture = "Coax"
            end
          elsif @selected == "Coax"
            if tunnel_lengths >= length
              @tunnels += 1
              @system_architecture = "Fiber and Coax"
            end
          end
        end
      end
    end
  end

  private
    def load_projects
      @completed_projects = TunnelProject.completed_projects(current_user.id, params[:page])
      @pending_projects = TunnelProject.pending_projects(current_user.id, params[:page])
    end

    def set_project
      @project = TunnelProject.get_project(params[:id], current_user.id)
      unless @project.present?
        redirect_to root_path
      end
    end

    def check_project_finished
      if @project.tunnel_type?
        redirect_to report_tunnel_project_path(@project, facility: @facility)
      end      
    end

    def set_tunnel
      @tunnel = @project.tunnel
    end

    def set_first_param
      @first = 1 if params[:first].present?      
    end

    def set_facility_option
      @facility = params[:facility] rescue ""
    end

    def set_page
      @page = params[:page].present? ? params[:page] : "1"
    end

    def project_params
      params.require(:tunnel_project).permit(:user_name, :project_name, :company, :name, :facility_option, :step_completed)
    end

    def tunnel_params
      params.require(:tunnel).permit(:number_of_tunnels, :have_floorplan)
    end

    def tunnel_data_entry_params
      params.require(:tunnel_data_entry).permit(:tunnel_name, :tunnel_number, :number_of_bores, :number_of_segments, :bore_number, :segment_number, :tunnel_length, :tunnel_width, :tunnel_height, :environment_designator, :tunnel_environment_id, :number_of_radio_rooms)
    end

    def tunnel_services_params
      params.require(:tunnel_project).permit(:number_of_services, :highest_frequency_band, :tunnel_product_frequency_channel_id, :step_completed)
    end

    def tunnel_system_params
      params.require(:tunnel_project).permit(:system_feed_method, :system_architecture, :expected_rssi_at_mobile, :marketing_id, :step_completed)
    end

    def tunnel_type_params
      params.require(:tunnel_project).permit(:technology_type, :papr, :communication_type, :step_completed, :report)
    end
end
