class Users::SessionsController < Devise::SessionsController
  respond_to :html, :js
  skip_before_action :trial_expired, only: [:destroy]
end