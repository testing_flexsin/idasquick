class UsersController < ApplicationController
  ## Filters
  before_action :authenticate_user!, except: [:send_activation_email, :retrieve_password, :retrieve_forgotten_password, :check_username_unique, :check_email_unique]
  before_action :set_first_param, only: [:welcome, :dashboard, :quick_start, :quick_start_step1, :quick_start_step2, :quick_start_step3]
  before_action :set_all_params, only: [:quick_start_step1, :quick_start_step2, :quick_start_step3]
  skip_before_action :trial_expired, only: [:welcome, :dashboard, :send_activation_email, :retrieve_password, :retrieve_forgotten_password, :check_username_unique, :check_current_username_unique, :check_email_unique, :check_current_email_unique]
  skip_before_action :set_static_page, only: [:send_activation_email, :check_username_unique, :check_current_username_unique, :check_email_unique, :check_current_email_unique]

  def welcome
  end

  def dashboard
    @credits = params[:credits]
    @expired = params[:expired]
  end

  def quick_start
  end

  def quick_start_step1
  end

  def quick_start_step2
  end

  def quick_start_step3
  end

  def download
  end

  def credits
    @page = params[:page].present? ? params[:page] : "1"
    @subscriptions = current_user.subscriptions.paginate(page: @page).order("id DESC")
  end

  def send_activation_email
    user = User.where(email: params[:email]).first
    if user.present?
      user.send_confirmation_instructions
      message = "You will receive an email with instructions for how to confirm your email address in a few minutes."
    else
    	message = "User not found."
    end
    redirect_to new_user_session_path, notice: message
  end

  def retrieve_password
    @user = User.new
  end

  def retrieve_forgotten_password
    login = params[:user][:login] rescue ""
    unless login.present?
      login = ""
    end
    user = User.where(["lower(user_name) = :value OR lower(email) = :value", { value: login.downcase }]).first
    if user.present?
      password = Devise.friendly_token.first(8)
      user.update(password: password)
    	UserMailer.delay.retrieve_password(user, password)
      redirect_to new_user_session_path, notice: "You will receive an email with your password in a few minutes."
    else
      redirect_to retrieve_password_path, notice: "Login not found."
    end
  end

  def check_project_subscription
    @authorized = true
    facility = params[:facility]
    if remaining_days <= 0 && !current_user.admin?
      if facility.present?
        if facility == "Building" && !current_user.building_plan.present?
          @authorized = false
        elsif facility == "Tunnel" && !current_user.tunnel_plan.present?
          @authorized = false
        end
      else
        @authorized = false
      end
    end
  end

  def check_username_unique
    user_name = params[:user][:user_name] rescue ""
    if user_name.present?
      user = User.user_username(user_name)
      if user.present?
        render json: false
      else
        if( (user_name == "super") || (user_name == "super_admin") || (user_name == "admin") || (user_name == "user") || (user_name == "admin_user") || (user_name == "user_admin") )
          render json: false
        else
          render json: true
        end
      end
    else
      render json: true
    end
  end

  def check_current_username_unique
    user_name = params[:user][:user_name] rescue ""
    if user_name.present?
      user = User.user_username(user_name)
      if user.present? && user.id != current_user.id
        render json: false
      else
        if( (user_name == "super") || (user_name == "super_admin") || (user_name == "admin") || (user_name == "user") || (user_name == "admin_user") || (user_name == "user_admin") )
          render json: false
        else
          render json: true
        end
      end
    else
      render json: true
    end
  end

  def check_email_unique
    email = params[:user][:email] rescue ""
    if email.present?
      user = User.user_email(params[:user][:email])
      if user.present?
        render json: false
      else
        render json: true
      end
    else
      render json: true
    end
  end

  def check_current_email_unique
    email = params[:user][:email] rescue ""
    if email.present?
      user = User.user_email(params[:user][:email])
      if user.present? && user.id != current_user.id
        render json: false
      else
        render json: true
      end
    else
      render json: true
    end
  end

  protected
    def set_first_param
      @first = 1 if params[:first].present?
    end

    def set_all_params
      @first = params[:first]
      @project_id = params[:project_id]
      @facility = params[:facility]
    end
end
