module DeviseHelper
  def devise_error_messages!
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t('errors.messages.not_saved',
                      count: resource.errors.count,
                      resource: resource.class.model_name.human.downcase)

    html = <<-HTML
    <div class="alert alert-danger fade in margin-bottom-10">
      <a class="close position-inherit" data-dismiss="alert">x</a>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end
end