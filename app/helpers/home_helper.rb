module HomeHelper
	def building_yearly_plans(building_plan, static_page)
		message = "<td class='subs-type'>Annually</td>"
		building_plan_name = ""
		yearly = false
		if building_plan.present?
			building_plan_name = building_plan.name.split(" ").first
			yearly = building_plan.yearly
		end
		if building_plan_name == "Basic" && yearly
			message += "<td></td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_link}' class='signUp subscribe'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Standard" && yearly
			message += "<td> </td>
			<td> </td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Professional" && yearly
			message += "<td> </td>
			<td> </td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.building_basic_plan_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end

	def building_monthly_plans(building_plan, static_page)
		message = "<td class='subs-type'>Monthly</td>"
		building_plan_name = ""
		yearly = true
		if building_plan.present?
			building_plan_name = building_plan.name.split(" ").first
			yearly = building_plan.yearly
		end
		if building_plan_name == "Basic" && !yearly
			message += "<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_monthly_link}' class='signUp subscribe'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Standard" && !yearly
			message += "<td> </td>
			<td> </td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif building_plan_name == "Professional" && !yearly
			message += "<td> </td>
			<td> </td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.building_basic_plan_monthly_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.building_standard_plan_monthly_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end

	def tunnel_yearly_plans(tunnel_plan, static_page)
		message = "<td class='subs-type'>Annually</td>"
		tunnel_plan_name = ""
		yearly = false
		if tunnel_plan.present?
			tunnel_plan_name = tunnel_plan.name.split(" ").first
			yearly = tunnel_plan.yearly
		end
		if tunnel_plan_name == "Basic" && yearly
			message += "<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_link}' class='signUp subscribe'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Standard" && yearly
			message += "<td> </td>
			<td> </td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Professional" && yearly
			message += "<td> </td>
			<td> </td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.tunnel_basic_plan_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end

	def tunnel_monthly_plans(tunnel_plan, static_page)
		message = "<td class='subs-type'>Monthly</td>"
		tunnel_plan_name = ""
		yearly = true
		if tunnel_plan.present?
			tunnel_plan_name = tunnel_plan.name.split(" ").first
			yearly = tunnel_plan.yearly
		end
		if tunnel_plan_name == "Basic" && !yearly
			message += "<td> </td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_monthly_link}' class='signUp subscribe'>Upgrade <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Standard" && !yearly
			message += "<td> </td>
			<td> </td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		elsif tunnel_plan_name == "Professional" && !yearly
			message += "<td> </td>
			<td> </td>
			<td> </td>"
		else
			message += "<td><a href='javascript:;' data-href='#{static_page.tunnel_basic_plan_monthly_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='javascript:;' data-href='#{static_page.tunnel_standard_plan_monthly_link}' class='signUp subscribe w150'>Subscribe <img src='/assets/circleArror.png' alt=''></a></td>
			<td><a href='/contact_us' data-href='' class='signUp w170'>Contact Us <img src='/assets/circleArror.png' alt=''></a></td>"
		end
		message
	end
end
