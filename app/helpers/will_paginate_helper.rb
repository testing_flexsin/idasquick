module WillPaginateHelper
  class WillPaginateJSLinkRenderer < WillPaginate::ActionView::LinkRenderer
    def prepare(collection, options, template)
      options[:params] ||= {}
      options[:params]['_'] = nil
      super(collection, options, template)
    end

    protected
    def link(text, target, attributes = {})
      if target.is_a? Fixnum
        attributes[:rel] = rel_value(target)
        target = url(target)
      end

      @template.link_to(target, attributes.merge(remote: true)) do
        text.to_s.html_safe
      end
    end

    def page_number(page)
      unless page == current_page
        tag(:li, link(page, page, rel: rel_value(page)))
      else
        tag(:li, link(page, "javascript:;"), class: "active")
      end
    end

    def previous_or_next_page(page, text, classname)
      if page
        if text == "Next &#8594;"
          tag(:li, link('<span aria-hidden="true"><img src="/assets/rightpagi.png" alt=""></span>'.html_safe, page, aria: {label: "Next"}))
        elsif text == "&#8592; Previous"
          tag(:li, link('<span aria-hidden="true"><img src="/assets/leftpagi.png" alt=""></span>'.html_safe, page, aria: {label: "Previous"}))
        end
      else
        if text == "Next &#8594;"
          tag(:li, '<a aria-label="Next" href="javascript:;"><span aria-hidden="true"><img src="/assets/rightpagi.png" alt=""></span></a>'.html_safe, class: 'disabled')
        elsif text == "&#8592; Previous"
          tag(:li, '<a aria-label="Previous" href="javascript:;"><span aria-hidden="true"><img src="/assets/leftpagi.png" alt=""></span></a>'.html_safe, class: 'disabled')
        end
      end
    end

    def html_container(html)
      tag(:ul, html, container_attributes)
    end
  end

  def js_will_paginate(collection, options = {})
    will_paginate(collection, options.merge(renderer: WillPaginateHelper::WillPaginateJSLinkRenderer))
  end
end