class ProjectMailer < ApplicationMailer
	def email_admin(project)
		@project = project
		static_page = StaticPage.last
		if static_page.present? && static_page.admin_email.present?
			mail(to: static_page.admin_email, subject: "New Project")
		else
			mail(to: ENV["ADMIN_MAIL"], subject: "New Project")
		end
	end

	def storage_reminder(user, project_type, static_page)
		@user = user
		@project_type = project_type
		@static_page = static_page
		mail(to: user.email, subject: "Reminder for Project (#{project_type}) Storage")
	end
end
