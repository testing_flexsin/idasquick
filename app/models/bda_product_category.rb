class BdaProductCategory < ActiveRecord::Base
	## Relations
	has_many :product_frequency_channels, dependent: :destroy
	has_many :tunnel_product_frequency_channels, dependent: :destroy
	has_many :coverage_area_per_bdas, dependent: :destroy
	has_many :tunnel_coverage_distance_per_bdas, dependent: :destroy
	
	## Validations
	validates :category, presence: true
end
