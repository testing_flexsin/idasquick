class Building < ActiveRecord::Base
	## Relations
	belongs_to :project
	has_many :building_data_entries, dependent: :destroy

	## Validations
	validates :project_id, :number_of_buildings, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
