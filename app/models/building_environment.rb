class BuildingEnvironment < ActiveRecord::Base
	## Relations
	has_many :building_data_entries

	## Validations
	validates :environment, presence: true
	validates :building_indoor_factor, presence: true, numericality: true
end
