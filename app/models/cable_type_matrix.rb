class CableTypeMatrix < ActiveRecord::Base
	## Validations
	validates :radiating_cable_type, presence: true
end
