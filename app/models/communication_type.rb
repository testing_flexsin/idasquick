class CommunicationType < ActiveRecord::Base
	## Validations
	validates :communication, presence: true
end
