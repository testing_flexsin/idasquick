class Contact < ActiveRecord::Base
	validates :first_name, :last_name, :email, :contact_number, :message, presence: true
  validates :contact_number, length: { in: 8..15 }, numericality: { only_integer: true }
  validates :message, length: { minimum: 4 }
end
