class CoverageAreaPerBda < ActiveRecord::Base
	## Relations
	belongs_to :bda_product_category

	## Validations
	validates :bda_product_category_id, presence: true, numericality: { only_integer: true }
	validates :coverage_area, presence: true, numericality: true
end
