class Credit < ActiveRecord::Base
	## Validations
	validates :product_number, presence: true
	validates :credits, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :amount, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
