class DesignInformation < ActiveRecord::Base
	## Relations
	belongs_to :building_data_entry

	## Validations
	validates :building_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :design_frequency, :coverage_radius, :building_total_coverage_area, :number_of_antennas, :horizontal_cable, :number_of_bda, :total_number_of_antennas, :number_of_floors, :distance_each_floor, :vertical_cable, :coverage_area_per_bda, numericality: true, allow_nil: true
end
