class DesignInformationAdmin < ActiveRecord::Base
	## Validations
	validates :distance_between_each_floor, :maximum_antennas_per_bda, presence: true, numericality: true
end
