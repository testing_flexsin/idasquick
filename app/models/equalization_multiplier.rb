class EqualizationMultiplier < ActiveRecord::Base
	## Validations
	validates :multiplied_factor, presence: true, numericality: true
end
