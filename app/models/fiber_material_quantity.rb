class FiberMaterialQuantity < ActiveRecord::Base
	## Relations
	belongs_to :report_material_list

	## Validations
	validates :report_material_list_id, presence: true, numericality: { only_integer: true }
	validates :quantity, presence: true, numericality: true
end
