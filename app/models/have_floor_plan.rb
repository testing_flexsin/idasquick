class HaveFloorPlan < ActiveRecord::Base
	validates :operator, :operand, presence: true
end
