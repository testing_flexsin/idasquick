class HighestFrequencyBand < ActiveRecord::Base
	## Relations
	has_many :cable_losses, dependent: :destroy
	has_many :tunnel_cable_c_losses, dependent: :destroy
	has_many :tunnel_cable_i_losses, dependent: :destroy

	## Validations
	validates :frequency, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
