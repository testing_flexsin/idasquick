class LinkBudget < ActiveRecord::Base
	## Relations
	belongs_to :building_data_entry

	## Validations
	validates :building_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :channel_power, :cable_length, :cable_loss, :splitter_loss, :jumper_loss, :connector_loss, :antenna_gain, :antenna_erp, :dl_margin, :allowed_pl, :rssi_at_portable, numericality: true, allow_nil: true
end
