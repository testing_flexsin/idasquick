class LinkBudgetAdmin < ActiveRecord::Base
	## Validations
	validates :cable_length, :antenna_gain, :dl_margin, presence: true, numericality: true
end
