class ManageCredit < ActiveRecord::Base
	## Validations
	validates :default_credits, :credits_required, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
