class MaterialCategory < ActiveRecord::Base
	## Relations
	has_many :report_material_lists, dependent: :destroy

	## Validations
	validates :category, presence: true
end
