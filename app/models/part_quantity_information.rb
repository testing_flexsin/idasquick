class PartQuantityInformation < ActiveRecord::Base
	## Relations
	belongs_to :building_data_entry
	belongs_to :report_material_list
	
	## Validations
	validates :building_data_entry_id, :report_material_list_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :quantity, numericality: true, allow_nil: true
end
