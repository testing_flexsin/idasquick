class Region < ActiveRecord::Base
	## Relations
	has_many :marketings, dependent: :destroy

	## Validations
	validates :name, presence: true
end
