class ReportMaterialList < ActiveRecord::Base
	## Relations
	belongs_to :material_category
	has_many :donor_direct_feed_quantities
	has_many :fiber_material_quantities
	has_many :product_price_lists
	has_many :part_quantity_informations

	## Validations
	validates :material_category_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :description, presence: true
end
