class RssiThresholdLevelBenchmark < ActiveRecord::Base
	## Validations
	validates :threshold_level, presence: true, numericality: { only_integer: true }
end
