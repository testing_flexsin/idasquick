class ServicesBreakout < ActiveRecord::Base
	## Validations
	validates :number_of_services, :number_of_bdas_donor, :number_of_bdas_das, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
