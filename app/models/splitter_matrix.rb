class SplitterMatrix < ActiveRecord::Base
	## Validations
	validates :antennas, :number_of_bdas, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

	## Callbacks
	before_save :check_parity

	def check_parity
		if self.number_of_bdas % 2 == 0
			self.matrix_type = "EVEN"
		else
			self.matrix_type = "ODD"
		end
	end
end
