class SquareFootageSize < ActiveRecord::Base
	validates :square_foot, presence: true, numericality: true
end
