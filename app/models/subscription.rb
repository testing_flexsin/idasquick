class Subscription < ActiveRecord::Base
	## Relations
	belongs_to :user

	## Show Per Page Records
	self.per_page = 10
end
