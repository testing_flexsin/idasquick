class SystemDimension < ActiveRecord::Base
	## Relations
	belongs_to :building_data_entry

	## Validations
	validates :building_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :estimated_path_distance, :frequency, presence: true, numericality: true
end
