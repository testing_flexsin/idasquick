class SystemFeedMethod < ActiveRecord::Base
	## Validations
	validates :feed_method, presence: true
end
