class TechnologyType < ActiveRecord::Base
	## Validations
	validates :technology, presence: true
	validates :papr, presence: true, numericality: true
end
