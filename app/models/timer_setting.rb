class TimerSetting < ActiveRecord::Base
	validates :plan_name, presence: true
	validates :report_generate_timer, :report_download_timer, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
