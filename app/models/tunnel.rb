class Tunnel < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_project
	has_many :tunnel_data_entries, dependent: :destroy

	## Validations
	validates :tunnel_project_id, :number_of_tunnels, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
