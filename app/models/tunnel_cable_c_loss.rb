class TunnelCableCLoss < ActiveRecord::Base
	## Relations
	belongs_to :highest_frequency_band

	## Validations
	validates :highest_frequency_band_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :cable_loss_1_by_2, presence: true, numericality: true
	validates :cable_loss_15_by_8, :cable_loss_7_by_8, :cable_loss_11_by_4, numericality: true, allow_nil: true
	
end
