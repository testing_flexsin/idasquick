class TunnelDataEntry < ActiveRecord::Base
	## Relations
	belongs_to :tunnel
	belongs_to :tunnel_environment
	has_one :tunnel_link_budget, dependent: :destroy
	has_one :tunnel_signal_level_benchmark, dependent: :destroy
	has_one :tunnel_design_information, dependent: :destroy
	has_many :tunnel_part_quantity_informations, dependent: :destroy

	## Validations
	validates :tunnel_name, presence: true
	validates :tunnel_id, :tunnel_number, :number_of_bores, :number_of_segments, :bore_number, :segment_number, :number_of_radio_rooms, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :tunnel_length, :tunnel_width, :tunnel_height, :environment_designator, presence: true, numericality: true
end
