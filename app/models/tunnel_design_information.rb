class TunnelDesignInformation < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_data_entry

	## Validations
	validates :tunnel_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :tunnel_length, :number_of_radio_rooms, :average_distance_radio_rooms, :number_of_cable_runs, :total_cable_length, :number_of_bdas, numericality: true, allow_nil: true
end
