class TunnelEnvironment < ActiveRecord::Base
	## Relations
	has_many :tunnel_data_entries

	## Validations
	validates :environment, presence: true
	validates :tunnel_indoor_factor, presence: true, numericality: true
end
