class TunnelFiberMaterialQuantity < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_report_material_list

	## Validations
	validates :tunnel_report_material_list_id, presence: true, numericality: { only_integer: true }
	validates :quantity, presence: true, numericality: true
end
