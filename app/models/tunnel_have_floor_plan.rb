class TunnelHaveFloorPlan < ActiveRecord::Base
	## Validations
	validates :operator, :operand, presence: true
end
