class TunnelLength < ActiveRecord::Base
	## Validations
	validates :length, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
