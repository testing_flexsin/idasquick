class TunnelLinkBudget < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_data_entry

	## Validations
	validates :tunnel_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :channel_power, :frequency, :distribution_loss, :radiax_length, :radiating_cable_insertion_loss, :radiating_cable_coupling_loss, :distance_from_cable, :indoor_margin, :indoor_factor, :path_loss_from_cable, :signal_level_at_mobile, numericality: true, allow_nil: true
end
