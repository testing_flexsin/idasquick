class TunnelLinkBudgetAdmin < ActiveRecord::Base
	## Validations
	validates :indoor_margin, presence: true, numericality: true
end
