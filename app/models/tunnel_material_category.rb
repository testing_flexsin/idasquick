class TunnelMaterialCategory < ActiveRecord::Base
	## Relations
	has_many :tunnel_report_material_lists, dependent: :destroy
	
	## Validations
	validates :category, presence: true
end
