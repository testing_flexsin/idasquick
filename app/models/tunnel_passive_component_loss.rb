class TunnelPassiveComponentLoss < ActiveRecord::Base
	## Validations
	validates :jumper_loss, :connector_loss, :way2_splitter_loss, :way3_splitter_loss, :way4_splitter_loss, :way6_splitter_loss, :way8_splitter_loss, :directional_coupler_loss, :hybrid_coupler_loss, presence: true, numericality: true
	validates :quantity_of_splitters, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :type_of_splitter, presence: true
end
