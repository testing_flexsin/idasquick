class TunnelProductFrequencyChannel < ActiveRecord::Base
	## Relations
	belongs_to :bda_product_category
	has_many :tunnel_projects

	## Validations
	validates :bda_product_category_id, :number_of_channels, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :composite_power, presence: true, numericality: true

	## Callbacks
  before_save :calculate_channel_power

	private
		def calculate_channel_power
			self.papr = TechnologyType.where(default_selected: true).last.try(:papr)
			self.channel_power = ((self.composite_power - 10 * (Math.log10(self.number_of_channels))) - self.papr).round(1)
		end
end
