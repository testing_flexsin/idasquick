class TunnelProject < ActiveRecord::Base
	## Soft Delete
	acts_as_paranoid

	## Friendly ID
	extend FriendlyId
	friendly_id :project_name

	## Relations
	belongs_to :user
	belongs_to :tunnel_product_frequency_channel
	belongs_to :marketing
	has_one :tunnel, dependent: :destroy

	## Validations
	validates :user_id, presence: true, numericality: { only_integer: true }
	validates :user_name, :project_name, :company, :facility_option, presence: true
	validates :number_of_services, :highest_frequency_band, :tunnel_product_frequency_channel_id, :marketing_id, :expected_rssi_at_mobile, numericality: { only_integer: true }, allow_nil: true

	## Callbacks
	after_update :save_building_data_entries_info, :send_admin_email

	## Show Per Page Records
	self.per_page = 10

	## Record every version of create, update, and destroy
	has_paper_trail

	## Override Friendly ID Method for generating slug
	def normalize_friendly_id(string)
		project_count = TunnelProject.where("project_name LIKE ?", "#{project_name}%").count
		if project_count > 0
	  	super.gsub("-", "_") + project_count.to_s
	  else
	  	super.gsub("-", "_")
	  end
	end

	## Class Methods
	def self.get_project(project_slug, user_id)
		self.where(slug: project_slug, user_id: user_id).eager_load(:tunnel).order("tunnel_projects.id ASC").first
	end

	def self.completed_projects(user_id, page)
		self.where(user_id: user_id, report: true).eager_load(:tunnel).paginate(page: page).order("tunnel_projects.id DESC")
	end

	def self.pending_projects(user_id, page)
		self.where(user_id: user_id, report: false).eager_load(:tunnel).paginate(page: page).order("tunnel_projects.id DESC")
	end

	## Instance Methods
	def project_details?
		self.step_completed == 1 ? true : false
	end

	def tunnel_data?
		self.step_completed == 2 ? true : false
	end

	def tunnel_services?
		self.step_completed == 3 ? true : false
	end

	def tunnel_system?
		self.step_completed == 4 ? true : false
	end

	def tunnel_type?
		self.step_completed == 5 ? true : false
	end

	def save_building_data_entries_info
		if self.report?
			tunnel_passive_component_loss = TunnelPassiveComponentLoss.last
			coverage_distance_per_bda = 0.0
			bda_product_category = ""
			tunnel_product_frequency_channel = self.tunnel_product_frequency_channel
			if tunnel_product_frequency_channel.present?
				product_category = tunnel_product_frequency_channel.bda_product_category
				category_id = product_category.try(:id)
				bda_product_category = product_category.try(:category)
				coverage_distance_per_bda = TunnelCoverageDistancePerBda.where(bda_product_category_id: category_id).last.try(:coverage_distance)
			end
			highest_frequency_band_id = HighestFrequencyBand.where(frequency: self.highest_frequency_band).last.try(:id)
			tunnel_cable_i_loss = TunnelCableILoss.where(highest_frequency_band_id: highest_frequency_band_id).last
			tunnel_cable_c_loss = TunnelCableCLoss.where(highest_frequency_band_id: highest_frequency_band_id).last
			indoor_margin = 0.0
			tunnel_link_budget_admin = TunnelLinkBudgetAdmin.last
			if tunnel_link_budget_admin.present?
				indoor_margin = tunnel_link_budget_admin.try(:indoor_margin)
			end
			cable_type_matrix = CableTypeMatrix.where(cable_type_assigned: true).last
			services_breakout = ServicesBreakout.where(number_of_services: self.number_of_services).last
			tunnel_report_material_lists = TunnelReportMaterialList.eager_load(:tunnel_donor_direct_feed_quantities, :tunnel_fiber_material_quantities).order("tunnel_report_material_lists.id ASC")

			tunnel_data_entries = self.tunnel.tunnel_data_entries.eager_load(:tunnel_link_budget, :tunnel_signal_level_benchmark, :tunnel_design_information, :tunnel_part_quantity_informations).order("tunnel_data_entries.id ASC")
			tunnel_data_entries.each do |tunnel_data_entry|
				length_of_bore = tunnel_data_entries.where(tunnel_number: tunnel_data_entry.tunnel_number, bore_number: tunnel_data_entry.bore_number).sum(:tunnel_length)

				## Save Link Budget Start
				channel_power = 0.0
				if tunnel_product_frequency_channel.present?
					channel_power = ((tunnel_product_frequency_channel.composite_power - 10 * (Math.log10(tunnel_product_frequency_channel.number_of_channels))) - self.papr).round(1)
				end

				frequency = self.highest_frequency_band

				distribution_loss = 0.0
				if tunnel_passive_component_loss.present?
					distribution_loss = tunnel_passive_component_loss.quantity_of_splitters * tunnel_passive_component_loss.try(tunnel_passive_component_loss.type_of_splitter.to_sym)
				end

				radiax_length = (tunnel_data_entry.tunnel_length * tunnel_data_entry.number_of_bores * 0.3048)

				radiating_cable_insertion_loss = 0.0
				radiating_cable_coupling_loss = 0.0
				if highest_frequency_band_id.present?
					radiating_cable_type = :cable_loss_1_by_2
					if cable_type_matrix.present? && cable_type_matrix.radiating_cable_type.present?
						radiating_cable_type = cable_type_matrix.radiating_cable_type.to_sym
					end
					radiating_cable_insertion_loss = tunnel_cable_i_loss.try(radiating_cable_type)
					radiating_cable_coupling_loss = tunnel_cable_c_loss.try(radiating_cable_type)
					cable_type = radiating_cable_type.to_s
				end

				distance_from_cable = (Math.sqrt((tunnel_data_entry.tunnel_width ** 2) + (tunnel_data_entry.tunnel_height ** 2))) * 0.3048

				indoor_factor = tunnel_data_entry.environment_designator

				path_loss_from_cable = (radiating_cable_coupling_loss + indoor_margin + (20 * Math.log10(self.highest_frequency_band / 1000.0)) + ((10 * indoor_factor) * (Math.log10(distance_from_cable) - 2)))

				signal_level_at_mobile = (channel_power - distribution_loss - (radiax_length * (radiating_cable_insertion_loss / 100)) - path_loss_from_cable)

				tunnel_data_entry.build_tunnel_link_budget(channel_power: channel_power, frequency: frequency, distribution_loss: distribution_loss, radiax_length: radiax_length, radiating_cable_insertion_loss: radiating_cable_insertion_loss, radiating_cable_coupling_loss: radiating_cable_coupling_loss, cable_type: cable_type, distance_from_cable: distance_from_cable, indoor_margin: indoor_margin, indoor_factor: indoor_factor, path_loss_from_cable: path_loss_from_cable, signal_level_at_mobile: signal_level_at_mobile).save
				## Save Link Budget End

				## Save Signal Level Benchmark Start
				tunnel_link_budget = tunnel_data_entry.tunnel_link_budget
				rssi_at_portable_in_tunnel = tunnel_link_budget.try(:signal_level_at_mobile)
				threshold_level_benchmark = self.expected_rssi_at_mobile
				threshold_overlimit_factor = rssi_at_portable_in_tunnel - threshold_level_benchmark

				tunnel_data_entry.build_tunnel_signal_level_benchmark(rssi_at_portable_in_tunnel: rssi_at_portable_in_tunnel, threshold_level_benchmark: threshold_level_benchmark, threshold_overlimit_factor: threshold_overlimit_factor).save
				## Save Signal Level Benchmark End

				## Save Design Information Start
				tunnel_link_budget = tunnel_data_entry.tunnel_link_budget
				tunnel_signal_level_benchmark = tunnel_data_entry.tunnel_signal_level_benchmark
				tunnel_length = tunnel_data_entry.tunnel_length.round

				number_of_radio_rooms = tunnel_data_entry.number_of_radio_rooms.round

				average_distance_radio_rooms = (length_of_bore / number_of_radio_rooms).round

				number_of_bdas = (tunnel_data_entry.tunnel_length / coverage_distance_per_bda).round

				number_of_cable_runs = number_of_bdas * 2

				cable_type = tunnel_link_budget.cable_type

				total_cable_length = tunnel_data_entry.tunnel_length

				communication_type = self.communication_type

				tunnel_data_entry.build_tunnel_design_information(tunnel_length: tunnel_length, number_of_radio_rooms: number_of_radio_rooms, average_distance_radio_rooms: average_distance_radio_rooms, number_of_cable_runs: number_of_cable_runs, cable_type: cable_type, total_cable_length: total_cable_length, bda_product_category: bda_product_category, number_of_bdas: number_of_bdas, communication_type: communication_type).save
				
				## Recalculate values of Link Budget DB18 and Signal Level Benchmark DB20
				radiax_length = tunnel_link_budget.radiax_length / number_of_cable_runs
				radiating_cable_insertion_loss = tunnel_link_budget.radiating_cable_insertion_loss
				radiating_cable_coupling_loss = tunnel_link_budget.radiating_cable_coupling_loss
				path_loss_from_cable = tunnel_link_budget.path_loss_from_cable
				signal_level_at_mobile = (tunnel_link_budget.channel_power - tunnel_link_budget.distribution_loss - (radiax_length * (radiating_cable_insertion_loss / 100)) - path_loss_from_cable)

				rssi_at_portable_in_tunnel = tunnel_signal_level_benchmark.rssi_at_portable_in_tunnel
				threshold_level_benchmark = tunnel_signal_level_benchmark.threshold_level_benchmark
				threshold_overlimit_factor = tunnel_signal_level_benchmark.threshold_overlimit_factor

				if threshold_overlimit_factor < 3
					radiating_cable_type = :cable_loss_7_by_8
					radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_overlimit_factor = check_threshold_overlimit_factor(tunnel_link_budget, radiating_cable_type, highest_frequency_band_id, tunnel_cable_i_loss, tunnel_cable_c_loss, radiax_length, radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, self.highest_frequency_band, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_level_benchmark, threshold_overlimit_factor)

					if threshold_overlimit_factor < 3
						radiating_cable_type = :cable_loss_11_by_4
						radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_overlimit_factor = check_threshold_overlimit_factor(tunnel_link_budget, radiating_cable_type, highest_frequency_band_id, tunnel_cable_i_loss, tunnel_cable_c_loss, radiax_length, radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, self.highest_frequency_band, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_level_benchmark, threshold_overlimit_factor)
					end
					if threshold_overlimit_factor < 3
						radiating_cable_type = :cable_loss_15_by_8
						radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_overlimit_factor = check_threshold_overlimit_factor(tunnel_link_budget, radiating_cable_type, highest_frequency_band_id, tunnel_cable_i_loss, tunnel_cable_c_loss, radiax_length, radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, self.highest_frequency_band, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_level_benchmark, threshold_overlimit_factor)
					end
					cable_type = radiating_cable_type.to_s
				end
				tunnel_link_budget.update(radiax_length: radiax_length, radiating_cable_insertion_loss: radiating_cable_insertion_loss, radiating_cable_coupling_loss: radiating_cable_coupling_loss, cable_type: cable_type, path_loss_from_cable: path_loss_from_cable, signal_level_at_mobile: signal_level_at_mobile)
				tunnel_signal_level_benchmark.update(rssi_at_portable_in_tunnel: rssi_at_portable_in_tunnel, threshold_overlimit_factor: threshold_overlimit_factor)
				tunnel_data_entry.tunnel_design_information.update(cable_type: cable_type)
				## Save Design Information End

				## Save Part Quantity Information Start
				tunnel_design_information = tunnel_data_entry.tunnel_design_information

				tunnel_report_material_lists.each do |tunnel_report_material_list|
					save = false
					quantity = 0
					donor_direct_feed_quantity = tunnel_report_material_list.tunnel_donor_direct_feed_quantities.last
		  		fiber_material_quantity = tunnel_report_material_list.tunnel_fiber_material_quantities.last
					
					## BI-DIRECTIONAL AMP
			  	if tunnel_report_material_list.description == "Low Power (1 Watt Composite Power) BDA Unit Connected to DAS" && (tunnel_report_material_list.tunnel_material_category_id == 1) && (self.tunnel_product_frequency_channel.try(:bda_product_category).try(:id) == 1)
						quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
						if quantity < 1
							quantity = 1
						end
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Medium Power (2 Watts Composite Power) BDA Unit Connected to DAS" && (tunnel_report_material_list.tunnel_material_category_id == 1) && (self.tunnel_product_frequency_channel.try(:bda_product_category).try(:id) == 2)
						quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
						if quantity < 1
							quantity = 1
						end
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "High Power (5 Watts Composite Power) BDA Unit Connected to DAS" && (tunnel_report_material_list.tunnel_material_category_id == 1) && (self.tunnel_product_frequency_channel.try(:bda_product_category).try(:id) == 3)
						quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
						if quantity < 1
							quantity = 1
						end
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Medium Power (2 Watts Composite Power) BDA Unit Connected to OFF-AIR DONOR" && (tunnel_report_material_list.tunnel_material_category_id == 1)
						quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
	  				if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true

					## DIRECT FEED
			  	elsif tunnel_report_material_list.description == "Base Station Interface (Point of Interconnect)" && (tunnel_report_material_list.tunnel_material_category_id == 2) && (self.system_feed_method == "Direct Feed")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif (tunnel_report_material_list.description == "Hybrid Combiner, N-type connectors") && (tunnel_report_material_list.tunnel_material_category_id == 2) && (self.system_feed_method == "Direct Feed")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif (tunnel_report_material_list.description == "Custom Built Filtering, N-type connectors") && (tunnel_report_material_list.tunnel_material_category_id == 2) && (self.system_feed_method == "Direct Feed")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true

					## FIBER OPTIC
			  	elsif tunnel_report_material_list.description == "Master Fiber Optic Transceiver (Single Port)" && (tunnel_report_material_list.tunnel_material_category_id == 3) && (self.system_architecture == "Fiber and Coax")
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
			  		quantity = fiber_material_quantity.quantity * quantity
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Master Fiber Optic Transceiver (8 Fiber Optic Ports)" && (tunnel_report_material_list.tunnel_material_category_id == 3) && (self.system_architecture == "Fiber and Coax")
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
			  		quantity = fiber_material_quantity.quantity * quantity
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = false
			  	elsif tunnel_report_material_list.description == "Slave Fiber Optic Transceiver (Single Port)" && (tunnel_report_material_list.tunnel_material_category_id == 3) && (self.system_architecture == "Fiber and Coax")
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
			  		quantity = fiber_material_quantity.quantity * quantity
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true
			  	elsif tunnel_report_material_list.description == "Fiber Optic Jumpers (10ft)" && (tunnel_report_material_list.tunnel_material_category_id == 3) && (self.system_architecture == "Fiber and Coax")
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
			  		quantity = fiber_material_quantity.quantity * quantity
			  		if quantity < 1
							quantity = 2 * 1
						else
							quantity = 2 * quantity
						end
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true
			  	elsif tunnel_report_material_list.description == "Fiber Optic Backbone Cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 3) && (self.system_architecture == "Fiber and Coax")
			  		bdas_total = 0.0
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
			  		if quantity >= 1
			  			(1..quantity).each do |bda|
			  				bdas_total += (length_of_bore - (length_of_bore / quantity) * bda)
			  			end
			  		end
			  		quantity = (length_of_bore + bdas_total) * tunnel_data_entry.number_of_bores
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true

					## OFF-AIR DONOR
			  	elsif tunnel_report_material_list.description == "Off-air donor antenna, Yagi, minimum 10dBi Gain" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "DC Block Donor, Coaxial Line Protector (N-Female connectors)" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "1/2 inch Off-air donor antenna cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "N-Male connectors for off-air donor antenna cables" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "Ground Kit for 1/2 inch Coax (for off-air donor antenna cable outer conductor)" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "Universal Weatherproofing Kit (for off-air donor antenna cable connectors)" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true
			  	elsif tunnel_report_material_list.description == "Prep Tool for 1/2 inch donor cable (all-in-one strip tool)" && (tunnel_report_material_list.tunnel_material_category_id == 4) && (self.system_feed_method == "Off-Air")
			  		quantity = self.number_of_services
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_donor
							quantity = bda_quantity_multiplier
	  				end
			  		quantity = quantity * donor_direct_feed_quantity.quantity
						save = true

					## TUNNEL DAS
			  	elsif tunnel_report_material_list.description == "1-5/8 inch Diameter Radiating Cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_design_information.cable_type == "cable_loss_15_by_8")
			  		quantity = tunnel_design_information.total_cable_length
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "1-1/4 inch Diameter Radiating Cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_design_information.cable_type == "cable_loss_11_by_4")
			  		quantity = tunnel_design_information.total_cable_length
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "7/8 inch Diameter Radiating Cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_design_information.cable_type == "cable_loss_7_by_8")
			  		quantity = tunnel_design_information.total_cable_length
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "1/2 inch Diameter Radiating Cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_design_information.cable_type == "cable_loss_1_by_2")
			  		quantity = tunnel_design_information.total_cable_length
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "1/2 inch plenum rated coaxial cable (ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = 100 * tunnel_design_information.number_of_bdas
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "2 way splitters (broadband),N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_passive_component_loss.type_of_splitter == "way2_splitter_loss")
						quantity = tunnel_design_information.number_of_bdas
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true
			  	elsif tunnel_report_material_list.description == "3 way splitters (broadband),N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_passive_component_loss.type_of_splitter == "way3_splitter_loss")
						quantity = tunnel_design_information.number_of_bdas
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true
			  	elsif tunnel_report_material_list.description == "4 way splitters (broadband),N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (tunnel_passive_component_loss.type_of_splitter == "way4_splitter_loss")
						quantity = tunnel_design_information.number_of_bdas
						if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
			  		save = true
			  	elsif tunnel_report_material_list.description == "Directional Coupler (broadband),N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = 0
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = false
			  	elsif tunnel_report_material_list.description == "Crossband Coupler ,N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (self.number_of_services < 3)
			  		if self.number_of_services < 2
			  			quantity = 0
							save = false
			  		elsif self.number_of_services == 2
			  			quantity = tunnel_design_information.number_of_bdas + self.number_of_services
							save = true
			  		end
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Hybrid Combiner,N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = 0
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = false
			  	elsif tunnel_report_material_list.description == "Custom Built Filtering,N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5) && (self.number_of_services >= 3)
			  		quantity = tunnel_design_information.number_of_bdas + self.number_of_services
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = tunnel_design_information.number_of_bdas
						if (self.number_of_services >= 3) && (self.number_of_services <= 20) && services_breakout.present?
							bda_quantity_multiplier = services_breakout.number_of_bdas_das
							quantity = bda_quantity_multiplier * quantity
	  				end
						if tunnel_passive_component_loss.present?
			  			quantity = (2 * tunnel_passive_component_loss.quantity_of_splitters) + quantity
			  			if self.communication_type.downcase == "simplex"
								quantity = quantity * 2
							end
							save = true
						end
			  	elsif tunnel_report_material_list.description == "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
						if tunnel_passive_component_loss.present?
			  			quantity = 6 * tunnel_passive_component_loss.quantity_of_splitters
			  			if self.communication_type.downcase == "simplex"
								quantity = quantity * 2
							end
							save = true
						end
			  	elsif tunnel_report_material_list.description == "Antenna, Yagi, minimum 10dBi Gain, N-type connectors" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = 0
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = false
			  	elsif tunnel_report_material_list.description == "Corresponding size cable clamps (kit or individual)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = tunnel_design_information.total_cable_length / 4
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "Corresponding Size Radiating Cable Connector (N-Type)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = tunnel_design_information.number_of_cable_runs * 2
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	elsif tunnel_report_material_list.description == "End of Cable 50 Ohm Load (N-Type Connector)" && (tunnel_report_material_list.tunnel_material_category_id == 5)
			  		quantity = tunnel_design_information.number_of_cable_runs
			  		if self.communication_type.downcase == "simplex"
							quantity = quantity * 2
						end
						save = true
			  	end

			  	if quantity.present?
			  		quantity = quantity.round
			  	end
			  	if save && (quantity > 0)
						tunnel_data_entry.tunnel_part_quantity_informations.create(tunnel_report_material_list_id: tunnel_report_material_list.id, quantity: quantity)
					end
				end
				## Save Part Quantity Information End
			end
		end
	end

	def check_threshold_overlimit_factor(tunnel_link_budget, radiating_cable_type, highest_frequency_band_id, tunnel_cable_i_loss, tunnel_cable_c_loss, radiax_length, radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, highest_frequency_band, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_level_benchmark, threshold_overlimit_factor)
		if highest_frequency_band_id.present?
			radiating_cable_insertion_loss = tunnel_cable_i_loss.try(radiating_cable_type)
			radiating_cable_coupling_loss = tunnel_cable_c_loss.try(radiating_cable_type)
		end

		path_loss_from_cable = (radiating_cable_coupling_loss + tunnel_link_budget.indoor_margin + (20 * Math.log10(highest_frequency_band / 1000.0)) + ((10 * tunnel_link_budget.indoor_factor) * (Math.log10(tunnel_link_budget.distance_from_cable) - 2)))

		signal_level_at_mobile = (tunnel_link_budget.channel_power - tunnel_link_budget.distribution_loss - (radiax_length * (radiating_cable_insertion_loss / 100)) - path_loss_from_cable)

		rssi_at_portable_in_tunnel = signal_level_at_mobile
		threshold_overlimit_factor = rssi_at_portable_in_tunnel - threshold_level_benchmark

		return radiating_cable_insertion_loss, radiating_cable_coupling_loss, path_loss_from_cable, signal_level_at_mobile, rssi_at_portable_in_tunnel, threshold_overlimit_factor
	end

	def send_admin_email
		if self.report?
			ProjectMailer.delay.email_admin(self)
		end
	end
end
