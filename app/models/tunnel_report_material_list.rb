class TunnelReportMaterialList < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_material_category
	has_many :tunnel_donor_direct_feed_quantities
	has_many :tunnel_fiber_material_quantities
	has_many :tunnel_product_price_lists
	has_many :tunnel_part_quantity_informations

	## Validations
	validates :tunnel_material_category_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :description, presence: true
end
