class TunnelSignalLevelBenchmark < ActiveRecord::Base
	## Relations
	belongs_to :tunnel_data_entry

	## Validations
	validates :tunnel_data_entry_id, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
	validates :rssi_at_portable_in_tunnel, :threshold_level_benchmark, :threshold_overlimit_factor, presence: true, numericality: true
end
