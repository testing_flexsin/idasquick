class TunnelSplitterMatrix < ActiveRecord::Base
	## Validations
	validates :number_of_bdas, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
 	validates :product_type_2w, :product_type_3w, :product_type_4w, :dc, numericality: true, allow_nil: true
end
