class UnitMultiplier < ActiveRecord::Base
	## Validations
	validates :multiplied_factor, presence: true, numericality: true
end
