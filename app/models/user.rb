class User < ActiveRecord::Base  
  # Include default devise modules. Others available are:
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, authentication_keys: [:login]

  ## Relations
  has_many :projects, dependent: :destroy
  has_many :tunnel_projects, dependent: :destroy
  has_many :subscriptions, dependent: :destroy
  belongs_to :building_plan, class_name: 'Plan', foreign_key: 'building_plan_id'
  belongs_to :tunnel_plan, class_name: 'Plan', foreign_key: 'tunnel_plan_id'

  ## Validations
  validates :first_name, :last_name, :user_name, :company_name, :phone_number, presence: true
  validates :user_name, uniqueness: { case_sensitive: false }, format: { with: /^[a-zA-Z0-9_\.]*$/, multiline: true }, exclusion: { in: %w(super super_admin admin user admin_user user_admin), message: "%{value} is taken." }
  validates :phone_number, length: { in: 8..15 }, numericality: { only_integer: true }
  # validates :password, format: { with: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}$/, message: "must include one number, one uppercase letter and one lowercase letter.", multiline: true}, on: :create
  validates :terms_and_conditions, acceptance: true

  attr_accessor :login, :current_password

  ## Callbacks
  before_create :assign_default_values, :send_verification_email
  before_save :assign_full_name

  ## Class Methods
  def self.find_first_by_auth_conditions(warden_conditions)
	  conditions = warden_conditions.dup
	  if login = conditions.delete(:login)
	    where(conditions.to_h).where(["lower(user_name) = :value OR lower(email) = :value", { value: login.downcase }]).first
	  else
	    where(conditions.to_h).first
	  end
	end

  def self.user_email(email)
    self.where(email: email).last
  end

  def self.user_username(user_name)
    self.where(user_name: user_name).last
  end

  ## Instance Methods

  #this method is called by devise to check for "active" state of the model
  def active_for_authentication?
    super and !self.disable?
  end

  def assign_default_values
    self.building_quote_storage_date = Time.now
    self.tunnel_quote_storage_date = Time.now
    manage_credit = ManageCredit.last
    if manage_credit.present?
      self.credits = manage_credit.default_credits
    end
  end

  def send_verification_email
  	UserMailer.delay.verification(self.email)
  end

  def assign_full_name
    first_name = (self.first_name.present? ? self.first_name : "")
    last_name = (self.last_name.present? ? self.last_name : "")
    self.full_name = first_name + " " + last_name
  end
end
