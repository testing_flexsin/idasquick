role :app, %w{ubuntu@52.89.218.97}
role :web, %w{ubuntu@52.89.218.97}
role :db,  %w{ubuntu@52.89.218.97}

set :stage, :production
set :rails_env, :production

set :deploy_user, "ubuntu"

server '52.89.218.97', user: 'ubuntu', roles: %w{web app db}, primary: true

# set :ssh_options, {
# 	ubuntu: 'ubuntu',
# 	forward_agent: false,
# 	auth_methods: %w(password),
# 	password: 'password'
# }

set :ssh_options, {
	keys: %w(/home/ubuntu/buildings.pem),
	forward_agent: false,
	auth_methods: %w(publickey)
}
