Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions' }

  resources :projects, except: [:show] do
    member do
      post :building
      get  :building_data_entry
      post :save_building_data_entry
      post :update_building_services
      post :update_building_system
      post :update_building_type
      get  :disclaimer
      post :update_disclaimer
      get  :report
      get  :show_generate_report
      get  :show_download_report
      get  :get_total_area_building
      get  :report_export
      get  :check_data_entries
      get  :check_system_architecture
    end
    collection do
      get  :get_floor_plan_message
    end
  end
  get  '/projects_pending'                   => 'projects#projects_pending',               as: :projects_pending
  get  '/projects/:id/building'              => 'projects#building_entry',                 as: :building_entry
  get  '/projects/:id/services'              => 'projects#project_building_services',      as: :project_building_services
  get  '/projects/:id/system'                => 'projects#project_building_system',        as: :project_building_system
  get  '/projects/:id/type'                  => 'projects#project_building_type',          as: :project_building_type
  get  '/projects/:id/destroy'               => 'projects#destroy_show',                   as: :project_destroy_show

  resources :tunnel_projects, except: [:show] do
    member do
      post :tunnel
      get  :tunnel_data_entry
      post :save_tunnel_data_entry
      post :update_tunnel_services
      post :update_tunnel_system
      post :update_tunnel_type
      get  :disclaimer
      post :update_disclaimer
      get  :report
      get  :show_generate_report
      get  :show_download_report
      get  :report_export
      get  :check_data_entries
      get  :check_system_architecture
    end
    collection do
      get  :get_floor_plan_message
    end
  end
  get  '/tunnel_projects_pending'            => 'tunnel_projects#projects_pending',        as: :tunnel_projects_pending
  get  '/tunnel_projects/:id/tunnel'         => 'tunnel_projects#tunnel_entry',            as: :tunnel_entry
  get  '/tunnel_projects/:id/services'       => 'tunnel_projects#project_tunnel_services', as: :project_tunnel_services
  get  '/tunnel_projects/:id/system'         => 'tunnel_projects#project_tunnel_system',   as: :project_tunnel_system
  get  '/tunnel_projects/:id/type'           => 'tunnel_projects#project_tunnel_type',     as: :project_tunnel_type
  get  '/tunnel_projects/:id/destroy'        => 'tunnel_projects#destroy_show',            as: :project_tunnel_destroy_show

  get  '/about_us'                           => 'home#about_us',                           as: :about_us
  get  '/contact_us'                         => 'home#contact_us',                         as: :contact_us
  post '/contact_us'                         => 'home#save_contact_us',                    as: :save_contact_us
  get  '/pricing'                            => 'home#pricing',                            as: :pricing
  get  '/help'                               => 'home#help',                               as: :help
  get  '/faq'                                => 'home#faq',                                as: :faq
  get  '/terms_and_conditions'               => 'home#terms_and_conditions',               as: :terms_and_conditions
  get  '/privacy_policy'                     => 'home#privacy_policy',                     as: :privacy_policy
  
  get  '/welcome'                            => 'users#welcome',                           as: :welcome
  get  '/dashboard'                          => 'users#dashboard',                         as: :dashboard
  get  '/quick_start'                        => 'users#quick_start',                       as: :quick_start
  get  '/quick_start/step1'                  => 'users#quick_start_step1',                 as: :quick_start_step1
  get  '/quick_start/step2'                  => 'users#quick_start_step2',                 as: :quick_start_step2
  get  '/quick_start/step3'                  => 'users#quick_start_step3',                 as: :quick_start_step3
  get  '/quick_start/download'               => 'users#download',                          as: :quick_start_download
  get  '/credits'                            => 'users#credits',                           as: :credits
  get  '/users/retrieve_password/new'        => 'users#retrieve_password',                 as: :retrieve_password
  post '/users/retrieve_password'            => 'users#retrieve_forgotten_password',       as: :retrieve_forgotten_password
  get  '/send_activation_email'              => 'users#send_activation_email',             as: :send_activation_email
  get  '/check_project_subscription'         => 'users#check_project_subscription',        as: :check_project_subscription
  get  '/check_username_unique'              => 'users#check_username_unique',             as: :check_username_unique
  get  '/check_current_username_unique'      => 'users#check_current_username_unique',     as: :check_current_username_unique
  get  '/check_email_unique'                 => 'users#check_email_unique',                as: :check_email_unique
  get  '/check_current_email_unique'         => 'users#check_current_email_unique',        as: :check_current_email_unique

  resources :subscriptions, only: [:create] do
    collection do
      post :plans
    end
  end

  # get '*path' => redirect('/')
  get "/404" => "errors#not_found"
  get "/500" => "errors#internal_server_error"

  devise_scope :user do
    authenticated :user do
      root to: "users#dashboard"
    end
    unauthenticated :user do
      root to: 'users/registrations#new', as: :unauthenticated_root
    end
  end
  
  # mount Ckeditor::Engine => '/ckeditor'
end
