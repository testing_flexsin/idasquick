set :output, "log/cron_log.log"

every 1.month do
	rake "delete_old:versions"
	rake "psql_db:backup"
end

every :day, at: '12:05 am' do
  rake "check_user:credits_and_subscription"
end

every :day, at: '12:15 am' do
  rake "check_long_term:data_storage"
end
# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
