class CreateNumberOfBuildings < ActiveRecord::Migration
  def change
    create_table :number_of_buildings do |t|
      t.integer :buildings

      t.timestamps null: false
    end
  end
end
