class CreateHaveFloorPlans < ActiveRecord::Migration
  def change
    create_table :have_floor_plans do |t|
      t.string :operator
      t.string :operand

      t.timestamps null: false
    end
  end
end
