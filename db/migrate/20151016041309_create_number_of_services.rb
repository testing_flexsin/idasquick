class CreateNumberOfServices < ActiveRecord::Migration
  def change
    create_table :number_of_services do |t|
      t.integer :services

      t.timestamps null: false
    end
  end
end
