class CreateBdaProductCategories < ActiveRecord::Migration
  def change
    create_table :bda_product_categories do |t|
      t.string :category

      t.timestamps null: false
    end
  end
end
