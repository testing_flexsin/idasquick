class CreateProductFrequencyChannels < ActiveRecord::Migration
  def change
    create_table :product_frequency_channels do |t|
      t.integer :number_of_channels
      t.float :composite_power
      t.float :channel_power
      t.float :papr
      t.integer :bda_product_category_id

      t.timestamps null: false
    end
  end
end
