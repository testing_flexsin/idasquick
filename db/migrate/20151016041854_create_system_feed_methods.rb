class CreateSystemFeedMethods < ActiveRecord::Migration
  def change
    create_table :system_feed_methods do |t|
      t.string :feed_method
      t.boolean :feed_assigned, default: false

      t.timestamps null: false
    end
  end
end
