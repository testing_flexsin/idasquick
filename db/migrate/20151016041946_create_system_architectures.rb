class CreateSystemArchitectures < ActiveRecord::Migration
  def change
    create_table :system_architectures do |t|
      t.string :architecture
      t.boolean :architecture_assigned, default: false

      t.timestamps null: false
    end
  end
end
