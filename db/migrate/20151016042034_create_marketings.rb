class CreateMarketings < ActiveRecord::Migration
  def change
    create_table :marketings do |t|
      t.integer :region_id
      t.string :vendor_name

      t.timestamps null: false
    end
  end
end
