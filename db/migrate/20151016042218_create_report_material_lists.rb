class CreateReportMaterialLists < ActiveRecord::Migration
  def change
    create_table :report_material_lists do |t|
      t.integer :material_category_id
      t.text :description

      t.timestamps null: false
    end
  end
end
