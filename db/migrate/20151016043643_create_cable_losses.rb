class CreateCableLosses < ActiveRecord::Migration
  def change
    create_table :cable_losses do |t|
      t.integer :highest_frequency_band_id
      t.float :cable_loss_1_by_2
      t.float :cable_loss_7_by_8
      t.float :cable_loss_11_by_4
      t.float :cable_loss_15_by_8

      t.timestamps null: false
    end
  end
end
