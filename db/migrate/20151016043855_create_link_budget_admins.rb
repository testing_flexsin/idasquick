class CreateLinkBudgetAdmins < ActiveRecord::Migration
  def change
    create_table :link_budget_admins do |t|
      t.float :cable_length
      t.float :antenna_gain
      t.float :dl_margin

      t.timestamps null: false
    end
  end
end
