class CreateBuildings < ActiveRecord::Migration
  def change
    create_table :buildings do |t|
      t.integer :project_id
      t.integer :number_of_buildings
      t.string :have_floorplan

      t.timestamps null: false
    end
  end
end
