class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :user_id
      t.string :user_name
      t.string :project_name
      t.string :company
      t.string :building_name
      t.string :facility_option
      t.integer :number_of_services
      t.integer :highest_frequency_band
      t.integer :product_frequency_channel_id
      t.string :system_feed_method
      t.string :system_architecture
      t.string :marketing_vendor_name

      t.timestamps null: false
    end
  end
end
