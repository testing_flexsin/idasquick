class CreateSystemDimensions < ActiveRecord::Migration
  def change
    create_table :system_dimensions do |t|
      t.integer :building_data_entry_id
      t.float :estimated_path_distance
      t.float :frequency

      t.timestamps null: false
    end
  end
end
