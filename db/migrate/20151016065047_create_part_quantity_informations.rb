class CreatePartQuantityInformations < ActiveRecord::Migration
  def change
    create_table :part_quantity_informations do |t|
      t.integer :building_data_entry_id
      t.integer :report_material_list_id
      t.integer :quantity

      t.timestamps null: false
    end
  end
end
