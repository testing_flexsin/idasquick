class AddReportToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :report, :boolean, default: false
  end
end
