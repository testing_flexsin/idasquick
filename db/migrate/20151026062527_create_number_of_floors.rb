class CreateNumberOfFloors < ActiveRecord::Migration
  def change
    create_table :number_of_floors do |t|
      t.integer :floors

      t.timestamps null: false
    end
  end
end
