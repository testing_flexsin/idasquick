class CreateDesignInformationAdmins < ActiveRecord::Migration
  def change
    create_table :design_information_admins do |t|
      t.float :distance_between_each_floor

      t.timestamps null: false
    end
  end
end
