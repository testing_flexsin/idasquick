class AddStepCompletedToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :step_completed, :integer, default: 0
  end
end
