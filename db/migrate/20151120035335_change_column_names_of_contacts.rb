class ChangeColumnNamesOfContacts < ActiveRecord::Migration
  def change
  	rename_column :contacts, :name, :first_name
  	rename_column :contacts, :subject, :last_name
  end
end
