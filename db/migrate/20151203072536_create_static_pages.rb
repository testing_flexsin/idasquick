class CreateStaticPages < ActiveRecord::Migration
  def change
    create_table :static_pages do |t|
      t.text :contact_us, default: " "
      t.text :faq, default: " "
      t.text :privacy_policy, default: " "
      t.text :terms_and_conditions, default: " "
      t.text :about_us, default: " "
      t.text :help, default: " "
      t.text :quick_start, default: " "
      t.text :quick_start_step1, default: " "
      t.text :quick_start_step2, default: " "
      t.text :quick_start_step3, default: " "
      t.text :quick_start_download, default: " "

      t.timestamps null: false
    end
  end
end
