class CreateDonorDirectFeedQuantities < ActiveRecord::Migration
  def change
    create_table :donor_direct_feed_quantities do |t|
      t.integer :report_material_list_id
      t.string :description
      t.float :quantity

      t.timestamps null: false
    end
  end
end
