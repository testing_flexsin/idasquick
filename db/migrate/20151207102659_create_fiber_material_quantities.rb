class CreateFiberMaterialQuantities < ActiveRecord::Migration
  def change
    create_table :fiber_material_quantities do |t|
      t.integer :report_material_list_id
      t.string :description
      t.float :quantity

      t.timestamps null: false
    end
  end
end
