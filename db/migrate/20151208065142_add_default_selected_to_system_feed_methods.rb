class AddDefaultSelectedToSystemFeedMethods < ActiveRecord::Migration
  def change
    add_column :system_feed_methods, :default_selected, :boolean, default: false
  end
end
