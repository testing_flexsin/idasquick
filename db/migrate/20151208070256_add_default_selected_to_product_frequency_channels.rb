class AddDefaultSelectedToProductFrequencyChannels < ActiveRecord::Migration
  def change
    add_column :product_frequency_channels, :default_selected, :boolean, default: false
  end
end
