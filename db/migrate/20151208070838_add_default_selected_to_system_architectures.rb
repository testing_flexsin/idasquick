class AddDefaultSelectedToSystemArchitectures < ActiveRecord::Migration
  def change
    add_column :system_architectures, :default_selected, :boolean, default: false
  end
end
