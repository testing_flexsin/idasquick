class CreateSquareFootageSizes < ActiveRecord::Migration
  def change
    create_table :square_footage_sizes do |t|
      t.float :square_foot

      t.timestamps null: false
    end
  end
end
