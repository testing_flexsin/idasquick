class AddColumnHomePageToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :home_page, :text, default: " "
  end
end
