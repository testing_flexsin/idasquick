class AddReportDisclaimerToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :report_disclaimer, :text, default: " "
  end
end
