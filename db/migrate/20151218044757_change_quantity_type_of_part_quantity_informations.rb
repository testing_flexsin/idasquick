class ChangeQuantityTypeOfPartQuantityInformations < ActiveRecord::Migration
  def change
  	change_column :part_quantity_informations, :quantity, :float
  end
end
