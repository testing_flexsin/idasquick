class AddHomeSliderToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :home_slider, :text, default: " "
  end
end
