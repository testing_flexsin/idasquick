class AddVisitedReportToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :visited_report, :boolean, default: false
  end
end
