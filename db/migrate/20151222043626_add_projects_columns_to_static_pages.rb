class AddProjectsColumnsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :building_projects, :text, default: " "
    add_column :static_pages, :building_pending_projects, :text, default: " "
    add_column :static_pages, :tunnel_projects, :text, default: " "
    add_column :static_pages, :tunnel_pending_projects, :text, default: " "
  end
end
