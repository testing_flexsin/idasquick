class CreateNumberOfTunnels < ActiveRecord::Migration
  def change
    create_table :number_of_tunnels do |t|
      t.integer :tunnels

      t.timestamps null: false
    end
  end
end
