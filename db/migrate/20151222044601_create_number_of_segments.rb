class CreateNumberOfSegments < ActiveRecord::Migration
  def change
    create_table :number_of_segments do |t|
      t.integer :segments

      t.timestamps null: false
    end
  end
end
