class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :user_id
      t.string :transaction_id
      t.string :transaction_gateway
      t.string :transaction_receipt
      t.string :transaction_type
      t.datetime :transaction_date
      t.float :transaction_amount
      t.string :transaction_currency
      t.string :seller_paypal_email
      t.string :customer_paypal_email
      t.string :paypal_payer_id
      t.string :product_number
      t.string :product_name
      t.string :product_type
      t.float :product_amount
      t.string :payment_type
      t.string :recurring_id
      t.datetime :next_rebill
      t.integer :recurring_times
      t.string :recurring_status
      t.integer :payment_number
      t.string :seller_id
      t.string :seller_email
      t.string :customer_firstname
      t.string :customer_lastname
      t.string :customer_address
      t.string :customer_state
      t.string :customer_city
      t.string :customer_country
      t.string :customer_email

      t.timestamps null: false
    end
  end
end
