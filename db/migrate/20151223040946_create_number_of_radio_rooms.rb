class CreateNumberOfRadioRooms < ActiveRecord::Migration
  def change
    create_table :number_of_radio_rooms do |t|
      t.integer :radio_rooms

      t.timestamps null: false
    end
  end
end
