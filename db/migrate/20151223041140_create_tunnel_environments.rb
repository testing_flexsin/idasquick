class CreateTunnelEnvironments < ActiveRecord::Migration
  def change
    create_table :tunnel_environments do |t|
      t.string :environment
      t.float :tunnel_indoor_factor

      t.timestamps null: false
    end
  end
end
