class CreateTunnelProductFrequencyChannels < ActiveRecord::Migration
  def change
    create_table :tunnel_product_frequency_channels do |t|
      t.integer :bda_product_category_id
      t.integer :number_of_channels
      t.float :composite_power
      t.float :channel_power
      t.float :papr
      t.boolean :default_selected, default: false

      t.timestamps null: false
    end
  end
end
