class CreateTunnelMaterialCategories < ActiveRecord::Migration
  def change
    create_table :tunnel_material_categories do |t|
      t.string :category

      t.timestamps null: false
    end
  end
end
