class CreateTunnelDonorDirectFeedQuantities < ActiveRecord::Migration
  def change
    create_table :tunnel_donor_direct_feed_quantities do |t|
      t.integer :tunnel_report_material_list_id
      t.text :description
      t.float :quantity

      t.timestamps null: false
    end
  end
end
