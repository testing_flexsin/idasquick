class CreateTunnelProductPriceLists < ActiveRecord::Migration
  def change
    create_table :tunnel_product_price_lists do |t|
      t.integer :tunnel_report_material_list_id
      t.string :vendor
      t.string :manufacturer
      t.string :mfg_pn
      t.string :our_pn
      t.text :description
      t.float :mfg_list_price
      t.float :our_cost
      t.float :discount_off_list
      t.float :our_sell_price

      t.timestamps null: false
    end
  end
end
