class CreateTunnelLinkBudgetAdmins < ActiveRecord::Migration
  def change
    create_table :tunnel_link_budget_admins do |t|
      t.float :indoor_margin

      t.timestamps null: false
    end
  end
end
