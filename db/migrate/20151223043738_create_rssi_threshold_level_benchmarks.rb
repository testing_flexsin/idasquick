class CreateRssiThresholdLevelBenchmarks < ActiveRecord::Migration
  def change
    create_table :rssi_threshold_level_benchmarks do |t|
      t.integer :threshold_level
      t.boolean :default_selected, default: false

      t.timestamps null: false
    end
  end
end
