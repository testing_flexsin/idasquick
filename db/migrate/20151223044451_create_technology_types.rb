class CreateTechnologyTypes < ActiveRecord::Migration
  def change
    create_table :technology_types do |t|
      t.string :technology
      t.float :papr
      t.boolean :default_selected, default: false

      t.timestamps null: false
    end
  end
end
