class CreateCommunicationTypes < ActiveRecord::Migration
  def change
    create_table :communication_types do |t|
      t.string :communication
      t.boolean :communication_assigned, default: false
      t.boolean :default_selected, default: false

      t.timestamps null: false
    end
  end
end
