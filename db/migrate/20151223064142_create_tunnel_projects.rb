class CreateTunnelProjects < ActiveRecord::Migration
  def change
    create_table :tunnel_projects do |t|
      t.integer :user_id
      t.string :user_name
      t.string :project_name
      t.string :company
      t.string :name
      t.string :facility_option
      t.integer :number_of_services
      t.integer :highest_frequency_band
      t.integer :tunnel_product_frequency_channel_id
      t.string :system_feed_method
      t.string :system_architecture
      t.integer :marketing_id
      t.integer :expected_rssi_at_mobile
      t.string :technology_type
      t.string :communication_type
      t.integer :step_completed
      t.boolean :report, default: false
      t.boolean :disclaimer_acknowledgement, default: false
      t.boolean :visited_report, default: false

      t.timestamps null: false
    end
  end
end
