class CreateTunnels < ActiveRecord::Migration
  def change
    create_table :tunnels do |t|
      t.integer :tunnel_project_id
      t.integer :number_of_tunnels
      t.string :have_floorplan

      t.timestamps null: false
    end
  end
end
