class CreateTunnelLinkBudgets < ActiveRecord::Migration
  def change
    create_table :tunnel_link_budgets do |t|
      t.integer :tunnel_data_entry_id
      t.float :channel_power
      t.float :frequency
      t.float :distribution_loss
      t.float :radiax_length
      t.float :radiating_cable_insertion_loss
      t.float :radiating_cable_coupling_loss
      t.float :distance_from_cable
      t.float :indoor_margin
      t.float :indoor_factor
      t.float :path_loss_from_cable
      t.float :signal_level_at_mobile

      t.timestamps null: false
    end
  end
end
