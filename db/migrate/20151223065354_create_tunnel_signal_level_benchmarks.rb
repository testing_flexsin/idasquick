class CreateTunnelSignalLevelBenchmarks < ActiveRecord::Migration
  def change
    create_table :tunnel_signal_level_benchmarks do |t|
      t.integer :tunnel_data_entry_id
      t.float :rssi_at_portable_in_tunnel
      t.float :threshold_level_benchmark
      t.float :threshold_overlimit_factor

      t.timestamps null: false
    end
  end
end
