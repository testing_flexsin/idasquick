class CreateTunnelDesignInformations < ActiveRecord::Migration
  def change
    create_table :tunnel_design_informations do |t|
      t.integer :tunnel_data_entry_id
      t.float :tunnel_length
      t.integer :number_of_radio_rooms
      t.float :average_distance_radio_rooms
      t.float :number_of_cable_runs
      t.string :cable_type
      t.float :total_cable_length
      t.string :bda_product_category
      t.float :number_of_bdas
      t.string :communication_type

      t.timestamps null: false
    end
  end
end
