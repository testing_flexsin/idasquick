class CreateTunnelPartQuantityInformations < ActiveRecord::Migration
  def change
    create_table :tunnel_part_quantity_informations do |t|
      t.integer :tunnel_data_entry_id
      t.integer :tunnel_report_material_list_id
      t.float :quantity

      t.timestamps null: false
    end
  end
end
