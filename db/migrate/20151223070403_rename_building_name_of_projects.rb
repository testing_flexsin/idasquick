class RenameBuildingNameOfProjects < ActiveRecord::Migration
  def change
  	rename_column :projects, :building_name, :name
  end
end
