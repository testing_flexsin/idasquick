class AddOnlineSearchOptionUrlToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :online_search_option_url, :text, default: " "
  end
end
