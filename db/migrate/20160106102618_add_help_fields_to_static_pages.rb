class AddHelpFieldsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :help_user_name, :string, default: " "
    add_column :static_pages, :help_project_name, :string, default: " "
    add_column :static_pages, :help_company, :string, default: " "
    add_column :static_pages, :help_name, :string, default: " "
    add_column :static_pages, :help_number_of_buildings, :string, default: " "
    add_column :static_pages, :help_number_of_tunnels, :string, default: " "
    add_column :static_pages, :help_have_floorplan, :string, default: " "
    add_column :static_pages, :help_number_of_services, :string, default: " "
    add_column :static_pages, :help_highest_frequency_band, :string, default: " "
    add_column :static_pages, :help_frequency_channels, :string, default: " "
    add_column :static_pages, :help_excepted_rssi_at_mobile, :string, default: " "
    add_column :static_pages, :help_materials_sourcing, :string, default: " "
  end
end
