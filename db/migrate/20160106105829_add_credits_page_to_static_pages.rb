class AddCreditsPageToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :credits, :text, default: " "
  end
end
