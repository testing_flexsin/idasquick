class AddTransactionHistoryToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :transaction_history, :text, default: " "
  end
end
