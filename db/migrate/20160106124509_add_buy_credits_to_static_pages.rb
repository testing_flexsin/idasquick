class AddBuyCreditsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :buy_credits, :text, default: " "
  end
end
