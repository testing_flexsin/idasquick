class ChangeColumnToStaticPages < ActiveRecord::Migration
  def change
    change_column :static_pages, :change_password, :text, default: " "
    change_column :static_pages, :building_facility_option, :text, default: " "
    change_column :static_pages, :tunnel_facility_option, :text, default: " "
    change_column :static_pages, :admin_email, :string, default: " "
  end
end
