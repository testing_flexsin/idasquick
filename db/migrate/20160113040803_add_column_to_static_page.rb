class AddColumnToStaticPage < ActiveRecord::Migration
  def change
    add_column :static_pages, :internal_server_error, :text, default: " "
    add_column :static_pages, :not_found, :text, default: " "
  end
end
