class AddNewFieldsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :show_report_heading, :text, default: " "
    add_column :static_pages, :active_material_spares, :string, default: " "
    add_column :static_pages, :filter_builder, :string, default: " "
    add_column :static_pages, :buy_quoted_materials_now, :string, default: " "
  end
end
