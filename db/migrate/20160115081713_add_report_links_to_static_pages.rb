class AddReportLinksToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :buttons_report_page, :text, default: " "
    remove_column :static_pages, :b2b_portal_access, :text
    remove_column :static_pages, :diagramming_tool, :text
    remove_column :static_pages, :active_material_spares, :text
    remove_column :static_pages, :filter_builder, :text
    remove_column :static_pages, :buy_quoted_materials_now, :text
    remove_column :static_pages, :online_search_option_url, :text
    remove_column :static_pages, :help, :text
  end
end
