class AddStaticDataEntriesColumnsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :help_building_name, :string, default: " "
    add_column :static_pages, :help_building_number, :string, default: " "
    add_column :static_pages, :help_number_of_floors, :string, default: " "
    add_column :static_pages, :help_quick_area_data_entry, :string, default: " "
    add_column :static_pages, :help_building_environment, :string, default: " "
    add_column :static_pages, :help_tunnel_name, :string, default: " "
    add_column :static_pages, :help_tunnel_number, :string, default: " "
    add_column :static_pages, :help_number_of_bores, :string, default: " "
    add_column :static_pages, :help_number_of_segments, :string, default: " "
    add_column :static_pages, :help_this_tunnel_bore, :string, default: " "
    add_column :static_pages, :help_this_tunnel_segment, :string, default: " "
    add_column :static_pages, :help_tunnel_environment, :string, default: " "
    add_column :static_pages, :help_number_of_stations, :string, default: " "
  end
end
