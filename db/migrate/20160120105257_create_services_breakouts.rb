class CreateServicesBreakouts < ActiveRecord::Migration
  def change
    create_table :services_breakouts do |t|
      t.integer :number_of_services
      t.integer :number_of_bdas_donor
      t.integer :number_of_bdas_das
      t.integer :services_breakout_in_each_bda_box

      t.timestamps null: false
    end
  end
end
