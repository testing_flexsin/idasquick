class AddDeviseColumnsToStaticPages < ActiveRecord::Migration
  def change
  	add_column :static_pages, :heading_change_your_password, :string, default: " "
  	add_column :static_pages, :heading_forgot_your_password, :string, default: " "
  	add_column :static_pages, :heading_register_account, :string, default: " "
  	add_column :static_pages, :heading_login_details, :string, default: " "
  	add_column :static_pages, :heading_retrieve_forgotten_password, :string, default: " "
  	add_column :static_pages, :heading_resend_confirmation_instructions, :string, default: " "
  end
end
