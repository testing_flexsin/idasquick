class ChangeTypeOfServicesBreakouts < ActiveRecord::Migration
  def change
  	change_column :services_breakouts, :services_breakout_in_each_bda_box, :string
  end
end
