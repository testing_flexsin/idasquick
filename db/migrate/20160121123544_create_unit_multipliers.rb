class CreateUnitMultipliers < ActiveRecord::Migration
  def change
    create_table :unit_multipliers do |t|
      t.float :multiplied_factor

      t.timestamps null: false
    end
  end
end
