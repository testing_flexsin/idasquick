class AddNewColumnsToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :expected_rssi_at_mobile, :integer
    add_column :projects, :technology_type, :string
    add_column :projects, :papr, :float
    add_column :projects, :communication_type, :string
  end
end
