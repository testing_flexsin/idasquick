class AddCableTypeToTunnelLinkBudgets < ActiveRecord::Migration
  def change
    add_column :tunnel_link_budgets, :cable_type, :string
  end
end
