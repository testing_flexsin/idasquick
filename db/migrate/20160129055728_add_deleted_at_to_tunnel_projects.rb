class AddDeletedAtToTunnelProjects < ActiveRecord::Migration
  def change
    add_column :tunnel_projects, :deleted_at, :datetime
    add_index :tunnel_projects, :deleted_at
  end
end
