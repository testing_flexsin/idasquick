class CreateCredits < ActiveRecord::Migration
  def change
    create_table :credits do |t|
      t.integer :credits
      t.float :amount

      t.timestamps null: false
    end
  end
end
