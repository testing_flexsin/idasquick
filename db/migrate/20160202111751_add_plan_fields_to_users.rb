class AddPlanFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :building_plan_id, :integer
    add_column :users, :building_last_transaction_date, :datetime
    add_column :users, :tunnel_plan_id, :integer
    add_column :users, :tunnel_last_transaction_date, :datetime
    add_column :users, :active, :boolean, default: false
  end
end
