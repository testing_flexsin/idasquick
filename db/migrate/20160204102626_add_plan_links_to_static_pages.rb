class AddPlanLinksToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :building_basic_plan_link, :string, default: " "
    add_column :static_pages, :building_standard_plan_link, :string, default: " "
    add_column :static_pages, :building_professional_plan_link, :string, default: " "
    add_column :static_pages, :tunnel_basic_plan_link, :string, default: " "
    add_column :static_pages, :tunnel_standard_plan_link, :string, default: " "
    add_column :static_pages, :tunnel_professional_plan_link, :string, default: " "
  end
end
