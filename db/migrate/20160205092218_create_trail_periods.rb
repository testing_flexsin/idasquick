class CreateTrailPeriods < ActiveRecord::Migration
  def change
    create_table :trail_periods do |t|
      t.integer :period

      t.timestamps null: false
    end
  end
end
