class AddWelcomePageToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :welcome_page, :text
  end
end
