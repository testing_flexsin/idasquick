class AddCreditsDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :building_last_credits_date, :datetime
    add_column :users, :tunnel_last_credits_date, :datetime
  end
end
