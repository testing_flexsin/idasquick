class AddContactEmailToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :contact_email, :string
  end
end
