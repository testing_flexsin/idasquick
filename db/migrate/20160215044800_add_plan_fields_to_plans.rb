class AddPlanFieldsToPlans < ActiveRecord::Migration
  def change
    add_column :plans, :yearly, :boolean
    add_column :plans, :quote_data_storage, :integer
  end
end
