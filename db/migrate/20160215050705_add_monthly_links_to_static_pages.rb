class AddMonthlyLinksToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :building_basic_plan_monthly_link, :string
    add_column :static_pages, :building_standard_plan_monthly_link, :string
    add_column :static_pages, :building_professional_plan_monthly_link, :string
    add_column :static_pages, :tunnel_basic_plan_monthly_link, :string
    add_column :static_pages, :tunnel_standard_plan_monthly_link, :string
    add_column :static_pages, :tunnel_professional_plan_monthly_link, :string
  end
end
