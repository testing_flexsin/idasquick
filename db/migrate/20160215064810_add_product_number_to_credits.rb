class AddProductNumberToCredits < ActiveRecord::Migration
  def change
    add_column :credits, :product_number, :string
  end
end
