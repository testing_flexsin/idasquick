class AddQuickStartFooterToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :quick_start_footer, :text
  end
end
