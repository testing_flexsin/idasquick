class AddQuoteStorageDateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :building_quote_storage_date, :datetime
    add_column :users, :tunnel_quote_storage_date, :datetime
  end
end
