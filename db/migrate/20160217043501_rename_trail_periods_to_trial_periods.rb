class RenameTrailPeriodsToTrialPeriods < ActiveRecord::Migration
  def change
  	rename_table :trail_periods, :trial_periods
  end
end
