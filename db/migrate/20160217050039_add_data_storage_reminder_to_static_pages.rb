class AddDataStorageReminderToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :data_storage_reminder_email, :text, default: " "
  end
end
