class AddEnvironmentIdsToProjects < ActiveRecord::Migration
  def change
    add_column :building_data_entries, :building_environment_id, :integer
    add_column :tunnel_data_entries, :tunnel_environment_id, :integer
  end
end
