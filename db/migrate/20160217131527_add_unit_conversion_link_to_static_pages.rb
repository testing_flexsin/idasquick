class AddUnitConversionLinkToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :unit_conversion_link, :string
  end
end
