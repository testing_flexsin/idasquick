class AddPageHeadToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :page_head, :text, default: " "
  end
end
