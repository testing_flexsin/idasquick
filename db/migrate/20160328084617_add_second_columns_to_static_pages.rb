class AddSecondColumnsToStaticPages < ActiveRecord::Migration
  def change
    add_column :static_pages, :building_band_and_services_second, :text, default: " "
    add_column :static_pages, :tunnel_band_and_services_second, :text, default: " "
  end
end
