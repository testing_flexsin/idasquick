puts "Create Admin User"
AdminUser.create!(email: 'admin@idasquick.com', password: 'password', password_confirmation: 'password')
puts "Done"

puts "Creating Number of Buildings"
NumberOfBuilding.create!([
  {buildings: 1},
  {buildings: 2},
  {buildings: 3},
  {buildings: 4},
  {buildings: 5},
  {buildings: 6},
  {buildings: 7},
  {buildings: 8},
  {buildings: 9},
  {buildings: 10}
])
puts "Done"

puts "Creating Number of Floors"
NumberOfFloor.create!([
  {floors: 1},
  {floors: 2},
  {floors: 3},
  {floors: 4},
  {floors: 5},
  {floors: 6},
  {floors: 7},
  {floors: 8},
  {floors: 9},
  {floors: 10},
  {floors: 11},
  {floors: 12},
  {floors: 13},
  {floors: 14},
  {floors: 15},
  {floors: 16},
  {floors: 17},
  {floors: 18},
  {floors: 19},
  {floors: 20},
  {floors: 21},
  {floors: 22},
  {floors: 23},
  {floors: 24},
  {floors: 25},
  {floors: 26},
  {floors: 27},
  {floors: 28},
  {floors: 29},
  {floors: 30},
  {floors: 31},
  {floors: 32},
  {floors: 33},
  {floors: 34},
  {floors: 35},
  {floors: 36},
  {floors: 37},
  {floors: 38},
  {floors: 39},
  {floors: 40},
  {floors: 41},
  {floors: 42},
  {floors: 43},
  {floors: 44},
  {floors: 45},
  {floors: 46},
  {floors: 47},
  {floors: 48},
  {floors: 49},
  {floors: 50},
  {floors: 51},
  {floors: 52},
  {floors: 53},
  {floors: 54},
  {floors: 55},
  {floors: 56},
  {floors: 57},
  {floors: 58},
  {floors: 59},
  {floors: 60},
  {floors: 61},
  {floors: 62},
  {floors: 63},
  {floors: 64},
  {floors: 65},
  {floors: 66},
  {floors: 67},
  {floors: 68},
  {floors: 69},
  {floors: 70},
  {floors: 71},
  {floors: 72},
  {floors: 73},
  {floors: 74},
  {floors: 75},
  {floors: 76},
  {floors: 77},
  {floors: 78},
  {floors: 79},
  {floors: 80},
  {floors: 81},
  {floors: 82},
  {floors: 83},
  {floors: 84},
  {floors: 85},
  {floors: 86},
  {floors: 87},
  {floors: 88},
  {floors: 89},
  {floors: 90},
  {floors: 91},
  {floors: 92},
  {floors: 93},
  {floors: 94},
  {floors: 95},
  {floors: 96},
  {floors: 97},
  {floors: 98},
  {floors: 99},
  {floors: 100},
  {floors: 101},
  {floors: 102},
  {floors: 103},
  {floors: 104},
  {floors: 105},
  {floors: 106},
  {floors: 107},
  {floors: 108},
  {floors: 109},
  {floors: 110},
  {floors: 111},
  {floors: 112},
  {floors: 113},
  {floors: 114},
  {floors: 115},
  {floors: 116},
  {floors: 117},
  {floors: 118},
  {floors: 119},
  {floors: 120},
  {floors: 121},
  {floors: 122},
  {floors: 123},
  {floors: 124},
  {floors: 125},
  {floors: 126},
  {floors: 127},
  {floors: 128},
  {floors: 129},
  {floors: 130},
  {floors: 131},
  {floors: 132},
  {floors: 133},
  {floors: 134},
  {floors: 135},
  {floors: 136},
  {floors: 137},
  {floors: 138},
  {floors: 139},
  {floors: 140},
  {floors: 141},
  {floors: 142},
  {floors: 143},
  {floors: 144},
  {floors: 145},
  {floors: 146},
  {floors: 147},
  {floors: 148},
  {floors: 149},
  {floors: 150},
  {floors: 151},
  {floors: 152},
  {floors: 153},
  {floors: 154},
  {floors: 155},
  {floors: 156},
  {floors: 157},
  {floors: 158},
  {floors: 159},
  {floors: 160},
  {floors: 161},
  {floors: 162},
  {floors: 163},
  {floors: 164},
  {floors: 165},
  {floors: 166},
  {floors: 167},
  {floors: 168},
  {floors: 169},
  {floors: 170},
  {floors: 171},
  {floors: 172},
  {floors: 173},
  {floors: 174},
  {floors: 175},
  {floors: 176},
  {floors: 177},
  {floors: 178},
  {floors: 179},
  {floors: 180},
  {floors: 181},
  {floors: 182},
  {floors: 183},
  {floors: 184},
  {floors: 185},
  {floors: 186},
  {floors: 187},
  {floors: 188},
  {floors: 189},
  {floors: 190},
  {floors: 191},
  {floors: 192},
  {floors: 193},
  {floors: 194},
  {floors: 195},
  {floors: 196},
  {floors: 197},
  {floors: 198},
  {floors: 199},
  {floors: 200},
  {floors: 201},
  {floors: 202},
  {floors: 203},
  {floors: 204},
  {floors: 205},
  {floors: 206},
  {floors: 207},
  {floors: 208},
  {floors: 209},
  {floors: 210},
  {floors: 211},
  {floors: 212},
  {floors: 213},
  {floors: 214},
  {floors: 215},
  {floors: 216},
  {floors: 217},
  {floors: 218},
  {floors: 219},
  {floors: 220}
])
puts "Done"

puts "Creating Number of Services"
NumberOfService.create!([
  {services: 1},
  {services: 2},
  {services: 3},
  {services: 4},
  {services: 5},
  {services: 6},
  {services: 7},
  {services: 8},
  {services: 9},
  {services: 10},
  {services: 11},
  {services: 12},
  {services: 13},
  {services: 14},
  {services: 15},
  {services: 16},
  {services: 17},
  {services: 18},
  {services: 19},
  {services: 20}
])
puts "Done"

puts "Creating Number of Tunnels"
NumberOfTunnel.create!([
  {tunnels: 1},
  {tunnels: 2},
  {tunnels: 3},
  {tunnels: 4},
  {tunnels: 5},
  {tunnels: 6},
  {tunnels: 7},
  {tunnels: 8},
  {tunnels: 9},
  {tunnels: 10}
])
puts "Done"

puts "Creating Number of Bores"
NumberOfBore.create!([
  {bores: 1},
  {bores: 2},
  {bores: 3},
  {bores: 4},
  {bores: 5},
  {bores: 6},
  {bores: 7},
  {bores: 8},
  {bores: 9},
  {bores: 10}
])
puts "Done"

puts "Creating Number of Segments"
NumberOfSegment.create!([
  {segments: 1},
  {segments: 2},
  {segments: 3},
  {segments: 4},
  {segments: 5},
  {segments: 6},
  {segments: 7},
  {segments: 8},
  {segments: 9},
  {segments: 10},
  {segments: 11},
  {segments: 12},
  {segments: 13},
  {segments: 14},
  {segments: 15},
  {segments: 16},
  {segments: 17},
  {segments: 18},
  {segments: 19},
  {segments: 20},
  {segments: 21},
  {segments: 22},
  {segments: 23},
  {segments: 24},
  {segments: 25},
  {segments: 26},
  {segments: 27},
  {segments: 28},
  {segments: 29},
  {segments: 30},
  {segments: 31},
  {segments: 32},
  {segments: 33},
  {segments: 34},
  {segments: 35},
  {segments: 36},
  {segments: 37},
  {segments: 38},
  {segments: 39},
  {segments: 40},
  {segments: 41},
  {segments: 42},
  {segments: 43},
  {segments: 44},
  {segments: 45},
  {segments: 46},
  {segments: 47},
  {segments: 48},
  {segments: 49},
  {segments: 50}
])
puts "Done"

puts "Creating Number of Radio Rooms"
NumberOfRadioRoom.create!([
  {radio_rooms: 1},
  {radio_rooms: 2},
  {radio_rooms: 3},
  {radio_rooms: 4},
  {radio_rooms: 5},
  {radio_rooms: 6},
  {radio_rooms: 7},
  {radio_rooms: 8},
  {radio_rooms: 9},
  {radio_rooms: 10},
  {radio_rooms: 11},
  {radio_rooms: 12},
  {radio_rooms: 13},
  {radio_rooms: 14},
  {radio_rooms: 15},
  {radio_rooms: 16},
  {radio_rooms: 17},
  {radio_rooms: 18},
  {radio_rooms: 19},
  {radio_rooms: 20},
  {radio_rooms: 21},
  {radio_rooms: 22},
  {radio_rooms: 23},
  {radio_rooms: 24},
  {radio_rooms: 25},
  {radio_rooms: 26},
  {radio_rooms: 27},
  {radio_rooms: 28},
  {radio_rooms: 29},
  {radio_rooms: 30},
  {radio_rooms: 31},
  {radio_rooms: 32},
  {radio_rooms: 33},
  {radio_rooms: 34},
  {radio_rooms: 35},
  {radio_rooms: 36},
  {radio_rooms: 37},
  {radio_rooms: 38},
  {radio_rooms: 39},
  {radio_rooms: 40},
  {radio_rooms: 41},
  {radio_rooms: 42},
  {radio_rooms: 43},
  {radio_rooms: 44},
  {radio_rooms: 45},
  {radio_rooms: 46},
  {radio_rooms: 47},
  {radio_rooms: 48},
  {radio_rooms: 49},
  {radio_rooms: 50},
  {radio_rooms: 51},
  {radio_rooms: 52},
  {radio_rooms: 53},
  {radio_rooms: 54},
  {radio_rooms: 55},
  {radio_rooms: 56},
  {radio_rooms: 57},
  {radio_rooms: 58},
  {radio_rooms: 59},
  {radio_rooms: 60},
  {radio_rooms: 61},
  {radio_rooms: 62},
  {radio_rooms: 63},
  {radio_rooms: 64},
  {radio_rooms: 65},
  {radio_rooms: 66},
  {radio_rooms: 67},
  {radio_rooms: 68},
  {radio_rooms: 69},
  {radio_rooms: 70},
  {radio_rooms: 71},
  {radio_rooms: 72},
  {radio_rooms: 73},
  {radio_rooms: 74},
  {radio_rooms: 75},
  {radio_rooms: 76},
  {radio_rooms: 77},
  {radio_rooms: 78},
  {radio_rooms: 79},
  {radio_rooms: 80},
  {radio_rooms: 81},
  {radio_rooms: 82},
  {radio_rooms: 83},
  {radio_rooms: 84},
  {radio_rooms: 85},
  {radio_rooms: 86},
  {radio_rooms: 87},
  {radio_rooms: 88},
  {radio_rooms: 89},
  {radio_rooms: 90},
  {radio_rooms: 91},
  {radio_rooms: 92},
  {radio_rooms: 93},
  {radio_rooms: 94},
  {radio_rooms: 95},
  {radio_rooms: 96},
  {radio_rooms: 97},
  {radio_rooms: 98},
  {radio_rooms: 99},
  {radio_rooms: 100}
])
puts "Done"

puts "Creating Highest Frequency Bands"
HighestFrequencyBand.create!([
  {frequency: 5},
  {frequency: 10},
  {frequency: 20},
  {frequency: 30},
  {frequency: 50},
  {frequency: 88},
  {frequency: 100},
  {frequency: 108},
  {frequency: 150},
  {frequency: 174},
  {frequency: 200},
  {frequency: 300},
  {frequency: 400},
  {frequency: 450},
  {frequency: 500},
  {frequency: 512},
  {frequency: 600},
  {frequency: 700},
  {frequency: 800},
  {frequency: 824},
  {frequency: 894},
  {frequency: 960},
  {frequency: 1000},
  {frequency: 1250},
  {frequency: 1500},
  {frequency: 1700},
  {frequency: 1950},
  {frequency: 2000},
  {frequency: 2300},
  {frequency: 2400},
  {frequency: 3000},
  {frequency: 3300},
  {frequency: 3400},
  {frequency: 4000},
  {frequency: 4900},
  {frequency: 5000},
  {frequency: 5200},
  {frequency: 5300},
  {frequency: 5600},
  {frequency: 6000}
])
puts "Done"

puts "Creating BDA Product Categories"
BdaProductCategory.create!([
  {category: "Low Power"},
  {category: "Medium Power"},
  {category: "High Power"}
])
puts "Done"

puts "Creating Regions"
Region.create!([
  {name: "CALA"},
  {name: "Europe"},
  {name: "Russia & CIS"},
  {name: "Middle East"},
  {name: "Africa"},
  {name: "Asia/Pacific"},
  {name: "North America-USA"},
  {name: "North America-Canada"}
])
puts "Done"

puts "Creating Material Categories"
MaterialCategory.create!([
  {category: "IN-BUILDING DAS"},
  {category: "BI-DIRECTIONAL AMP"},
  {category: "DIRECT FEED"},
  {category: "FIBER OPTIC"},
  {category: "OFF-AIR DONOR"},
  {category: "TUNNEL DAS"}
])
puts "Done"

puts "Creating Passive Component Losses"
PassiveComponentLoss.create!([
  {jumper_loss: 0.5, connector_loss: 0.1, way2_splitter_loss: 3.5, way3_splitter_loss: 5.5, way4_splitter_loss: 6.5, way6_splitter_loss: 8.2, way8_splitter_loss: 9.5, directional_coupler_loss: 0.8, hybrid_coupler_loss: 3.5, quantity_of_splitters: 3, type_of_splitter: "way2_splitter_loss"}
])
puts "Done"

puts "Creating Timer Settings"
TimerSetting.create!([
  {plan_name: "Basic", report_download_timer: 15, report_generate_timer: 15},
  {plan_name: "Standard", report_download_timer: 10, report_generate_timer: 10},
  {plan_name: "Professional", report_download_timer: 5, report_generate_timer: 5}
])
puts "Done"

puts "Creating Services Breakout"
ServicesBreakout.create!([
  {number_of_services: 1, number_of_bdas_donor: 1, number_of_bdas_das: 1, services_breakout_in_each_bda_box: "1"},
  {number_of_services: 2, number_of_bdas_donor: 1, number_of_bdas_das: 1, services_breakout_in_each_bda_box: "2"},
  {number_of_services: 3, number_of_bdas_donor: 2, number_of_bdas_das: 2, services_breakout_in_each_bda_box: "2+1"},
  {number_of_services: 4, number_of_bdas_donor: 2, number_of_bdas_das: 2, services_breakout_in_each_bda_box: "2+2"},
  {number_of_services: 5, number_of_bdas_donor: 3, number_of_bdas_das: 3, services_breakout_in_each_bda_box: "2+2+1"},
  {number_of_services: 6, number_of_bdas_donor: 3, number_of_bdas_das: 3, services_breakout_in_each_bda_box: "2+2+2"},
  {number_of_services: 7, number_of_bdas_donor: 4, number_of_bdas_das: 4, services_breakout_in_each_bda_box: "2+2+2+1"},
  {number_of_services: 8, number_of_bdas_donor: 4, number_of_bdas_das: 4, services_breakout_in_each_bda_box: "2+2+2+2"},
  {number_of_services: 9, number_of_bdas_donor: 5, number_of_bdas_das: 5, services_breakout_in_each_bda_box: "2+2+2+2+1"},
  {number_of_services: 10, number_of_bdas_donor: 5, number_of_bdas_das: 5, services_breakout_in_each_bda_box: "2+2+2+2+2"},
  {number_of_services: 11, number_of_bdas_donor: 6, number_of_bdas_das: 6, services_breakout_in_each_bda_box: "2+2+2+2+2+1"},
  {number_of_services: 12, number_of_bdas_donor: 6, number_of_bdas_das: 6, services_breakout_in_each_bda_box: "2+2+2+2+2+2"},
  {number_of_services: 13, number_of_bdas_donor: 7, number_of_bdas_das: 7, services_breakout_in_each_bda_box: "2+2+2+2+2+2+1"},
  {number_of_services: 14, number_of_bdas_donor: 7, number_of_bdas_das: 7, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2"},
  {number_of_services: 15, number_of_bdas_donor: 8, number_of_bdas_das: 8, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+1"},
  {number_of_services: 16, number_of_bdas_donor: 8, number_of_bdas_das: 8, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+2"},
  {number_of_services: 17, number_of_bdas_donor: 9, number_of_bdas_das: 9, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+2+1"},
  {number_of_services: 18, number_of_bdas_donor: 9, number_of_bdas_das: 9, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+2+2"},
  {number_of_services: 19, number_of_bdas_donor: 10, number_of_bdas_das: 10, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+2+2+1"},
  {number_of_services: 20, number_of_bdas_donor: 10, number_of_bdas_das: 10, services_breakout_in_each_bda_box: "2+2+2+2+2+2+2+2+2+2"}
])
puts "Done"

puts "Creating Splitter Matrices"
(1..1000).each do |number_of_bdas|
  (1..14).each do |antennas|
    if number_of_bdas % 2 == 0
      case antennas
      when 1
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 2
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 3
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 4
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 5
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 6
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 7
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 8
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 9
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 6.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 10
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 5.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 11
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 3.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 12
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 4.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 13
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 14
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      end
    else
      case antennas
      when 1
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 2
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 3
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 0.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 4
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 0.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 5
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 6
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 1.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 7
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 8
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 9
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 6.0, product_type_3w: 1.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 10
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 5.0, product_type_3w: 2.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 11
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 4.0, product_type_3w: 3.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 12
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 4.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 13
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 2.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      when 14
        SplitterMatrix.create!(antennas: antennas, number_of_bdas: number_of_bdas, product_type_2w: 3.0, product_type_3w: 5.0, product_type_4w: 0.0, product_type_6w: 0.0, product_type_8w: 0.0, dc: 0.0, hc: 0.0)
      end
    end
  end
end
puts "Done"

puts "Creating Tunnel Splitter Matrices"
TunnelSplitterMatrix.create!([
  {number_of_bdas: 1, product_type_2w: 1.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 2, product_type_2w: 2.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 3, product_type_2w: 3.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 4, product_type_2w: 4.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 5, product_type_2w: 5.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 6, product_type_2w: 6.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 7, product_type_2w: 7.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 8, product_type_2w: 8.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 9, product_type_2w: 9.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 10, product_type_2w: 10.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 11, product_type_2w: 11.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 12, product_type_2w: 12.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 13, product_type_2w: 13.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 14, product_type_2w: 14.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 15, product_type_2w: 15.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 16, product_type_2w: 16.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 17, product_type_2w: 17.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 18, product_type_2w: 18.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 19, product_type_2w: 19.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 20, product_type_2w: 20.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 21, product_type_2w: 21.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 22, product_type_2w: 22.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 23, product_type_2w: 23.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 24, product_type_2w: 24.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 25, product_type_2w: 25.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 26, product_type_2w: 26.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 27, product_type_2w: 27.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 28, product_type_2w: 28.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 29, product_type_2w: 29.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 30, product_type_2w: 30.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 31, product_type_2w: 31.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 32, product_type_2w: 32.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 33, product_type_2w: 33.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 34, product_type_2w: 34.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 35, product_type_2w: 35.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 36, product_type_2w: 36.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 37, product_type_2w: 37.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 38, product_type_2w: 38.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 39, product_type_2w: 39.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 40, product_type_2w: 40.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 41, product_type_2w: 41.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 42, product_type_2w: 42.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 43, product_type_2w: 43.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 44, product_type_2w: 44.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 45, product_type_2w: 45.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 46, product_type_2w: 46.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 47, product_type_2w: 47.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 48, product_type_2w: 48.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 49, product_type_2w: 49.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
  {number_of_bdas: 50, product_type_2w: 50.0, product_type_3w: 0.0, product_type_4w: 0.0, dc: 0.0, config_per_bda: nil},
])
puts "Done"

puts "Creating Unit Multiplier"
UnitMultiplier.create!([
  {multiplied_factor: 1.6}
])
puts "Done"

puts "Creating Tunnel Length"
TunnelLength.create!([
  {length: 2640.0}
])
puts "Done"

puts "Creating Cable Losses"
CableLoss.create!([
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 0.00676, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 0.00722, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 0.00751, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 0.00889, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 0.0096, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 0.0103, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 0.0127, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 0.0148, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 0.0157, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 0.0166, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 0.0168, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 0.0183, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 0.0199, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 0.0213, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 0.0219, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 0.0226, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 0.0235, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 0.024, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 0.0271, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 0.0299, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 0.032, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 0.0345, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 0.035, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 0.0376, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 0.0392, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 0.0438, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 0.0452, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 0.0481, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 0.0545, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 0.0658, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 0.0628, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 0.0654, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 0.061, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 0.0609, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 0.0638, cable_loss_7_by_8: nil, cable_loss_11_by_4: nil, cable_loss_15_by_8: nil}
])
puts "Done"

puts "Creating Technology Types"
TechnologyType.create!([
  {technology: "DMR", papr: 3.0, default_selected: false},
  {technology: "NXDN", papr: 3.0, default_selected: false},
  {technology: "UMTS/HSPA", papr: 10.0, default_selected: false},
  {technology: "Analog FM", papr: 0.0, default_selected: true},
  {technology: "TDMA", papr: 3.0, default_selected: false},
  {technology: "CW", papr: 3.0, default_selected: false},
  {technology: "GSM", papr: 0.0, default_selected: false},
  {technology: "CDMA", papr: 10.6, default_selected: false},
  {technology: "P25", papr: 3.3, default_selected: false},
  {technology: "TETRA", papr: 3.3, default_selected: false},
  {technology: "IDEN", papr: 3.7, default_selected: false},
  {technology: "LTE (SISO)", papr: 12.0, default_selected: false},
  {technology: "TETRAPOL", papr: 3.3, default_selected: false},
  {technology: "Wi-Fi (SISO)", papr: 12.0, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Product Frequency Channels"
TunnelProductFrequencyChannel.create!([
  {bda_product_category_id: 1, number_of_channels: 1, composite_power: 30.0, channel_power: 27.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 2, composite_power: 30.0, channel_power: 24.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 3, composite_power: 30.0, channel_power: 22.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 4, composite_power: 30.0, channel_power: 21.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 5, composite_power: 30.0, channel_power: 20.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 6, composite_power: 30.0, channel_power: 19.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 7, composite_power: 30.0, channel_power: 18.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 8, composite_power: 30.0, channel_power: 18.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 9, composite_power: 30.0, channel_power: 17.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 10, composite_power: 30.0, channel_power: 17.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 11, composite_power: 30.0, channel_power: 16.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 12, composite_power: 30.0, channel_power: 16.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 13, composite_power: 30.0, channel_power: 15.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 14, composite_power: 30.0, channel_power: 15.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 1, number_of_channels: 15, composite_power: 30.0, channel_power: 15.2, papr: 3.0, default_selected: true},
  {bda_product_category_id: 2, number_of_channels: 16, composite_power: 34.0, channel_power: 18.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 17, composite_power: 34.0, channel_power: 17.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 18, composite_power: 34.0, channel_power: 17.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 19, composite_power: 34.0, channel_power: 17.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 20, composite_power: 34.0, channel_power: 17.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 21, composite_power: 34.0, channel_power: 16.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 22, composite_power: 34.0, channel_power: 16.6, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 23, composite_power: 34.0, channel_power: 16.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 24, composite_power: 34.0, channel_power: 16.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 25, composite_power: 34.0, channel_power: 16.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 26, composite_power: 34.0, channel_power: 15.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 27, composite_power: 34.0, channel_power: 15.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 28, composite_power: 34.0, channel_power: 15.5, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 29, composite_power: 34.0, channel_power: 15.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 2, number_of_channels: 30, composite_power: 34.0, channel_power: 15.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 31, composite_power: 37.0, channel_power: 15.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 32, composite_power: 37.0, channel_power: 18.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 33, composite_power: 37.0, channel_power: 18.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 34, composite_power: 37.0, channel_power: 18.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 35, composite_power: 37.0, channel_power: 18.6, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 36, composite_power: 37.0, channel_power: 18.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 37, composite_power: 37.0, channel_power: 18.3, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 38, composite_power: 37.0, channel_power: 18.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 39, composite_power: 37.0, channel_power: 18.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 40, composite_power: 37.0, channel_power: 18.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 41, composite_power: 37.0, channel_power: 17.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 42, composite_power: 37.0, channel_power: 17.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 43, composite_power: 37.0, channel_power: 17.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 44, composite_power: 37.0, channel_power: 17.6, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 45, composite_power: 37.0, channel_power: 17.5, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 46, composite_power: 37.0, channel_power: 17.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 47, composite_power: 37.0, channel_power: 17.3, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 48, composite_power: 37.0, channel_power: 17.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 49, composite_power: 37.0, channel_power: 17.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 50, composite_power: 37.0, channel_power: 17.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 51, composite_power: 37.0, channel_power: 16.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 52, composite_power: 37.0, channel_power: 16.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 53, composite_power: 37.0, channel_power: 16.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 54, composite_power: 37.0, channel_power: 16.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 55, composite_power: 37.0, channel_power: 16.6, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 56, composite_power: 37.0, channel_power: 16.5, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 57, composite_power: 37.0, channel_power: 16.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 58, composite_power: 37.0, channel_power: 16.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 59, composite_power: 37.0, channel_power: 16.3, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 60, composite_power: 37.0, channel_power: 16.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 61, composite_power: 37.0, channel_power: 16.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 62, composite_power: 37.0, channel_power: 16.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 63, composite_power: 37.0, channel_power: 16.0, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 64, composite_power: 37.0, channel_power: 15.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 65, composite_power: 37.0, channel_power: 15.9, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 66, composite_power: 37.0, channel_power: 15.8, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 67, composite_power: 37.0, channel_power: 15.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 68, composite_power: 37.0, channel_power: 15.7, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 69, composite_power: 37.0, channel_power: 15.6, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 70, composite_power: 37.0, channel_power: 15.5, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 71, composite_power: 37.0, channel_power: 15.5, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 72, composite_power: 37.0, channel_power: 15.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 73, composite_power: 37.0, channel_power: 15.4, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 74, composite_power: 37.0, channel_power: 15.3, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 75, composite_power: 37.0, channel_power: 15.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 76, composite_power: 37.0, channel_power: 15.2, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 77, composite_power: 37.0, channel_power: 15.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 78, composite_power: 37.0, channel_power: 15.1, papr: 0.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 79, composite_power: 37.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 80, composite_power: 37.0, channel_power: 15.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 81, composite_power: 37.0, channel_power: 14.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 82, composite_power: 37.0, channel_power: 14.9, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 83, composite_power: 37.0, channel_power: 14.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 84, composite_power: 37.0, channel_power: 14.8, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 85, composite_power: 37.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 86, composite_power: 37.0, channel_power: 14.7, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 87, composite_power: 37.0, channel_power: 14.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 88, composite_power: 37.0, channel_power: 14.6, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 89, composite_power: 37.0, channel_power: 14.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 90, composite_power: 37.0, channel_power: 14.5, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 91, composite_power: 37.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 92, composite_power: 37.0, channel_power: 14.4, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 93, composite_power: 37.0, channel_power: 14.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 94, composite_power: 37.0, channel_power: 14.3, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 95, composite_power: 37.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 96, composite_power: 37.0, channel_power: 14.2, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 97, composite_power: 37.0, channel_power: 14.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 98, composite_power: 37.0, channel_power: 14.1, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 99, composite_power: 37.0, channel_power: 14.0, papr: 3.0, default_selected: false},
  {bda_product_category_id: 3, number_of_channels: 100, composite_power: 37.0, channel_power: 14.0, papr: 3.0, default_selected: false}
])
puts "Done"

puts "Creating Cable Type Matrices"
CableTypeMatrix.create!([
  {radiating_cable_type: "cable_loss_1_by_2", cable_type_assigned: true},
  {radiating_cable_type: "cable_loss_7_by_8", cable_type_assigned: false},
  {radiating_cable_type: "cable_loss_11_by_4", cable_type_assigned: false},
  {radiating_cable_type: "cable_loss_15_by_8", cable_type_assigned: false}
])
puts "Done"

puts "Creating Tunnel Material Categories"
TunnelMaterialCategory.create!([
  {category: "BI-DIRECTIONAL AMP"},
  {category: "DIRECT FEED"},
  {category: "FIBER OPTIC"},
  {category: "OFF-AIR DONOR"},
  {category: "TUNNEL DAS"}
])
puts "Done"

puts "Creating Marketings"
Marketing.create!([
  {region_id: 1, vendor_name: "Vendor 1"},
  {region_id: 1, vendor_name: "Vendor 2"},
  {region_id: 1, vendor_name: "Vendor 3"},
  {region_id: 1, vendor_name: "Vendor 4"},
  {region_id: 1, vendor_name: "Vendor 5"},
  {region_id: 1, vendor_name: "Vendor 6"},
  {region_id: 2, vendor_name: "Vendor 1"},
  {region_id: 2, vendor_name: "Vendor 2"},
  {region_id: 3, vendor_name: "Vendor 1"},
  {region_id: 3, vendor_name: "Vendor 2"},
  {region_id: 3, vendor_name: "Vendor 3"},
  {region_id: 3, vendor_name: "Vendor 4"},
  {region_id: 3, vendor_name: "Vendor 5"},
  {region_id: 3, vendor_name: "Vendor 6"},
  {region_id: 3, vendor_name: "Vendor 7"},
  {region_id: 3, vendor_name: "Vendor 8"},
  {region_id: 4, vendor_name: "Vendor 1"},
  {region_id: 4, vendor_name: "Vendor 2"},
  {region_id: 5, vendor_name: "Vendor 1"},
  {region_id: 5, vendor_name: "Vendor 2"},
  {region_id: 5, vendor_name: "Vendor 3"},
  {region_id: 5, vendor_name: "Vendor 4"},
  {region_id: 5, vendor_name: "Vendor 5"},
  {region_id: 6, vendor_name: "Vendor 1"},
  {region_id: 6, vendor_name: "Vendor 2"},
  {region_id: 6, vendor_name: "Vendor 3"},
  {region_id: 6, vendor_name: "Vendor 4"},
  {region_id: 6, vendor_name: "Vendor 5"},
  {region_id: 7, vendor_name: "Vendor 1"},
  {region_id: 7, vendor_name: "Vendor 2"},
  {region_id: 7, vendor_name: "Vendor 3"},
  {region_id: 7, vendor_name: "Vendor 4"},
  {region_id: 7, vendor_name: "Vendor 5"},
  {region_id: 7, vendor_name: "Vendor 6"},
  {region_id: 7, vendor_name: "Vendor 7"},
  {region_id: 8, vendor_name: "Vendor 1"},
  {region_id: 8, vendor_name: "Vendor 2"},
  {region_id: 8, vendor_name: "Vendor 3"}
])
puts "Done"

puts "Creating Report Material Lists"
ReportMaterialList.create!([
  {material_category_id: 1, description: "1/2 inch plenum rated coaxial cable (ft)"},
  {material_category_id: 1, description: "Crossband Coupler, N-type connectors"},
  {material_category_id: 1, description: "Directional Coupler (broadband), N-type connectors"},
  {material_category_id: 1, description: "4 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "3 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "2 way splitters (broadband), N-type connectors"},
  {material_category_id: 1, description: "Hybrid Combiner, N-type connectors"},
  {material_category_id: 1, description: "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)"},
  {material_category_id: 1, description: "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)"},
  {material_category_id: 1, description: "InBuilding Antenna (broadband), N-type connectors"},
  {material_category_id: 1, description: "Custom Built Filtering, N-type connectors"},
  {material_category_id: 2, description: "Low Power (1 Watt Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "High Power (5 Watts Composite Power) BDA Unit Connected to DAS"},
  {material_category_id: 2, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to OFF-AIR DONOR"},
  {material_category_id: 3, description: "Hybrid Combiner, N-type connectors"},
  {material_category_id: 3, description: "Custom Built Filtering, N-type connectors"},
  {material_category_id: 3, description: "Base Station Interface (Point of Interconnect)"},
  {material_category_id: 4, description: "Master Fiber Optic Transceiver (Single Port)"},
  {material_category_id: 4, description: "Master Fiber Optic Transceiver (8 Fiber Optic Ports)"},
  {material_category_id: 4, description: "Slave Fiber Optic Transceiver (Single Port)"},
  {material_category_id: 4, description: "Fiber Optic Jumpers (10ft)"},
  {material_category_id: 4, description: "Fiber Optic Backbone Cable (ft)"},
  {material_category_id: 5, description: "Off-air donor antenna, Yagi, minimum 10dBi Gain"},
  {material_category_id: 5, description: "DC Block Donor, Coaxial Line Protector (N-Female connectors)"},
  {material_category_id: 5, description: "1/2 inch Off-air donor antenna cable (ft)"},
  {material_category_id: 5, description: "N-Male connectors for off-air donor antenna cables"},
  {material_category_id: 5, description: "Ground Kit for 1/2 inch Coax (for off-air donor antenna cable outer conductor)"},
  {material_category_id: 5, description: "Universal Weatherproofing Kit (for off-air donor antenna cable connectors)"},
  {material_category_id: 5, description: "Prep Tool for 1/2 inch donor cable (all-in-one strip tool)"}
])
puts "Done"

puts "Creating Square Footage Size"
SquareFootageSize.create!([
  {square_foot: 500000.0}
])
puts "Done"

puts "Creating Tunnel Cable I Losses"
TunnelCableILoss.create!([
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 5.01, cable_loss_7_by_8: 1.78, cable_loss_11_by_4: 1.26, cable_loss_15_by_8: 0.91, default_selected: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 5.05, cable_loss_7_by_8: 1.81, cable_loss_11_by_4: 1.27, cable_loss_15_by_8: 0.93, default_selected: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 5.08, cable_loss_7_by_8: 1.83, cable_loss_11_by_4: 1.29, cable_loss_15_by_8: 0.94, default_selected: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 3.6, cable_loss_7_by_8: 1.31, cable_loss_11_by_4: 0.92, cable_loss_15_by_8: 0.79, default_selected: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 5.32, cable_loss_7_by_8: 1.99, cable_loss_11_by_4: 1.4, cable_loss_15_by_8: 1.04, default_selected: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 5.41, cable_loss_7_by_8: 2.06, cable_loss_11_by_4: 1.44, cable_loss_15_by_8: 1.09, default_selected: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 5.77, cable_loss_7_by_8: 2.3, cable_loss_11_by_4: 1.6, cable_loss_15_by_8: 1.24, default_selected: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 6.12, cable_loss_7_by_8: 2.55, cable_loss_11_by_4: 1.77, cable_loss_15_by_8: 1.4, default_selected: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 6.1, cable_loss_7_by_8: 2.56, cable_loss_11_by_4: 1.77, cable_loss_15_by_8: 1.51, default_selected: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 6.5, cable_loss_7_by_8: 2.72, cable_loss_11_by_4: 1.89, cable_loss_15_by_8: 1.61, default_selected: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 6.52, cable_loss_7_by_8: 2.83, cable_loss_11_by_4: 1.95, cable_loss_15_by_8: 1.58, default_selected: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 6.84, cable_loss_7_by_8: 3.04, cable_loss_11_by_4: 2.1, cable_loss_15_by_8: 1.72, default_selected: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 7.48, cable_loss_7_by_8: 3.39, cable_loss_11_by_4: 2.38, cable_loss_15_by_8: 2.04, default_selected: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 7.93, cable_loss_7_by_8: 3.69, cable_loss_11_by_4: 2.59, cable_loss_15_by_8: 2.04, default_selected: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 7.64, cable_loss_7_by_8: 3.6, cable_loss_11_by_4: 2.46, cable_loss_15_by_8: 2.08, default_selected: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 7.89, cable_loss_7_by_8: 3.77, cable_loss_11_by_4: 2.58, cable_loss_15_by_8: 2.19, default_selected: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 8.6, cable_loss_7_by_8: 4.15, cable_loss_11_by_4: 2.96, cable_loss_15_by_8: 2.36, default_selected: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 8.27, cable_loss_7_by_8: 4.03, cable_loss_11_by_4: 2.75, cable_loss_15_by_8: 2.36, default_selected: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 9.16, cable_loss_7_by_8: 4.65, cable_loss_11_by_4: 3.16, cable_loss_15_by_8: 2.76, default_selected: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 10.06, cable_loss_7_by_8: 5.27, cable_loss_11_by_4: 3.57, cable_loss_15_by_8: 3.15, default_selected: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 11.56, cable_loss_7_by_8: 6.13, cable_loss_11_by_4: 4.03, cable_loss_15_by_8: 3.51, default_selected: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 12.57, cable_loss_7_by_8: 6.62, cable_loss_11_by_4: 4.36, cable_loss_15_by_8: 3.71, default_selected: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 12.72, cable_loss_7_by_8: 6.9, cable_loss_11_by_4: 4.52, cable_loss_15_by_8: 4.03, default_selected: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 12.85, cable_loss_7_by_8: 7.29, cable_loss_11_by_4: 4.82, cable_loss_15_by_8: 4.34, default_selected: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 13.09, cable_loss_7_by_8: 7.66, cable_loss_11_by_4: 5.13, cable_loss_15_by_8: 4.63, default_selected: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 13.81, cable_loss_7_by_8: 8.1, cable_loss_11_by_4: 5.71, cable_loss_15_by_8: 5.54, default_selected: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 1650.0, cable_loss_7_by_8: 9.71, cable_loss_11_by_4: 6.53, cable_loss_15_by_8: 6.02, default_selected: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 16.86, cable_loss_7_by_8: 9.96, cable_loss_11_by_4: 6.69, cable_loss_15_by_8: 6.18, default_selected: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 19.0, cable_loss_7_by_8: 11.44, cable_loss_11_by_4: 7.67, cable_loss_15_by_8: 7.13, default_selected: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 22.22, cable_loss_7_by_8: 13.67, cable_loss_11_by_4: 9.15, cable_loss_15_by_8: 8.56, default_selected: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 22.58, cable_loss_7_by_8: 13.91, cable_loss_11_by_4: 9.32, cable_loss_15_by_8: 8.72, default_selected: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 23.3, cable_loss_7_by_8: 14.41, cable_loss_11_by_4: 9.64, cable_loss_15_by_8: 9.04, default_selected: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 23.65, cable_loss_7_by_8: 14.65, cable_loss_11_by_4: 9.81, cable_loss_15_by_8: 9.2, default_selected: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 24.73, cable_loss_7_by_8: 15.39, cable_loss_11_by_4: 10.3, cable_loss_15_by_8: 9.68, default_selected: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 26.16, cable_loss_7_by_8: 16.38, cable_loss_11_by_4: 10.96, cable_loss_15_by_8: 10.31, default_selected: nil}
])
puts "Done"

puts "Creating RSSI Threshold Level Benchmarks"
RssiThresholdLevelBenchmark.create!([
  {threshold_level: -50, default_selected: false},
  {threshold_level: -55, default_selected: false},
  {threshold_level: -60, default_selected: false},
  {threshold_level: -65, default_selected: false},
  {threshold_level: -70, default_selected: false},
  {threshold_level: -75, default_selected: false},
  {threshold_level: -80, default_selected: false},
  {threshold_level: -85, default_selected: true},
  {threshold_level: -90, default_selected: false},
  {threshold_level: -95, default_selected: false},
  {threshold_level: -100, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Report Material Lists"
TunnelReportMaterialList.create!([
  {tunnel_material_category_id: 1, description: "Low Power (1 Watt Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "High Power (5 Watts Composite Power) BDA Unit Connected to DAS"},
  {tunnel_material_category_id: 1, description: "Medium Power (2 Watts Composite Power) BDA Unit Connected to OFF-AIR DONOR"},
  {tunnel_material_category_id: 2, description: "Hybrid Combiner, N-type connectors"},
  {tunnel_material_category_id: 2, description: "Custom Built Filtering, N-type connectors"},
  {tunnel_material_category_id: 2, description: "Base Station Interface (Point of Interconnect)"},
  {tunnel_material_category_id: 3, description: "Master Fiber Optic Transceiver (Single Port)"},
  {tunnel_material_category_id: 3, description: "Master Fiber Optic Transceiver (8 Fiber Optic Ports)"},
  {tunnel_material_category_id: 3, description: "Slave Fiber Optic Transceiver (Single Port)"},
  {tunnel_material_category_id: 3, description: "Fiber Optic Jumpers (10ft)"},
  {tunnel_material_category_id: 3, description: "Fiber Optic Backbone Cable (ft)"},
  {tunnel_material_category_id: 4, description: "Off-air donor antenna, Yagi, minimum 10dBi Gain"},
  {tunnel_material_category_id: 4, description: "DC Block Donor, Coaxial Line Protector (N-Female connectors)"},
  {tunnel_material_category_id: 4, description: "1/2 inch Off-air donor antenna cable (ft)"},
  {tunnel_material_category_id: 4, description: "N-Male connectors for off-air donor antenna cables"},
  {tunnel_material_category_id: 4, description: "Ground Kit for 1/2 inch Coax (for off-air donor antenna cable outer conductor)"},
  {tunnel_material_category_id: 4, description: "Universal Weatherproofing Kit (for off-air donor antenna cable connectors)"},
  {tunnel_material_category_id: 4, description: "Prep Tool for 1/2 inch donor cable (all-in-one strip tool)"},
  {tunnel_material_category_id: 5, description: "1-5/8 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1-1/4 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "7/8 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1/2 inch Diameter Radiating Cable (ft)"},
  {tunnel_material_category_id: 5, description: "1/2 inch plenum rated coaxial cable (ft)"},
  {tunnel_material_category_id: 5, description: "2 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "3 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "4 way splitters (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "Directional Coupler (broadband),N-type connectors"},
  {tunnel_material_category_id: 5, description: "Crossband Coupler ,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Hybrid Combiner,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Custom Built Filtering,N-type connectors"},
  {tunnel_material_category_id: 5, description: "Coaxial RF jumpers, plenum rated, N-Male Connectors (3ft)"},
  {tunnel_material_category_id: 5, description: "N-Male Connectors (for 1/2 inch plenum rated coaxial cable)"},
  {tunnel_material_category_id: 5, description: "Antenna, Yagi, minimum 10dBi Gain, N-type connectors"},
  {tunnel_material_category_id: 5, description: "Corresponding size cable clamps (kit or individual)"},
  {tunnel_material_category_id: 5, description: "Corresponding Size Radiating Cable Connector (N-Type)"},
  {tunnel_material_category_id: 5, description: "End of Cable 50 Ohm Load (N-Type Connector)"}
])
puts "Done"

puts "Creating Link Budget Admin"
LinkBudgetAdmin.create!([
  {cable_length: 120.0, antenna_gain: 0.0, dl_margin: 3.0}
])
puts "Done"

puts "Creating Tunnel Link Budget Admin"
TunnelLinkBudgetAdmin.create!([
  {indoor_margin: 15.0}
])
puts "Done"

puts "Creating System Architectures"
SystemArchitecture.create!([
  {architecture: "Fiber and Coax", architecture_assigned: false, default_selected: true},
  {architecture: "Coax", architecture_assigned: false, default_selected: false},
  {architecture: "Auto Select", architecture_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Have Floor Plans"
HaveFloorPlan.create!([
  {operator: "No", operand: "Enter Building Data on the following pop-up page. Click the selection box to confirm Quick Area Data (Total Building Floor Area) Entry. Enter Estimated Total Building Area. Estimated Area may possibly be verified online using the Planimeter Tool available under \"Data Options\".  Click \"ENTER AREA\" button below to get started."},
  {operator: "Yes", operand: "Enter Building Data on the following pop-up page. Click the selection box to confirm Quick Area Data (Total Building Floor Area) Entry. Enter True Total Building Area OR Enter True Area of Each Floor. True Area may be verified from actual floorplans. Click \"ENTER AREA\" button below to get started."}
])
puts "Done"

puts "Creating Tunnel Have Floor Plans"
TunnelHaveFloorPlan.create!([
  {operator: "Yes", operand: "Enter True Data for each Tunnel Segment in Data Entry  window below"},
  {operator: "No", operand: "Enter Estimated Data for each Tunnel Segment in Data Entry  window below"}
])
puts "Done"

puts "Creating Tunnel Cable C Losses"
TunnelCableCLoss.create!([
  {highest_frequency_band_id: 6, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 7, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 8, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 9, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 10, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 11, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 12, cable_loss_1_by_2: 64.0, cable_loss_7_by_8: 64.0, cable_loss_11_by_4: 64.0, cable_loss_15_by_8: 56.0, default_selected: nil},
  {highest_frequency_band_id: 13, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 14, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 15, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 16, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 17, cable_loss_1_by_2: 66.0, cable_loss_7_by_8: 66.0, cable_loss_11_by_4: 66.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 18, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 19, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 20, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 21, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 22, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 23, cable_loss_1_by_2: 67.0, cable_loss_7_by_8: 67.0, cable_loss_11_by_4: 67.0, cable_loss_15_by_8: 70.0, default_selected: nil},
  {highest_frequency_band_id: 24, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 25, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 26, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 27, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 69.0, default_selected: nil},
  {highest_frequency_band_id: 28, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 66.0, default_selected: nil},
  {highest_frequency_band_id: 29, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 66.0, default_selected: nil},
  {highest_frequency_band_id: 30, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 71.0, default_selected: nil},
  {highest_frequency_band_id: 31, cable_loss_1_by_2: 68.0, cable_loss_7_by_8: 68.0, cable_loss_11_by_4: 68.0, cable_loss_15_by_8: 71.0, default_selected: nil},
  {highest_frequency_band_id: 32, cable_loss_1_by_2: 70.0, cable_loss_7_by_8: 70.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 33, cable_loss_1_by_2: 70.0, cable_loss_7_by_8: 70.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 34, cable_loss_1_by_2: 71.0, cable_loss_7_by_8: 71.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 35, cable_loss_1_by_2: 73.0, cable_loss_7_by_8: 73.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 36, cable_loss_1_by_2: 73.0, cable_loss_7_by_8: 73.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 37, cable_loss_1_by_2: 73.0, cable_loss_7_by_8: 73.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 38, cable_loss_1_by_2: 73.0, cable_loss_7_by_8: 73.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 39, cable_loss_1_by_2: 74.0, cable_loss_7_by_8: 74.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil},
  {highest_frequency_band_id: 40, cable_loss_1_by_2: 75.0, cable_loss_7_by_8: 75.0, cable_loss_11_by_4: 65.0, cable_loss_15_by_8: 65.0, default_selected: nil}
])
puts "Done"

puts "Creating Product Frequency Channels"
ProductFrequencyChannel.create!([
  {number_of_channels: 1, composite_power: 30.0, channel_power: 27.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 2, composite_power: 30.0, channel_power: 24.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 3, composite_power: 30.0, channel_power: 22.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 4, composite_power: 30.0, channel_power: 21.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 5, composite_power: 30.0, channel_power: 20.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 6, composite_power: 30.0, channel_power: 19.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 7, composite_power: 30.0, channel_power: 18.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 8, composite_power: 30.0, channel_power: 18.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 9, composite_power: 30.0, channel_power: 17.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 10, composite_power: 30.0, channel_power: 17.0, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 11, composite_power: 30.0, channel_power: 16.6, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 12, composite_power: 30.0, channel_power: 16.2, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 13, composite_power: 30.0, channel_power: 15.9, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 14, composite_power: 30.0, channel_power: 15.5, papr: 3.0, bda_product_category_id: 1, default_selected: false},
  {number_of_channels: 15, composite_power: 30.0, channel_power: 15.2, papr: 3.0, bda_product_category_id: 1, default_selected: true},
  {number_of_channels: 16, composite_power: 34.0, channel_power: 19.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 17, composite_power: 34.0, channel_power: 18.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 18, composite_power: 34.0, channel_power: 18.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 19, composite_power: 34.0, channel_power: 18.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 20, composite_power: 34.0, channel_power: 18.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 21, composite_power: 34.0, channel_power: 17.8, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 22, composite_power: 34.0, channel_power: 17.6, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 23, composite_power: 34.0, channel_power: 17.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 24, composite_power: 34.0, channel_power: 17.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 25, composite_power: 34.0, channel_power: 17.0, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 26, composite_power: 34.0, channel_power: 16.9, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 27, composite_power: 34.0, channel_power: 16.7, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 28, composite_power: 34.0, channel_power: 16.5, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 29, composite_power: 34.0, channel_power: 16.4, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 30, composite_power: 34.0, channel_power: 16.2, papr: 3.0, bda_product_category_id: 2, default_selected: false},
  {number_of_channels: 31, composite_power: 37.0, channel_power: 19.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 32, composite_power: 37.0, channel_power: 18.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 33, composite_power: 37.0, channel_power: 18.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 34, composite_power: 37.0, channel_power: 18.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 35, composite_power: 37.0, channel_power: 18.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 36, composite_power: 37.0, channel_power: 18.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 37, composite_power: 37.0, channel_power: 18.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 38, composite_power: 37.0, channel_power: 18.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 39, composite_power: 37.0, channel_power: 18.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 40, composite_power: 37.0, channel_power: 18.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 41, composite_power: 37.0, channel_power: 17.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 42, composite_power: 37.0, channel_power: 17.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 43, composite_power: 37.0, channel_power: 17.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 44, composite_power: 37.0, channel_power: 17.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 45, composite_power: 37.0, channel_power: 17.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 46, composite_power: 37.0, channel_power: 17.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 47, composite_power: 37.0, channel_power: 17.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 48, composite_power: 37.0, channel_power: 17.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 49, composite_power: 37.0, channel_power: 17.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 50, composite_power: 37.0, channel_power: 17.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 51, composite_power: 37.0, channel_power: 16.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 52, composite_power: 37.0, channel_power: 16.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 53, composite_power: 37.0, channel_power: 16.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 54, composite_power: 37.0, channel_power: 16.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 55, composite_power: 37.0, channel_power: 16.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 56, composite_power: 37.0, channel_power: 16.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 57, composite_power: 37.0, channel_power: 16.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 58, composite_power: 37.0, channel_power: 16.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 59, composite_power: 37.0, channel_power: 16.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 60, composite_power: 37.0, channel_power: 16.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 61, composite_power: 37.0, channel_power: 16.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 62, composite_power: 37.0, channel_power: 16.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 63, composite_power: 37.0, channel_power: 16.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 64, composite_power: 37.0, channel_power: 15.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 65, composite_power: 37.0, channel_power: 15.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 66, composite_power: 37.0, channel_power: 15.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 67, composite_power: 37.0, channel_power: 15.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 68, composite_power: 37.0, channel_power: 15.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 69, composite_power: 37.0, channel_power: 15.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 70, composite_power: 37.0, channel_power: 15.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 71, composite_power: 37.0, channel_power: 15.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 72, composite_power: 37.0, channel_power: 15.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 73, composite_power: 37.0, channel_power: 15.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 74, composite_power: 37.0, channel_power: 15.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 75, composite_power: 37.0, channel_power: 15.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 76, composite_power: 37.0, channel_power: 15.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 77, composite_power: 37.0, channel_power: 15.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 78, composite_power: 37.0, channel_power: 15.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 79, composite_power: 37.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 80, composite_power: 37.0, channel_power: 15.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 81, composite_power: 37.0, channel_power: 14.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 82, composite_power: 37.0, channel_power: 14.9, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 83, composite_power: 37.0, channel_power: 14.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 84, composite_power: 37.0, channel_power: 14.8, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 85, composite_power: 37.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 86, composite_power: 37.0, channel_power: 14.7, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 87, composite_power: 37.0, channel_power: 14.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 88, composite_power: 37.0, channel_power: 14.6, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 89, composite_power: 37.0, channel_power: 14.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 90, composite_power: 37.0, channel_power: 14.5, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 91, composite_power: 37.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 92, composite_power: 37.0, channel_power: 14.4, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 93, composite_power: 37.0, channel_power: 14.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 94, composite_power: 37.0, channel_power: 14.3, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 95, composite_power: 37.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 96, composite_power: 37.0, channel_power: 14.2, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 97, composite_power: 37.0, channel_power: 14.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 98, composite_power: 37.0, channel_power: 14.1, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 99, composite_power: 37.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 3, default_selected: false},
  {number_of_channels: 100, composite_power: 37.0, channel_power: 14.0, papr: 3.0, bda_product_category_id: 3, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Product Price Lists"
TunnelProductPriceList.create!([
  {tunnel_report_material_list_id: 1, vendor: "vendor1", manufacturer: "manufacturer1", mfg_pn: "mfg_pn1", our_pn: "our_pn1", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 9790.29},
  {tunnel_report_material_list_id: 2, vendor: "vendor13", manufacturer: "manufacturer13", mfg_pn: "mfg_pn13", our_pn: "our_pn13", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 14547.39},
  {tunnel_report_material_list_id: 3, vendor: "vendor14", manufacturer: "manufacturer14", mfg_pn: "mfg_pn14", our_pn: "our_pn14", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 19305.0},
  {tunnel_report_material_list_id: 4, vendor: "vendor15", manufacturer: "manufacturer15", mfg_pn: "mfg_pn15", our_pn: "our_pn15", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 14547.39},
  {tunnel_report_material_list_id: 5, vendor: "vendor16", manufacturer: "manufacturer16", mfg_pn: "mfg_pn16", our_pn: "our_pn16", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2064.92},
  {tunnel_report_material_list_id: 6, vendor: "vendor17", manufacturer: "manufacturer17", mfg_pn: "mfg_pn17", our_pn: "our_pn17", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 350.0},
  {tunnel_report_material_list_id: 8, vendor: "vendor19", manufacturer: "manufacturer19", mfg_pn: "mfg_pn19", our_pn: "our_pn19", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 5639.92},
  {tunnel_report_material_list_id: 9, vendor: "vendor20", manufacturer: "manufacturer20", mfg_pn: "mfg_pn20", our_pn: "our_pn20", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 8000.0},
  {tunnel_report_material_list_id: 10, vendor: "vendor21", manufacturer: "manufacturer21", mfg_pn: "mfg_pn21", our_pn: "our_pn21", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2819.96},
  {tunnel_report_material_list_id: 11, vendor: "vendor22", manufacturer: "manufacturer22", mfg_pn: "mfg_pn22", our_pn: "our_pn22", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 18.0},
  {tunnel_report_material_list_id: 12, vendor: "vendor23", manufacturer: "manufacturer23", mfg_pn: "mfg_pn23", our_pn: "our_pn23", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 0.5},
  {tunnel_report_material_list_id: 13, vendor: "vendor24", manufacturer: "manufacturer24", mfg_pn: "mfg_pn24", our_pn: "our_pn24", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 227.87},
  {tunnel_report_material_list_id: 14, vendor: "vendor25", manufacturer: "manufacturer25", mfg_pn: "mfg_pn25", our_pn: "our_pn25", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 73.86},
  {tunnel_report_material_list_id: 23, vendor: "vendor34", manufacturer: "manufacturer34", mfg_pn: "mfg_pn34", our_pn: "our_pn34", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 5.25},
  {tunnel_report_material_list_id: 16, vendor: "vendor27", manufacturer: "manufacturer27", mfg_pn: "mfg_pn27", our_pn: "our_pn27", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.24},
  {tunnel_report_material_list_id: 17, vendor: "vendor28", manufacturer: "manufacturer28", mfg_pn: "mfg_pn28", our_pn: "our_pn28", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 107.25},
  {tunnel_report_material_list_id: 18, vendor: "vendor29", manufacturer: "manufacturer29", mfg_pn: "mfg_pn29", our_pn: "our_pn29", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.25},
  {tunnel_report_material_list_id: 19, vendor: "vendor30", manufacturer: "manufacturer30", mfg_pn: "mfg_pn30", our_pn: "our_pn30", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 190.0},
  {tunnel_report_material_list_id: 24, vendor: "vendor35", manufacturer: "manufacturer35", mfg_pn: "mfg_pn35", our_pn: "our_pn35", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2.37},
  {tunnel_report_material_list_id: 25, vendor: "vendor36", manufacturer: "manufacturer36", mfg_pn: "mfg_pn36", our_pn: "our_pn36", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 75.5},
  {tunnel_report_material_list_id: 26, vendor: "vendor37", manufacturer: "manufacturer37", mfg_pn: "mfg_pn37", our_pn: "our_pn37", description: nil, mfg_list_price: 127.0, our_cost: 128.0, discount_off_list: 2.0, our_sell_price: 95.9},
  {tunnel_report_material_list_id: 27, vendor: "vendor38", manufacturer: "manufacturer38", mfg_pn: "mfg_pn38", our_pn: "our_pn38", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 244.53},
  {tunnel_report_material_list_id: 28, vendor: "vendor39", manufacturer: "manufacturer39", mfg_pn: "mfg_pn39", our_pn: "our_pn39", description: nil, mfg_list_price: 122.0, our_cost: 123.0, discount_off_list: 1.0, our_sell_price: 235.52},
  {tunnel_report_material_list_id: 15, vendor: "vendor26", manufacturer: "manufacturer26", mfg_pn: "mfg_pn26", our_pn: "our_pn26", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2.83},
  {tunnel_report_material_list_id: 29, vendor: "vendor40", manufacturer: "manufacturer40", mfg_pn: "mfg_pn40", our_pn: "our_pn40", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 850.0},
  {tunnel_report_material_list_id: 30, vendor: "vendor41", manufacturer: "manufacturer41", mfg_pn: "mfg_pn41", our_pn: "our_pn41", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 350.0},
  {tunnel_report_material_list_id: 20, vendor: "vendor31", manufacturer: "manufacturer31", mfg_pn: "mfg_pn31", our_pn: "our_pn31", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 11.7},
  {tunnel_report_material_list_id: 21, vendor: "vendor32", manufacturer: "manufacturer32", mfg_pn: "mfg_pn32", our_pn: "our_pn32", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 10.48},
  {tunnel_report_material_list_id: 22, vendor: "vendor33", manufacturer: "manufacturer33", mfg_pn: "mfg_pn33", our_pn: "our_pn33", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 9.25},
  {tunnel_report_material_list_id: 31, vendor: "vendor42", manufacturer: "manufacturer42", mfg_pn: "mfg_pn42", our_pn: "our_pn42", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 3800.0},
  {tunnel_report_material_list_id: 32, vendor: "vendor43", manufacturer: "manufacturer43", mfg_pn: "mfg_pn43", our_pn: "our_pn43", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 46.76},
  {tunnel_report_material_list_id: 33, vendor: "vendor44", manufacturer: "manufacturer44", mfg_pn: "mfg_pn44", our_pn: "our_pn44", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.25},
  {tunnel_report_material_list_id: 34, vendor: "vendor45", manufacturer: "manufacturer45", mfg_pn: "mfg_pn45", our_pn: "our_pn45", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 227.87},
  {tunnel_report_material_list_id: 35, vendor: "vendor46", manufacturer: "manufacturer46", mfg_pn: "mfg_pn46", our_pn: "our_pn46", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 4.75},
  {tunnel_report_material_list_id: 37, vendor: "vendor48", manufacturer: "manufacturer48", mfg_pn: "mfg_pn48", our_pn: "our_pn48", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 71.5},
  {tunnel_report_material_list_id: 36, vendor: "vendor47", manufacturer: "manufacturer47", mfg_pn: "mfg_pn47", our_pn: "our_pn47", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 95.0},
  {tunnel_report_material_list_id: 7, vendor: "vendor18", manufacturer: "manufacturer18", mfg_pn: "mfg_pn18", our_pn: "our_pn18", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 3800.0}
])
puts "Done"

puts "Creating Tunnel Coverage Distance Per BDAs"
TunnelCoverageDistancePerBda.create!([
  {bda_product_category_id: 1, coverage_distance: 1320.0},
  {bda_product_category_id: 2, coverage_distance: 2640.0},
  {bda_product_category_id: 3, coverage_distance: 5280.0}
])
puts "Done"

puts "Creating System Feed Methods"
SystemFeedMethod.create!([
  {feed_method: "Direct Feed", feed_assigned: false, default_selected: false},
  {feed_method: "Off-Air", feed_assigned: false, default_selected: true},
  {feed_method: "Auto Select", feed_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Tunnel Fiber Material Quantities"
TunnelFiberMaterialQuantity.create!([
  {tunnel_report_material_list_id: 8, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 9, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 10, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 11, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 12, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Tunnel Donor Direct Feed Quantities"
TunnelDonorDirectFeedQuantity.create!([
  {tunnel_report_material_list_id: 5, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 6, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 7, description: nil, quantity: 0.0},
  {tunnel_report_material_list_id: 13, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 14, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 15, description: nil, quantity: 250.0},
  {tunnel_report_material_list_id: 16, description: nil, quantity: 2.0},
  {tunnel_report_material_list_id: 17, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 18, description: nil, quantity: 1.0},
  {tunnel_report_material_list_id: 19, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Building Environments"
BuildingEnvironment.create!([
  {environment: "Airport Terminal Building", building_indoor_factor: 2.2},
  {environment: "Apartment Building or Condo", building_indoor_factor: 3.8},
  {environment: "Bank", building_indoor_factor: 2.9},
  {environment: "Building (With Mostly Brick or Cinder Block Rooms)", building_indoor_factor: 4.0},
  {environment: "Building Basement or Underground Pathways", building_indoor_factor: 3.8},
  {environment: "Bus Station or Train Station Terminal Builing", building_indoor_factor: 3.0},
  {environment: "Capitol Building / Town Hall", building_indoor_factor: 3.2},
  {environment: "Casino", building_indoor_factor: 2.6},
  {environment: "Chemical Plant", building_indoor_factor: 3.8},
  {environment: "Church", building_indoor_factor: 2.3},
  {environment: "Club / Bar / Pub", building_indoor_factor: 3.0},
  {environment: "Convention Center", building_indoor_factor: 2.3},
  {environment: "Correctional Facility (Jail)", building_indoor_factor: 4.0},
  {environment: "Courthouse", building_indoor_factor: 3.0},
  {environment: "DAM / Reservoir", building_indoor_factor: 4.0},
  {environment: "Densely Cluttered Facility", building_indoor_factor: 4.0},
  {environment: "Factory", building_indoor_factor: 3.8},
  {environment: "Fire Station", building_indoor_factor: 2.8},
  {environment: "Fittness Center / Community Center", building_indoor_factor: 2.8},
  {environment: "Hospital", building_indoor_factor: 3.2},
  {environment: "Hotel / Motel", building_indoor_factor: 3.8},
  {environment: "Mail or Package Sorting Facility", building_indoor_factor: 3.8},
  {environment: "Mall or Shopping Center", building_indoor_factor: 2.4},
  {environment: "Manufacturing Plant", building_indoor_factor: 3.8},
  {environment: "Medium Cluttered Facility", building_indoor_factor: 3.2},
  {environment: "Office Building (With Mixed-Material Rooms)", building_indoor_factor: 3.3},
  {environment: "Office Building (With Mostly Low Partitions)", building_indoor_factor: 2.8},
  {environment: "Office Building (With Mostly Soft-Material Rooms)", building_indoor_factor: 3.0},
  {environment: "Oil Rig or Other Sea Vessel", building_indoor_factor: 3.8},
  {environment: "Parking Garage (Above Ground)", building_indoor_factor: 2.8},
  {environment: "Parking Garage (Underground)", building_indoor_factor: 3.5},
  {environment: "Police Station", building_indoor_factor: 3.3},
  {environment: "Power Plant or Nuclear Facility", building_indoor_factor: 3.8},
  {environment: "Restaurant", building_indoor_factor: 3.2},
  {environment: "Retail Store or Big Box Store", building_indoor_factor: 2.3},
  {environment: "School", building_indoor_factor: 3.5},
  {environment: "Ship", building_indoor_factor: 3.8},
  {environment: "Stadium", building_indoor_factor: 2.4},
  {environment: "Suburban Home", building_indoor_factor: 3.0},
  {environment: "Supermarket", building_indoor_factor: 3.6},
  {environment: "Theater", building_indoor_factor: 2.5},
  {environment: "University Campus", building_indoor_factor: 3.5},
  {environment: "Warehouse (With Metal Shelving or Internal Partitions)", building_indoor_factor: 3.8},
  {environment: "Warehouse (Without Shelving or Internal Partitions)", building_indoor_factor: 2.2},
  {environment: "Water Treatment Plant", building_indoor_factor: 3.8},
  {environment: "Wide Open Facility (Free Space-No Clutter)", building_indoor_factor: 2.2}
])
puts "Done"

puts "Creating Coverage Area Per BDAs"
CoverageAreaPerBda.create!([
  {bda_product_category_id: 1, coverage_area: 100000.0},
  {bda_product_category_id: 2, coverage_area: 180000.0},
  {bda_product_category_id: 3, coverage_area: 250000.0}
])
puts "Done"

puts "Creating Static Page"
StaticPage.create!([
  {contact_us: " <!-- \r\n<div class=\"contactMap\">\r\n<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d55613461.81318646!2d-146.61479896957405!3d31.70561343684153!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52b30b71698e729d%3A0x131328839761a382!2sNorth+America!5e0!3m2!1sen!2sin!4v1447843417347\" width=\"100%\" height=\"350\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\r\n</div>\r\n-->\r\n<div class=\"addressCon\">\r\n  <h1 class=\"quick_start_h1\"> Reach Out to Us Today</h1>\r\n  <p style=\"font-weight:300;\">We welcome any and all information - your comments, and criticisms alike - that helps us improve our service to you, our customers. If you simply wish to drop us a line, you may use the Contact Form below. As a reminder, if you really need support - technical or otherwise - please visit our <a href=\"https://idasquick.freshdesk.com/support/login/\" target=\"_blank\">Help Desk</a>, where we can systematically address any issue or concern you may have.</p>\r\n  <ul>\r\n<!--\r\n    <li>\r\n      <img src=\"/assets/address.png\">\r\n      <p>Consulting Lorem ipsum dolor sit amet.</p>\r\n    </li>\r\n-->\r\n    <li>\r\n      <img src=\"/assets/phone.png\" alt=\"\">\r\n      <p>(555) 432-6789</p>\r\n    </li>\r\n    <li>\r\n      <img src=\"/assets/email.png\" alt=\"\">\r\n      <p>info@idasquick.net</p>\r\n    </li>\r\n  </ul>\r\n</div>\r\n<div class=\"formbox\">\r\n  <div class=\"formOuter\">\r\n    <div class=\"blackHD quick_start_h1\">Send Us a Message</div>\r\n\r\n <div class=\"row\">\r\n      <form method=\"post\" data-remote=\"true\" accept-charset=\"UTF-8\" action=\"/contact_us\" id=\"new_contact\" class=\"new_contact\" novalidate=\"novalidate\">\r\n      <div class=\"col-sm-12\" id=\"message\">\r\n        <div class=\"alert alert-info fade in margin-bottom-10\">\r\n          <a data-dismiss=\"alert\" class=\"close position-inherit\">x</a>\r\n          <div id=\"return_message\"></div>\r\n        </div>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-6 prz\">\r\n        <input type=\"text\" id=\"contact_first_name\" name=\"contact[first_name]\" placeholder=\"First name\" class=\"form-control inputText mb5\" required=\"required\" autofocus=\"autofocus\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"col-xs-6 plz\">\r\n        <input type=\"text\" id=\"contact_last_name\" name=\"contact[last_name]\" placeholder=\"Last name\" class=\"form-control inputText mb5\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-6 prz\">\r\n        <input type=\"email\" id=\"contact_email\" name=\"contact[email]\" placeholder=\"Email Address\" class=\"form-control inputText mb5 mt15\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"col-xs-6 plz\">\r\n        <input type=\"text\" id=\"contact_contact_number\" name=\"contact[contact_number]\" placeholder=\"Contact Number\" class=\"form-control inputText mb5 mt15\" required=\"required\" aria-required=\"true\">\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-12\">\r\n        <textarea id=\"contact_message\" name=\"contact[message]\" placeholder=\"Enter Message\" class=\"form-control inputText mb5 mt15\" required=\"required\" aria-required=\"true\"></textarea>\r\n      </div>\r\n      <div class=\"clearfix\"></div>\r\n      <div class=\"col-xs-12\">\r\n        <input type=\"submit\" class=\"form-control inputText inputSub\" value=\"Submit\" name=\"commit\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n", faq: "<!--Start Frequently Asked Questions container-->\r\n<div class=\"innermainCon2 innermainCon3\">\r\n  <div class=\"banrHd\">\r\n    <h1 class=\"quick_start_h1\">Frequently Asked Questions</h1>\r\n  </div>\r\n  <div class=\"faqCon\">\r\n    <div class=\"container\">\r\n      <div class=\"heading\">General Questions</div>\r\n      <ul>\r\n        <li>\r\n          <strong>What does \"Max Number of (Simultaneous ) Quotes per instance\" mean ?</strong>\r\n          <p id=\"1\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>What are Usage Credits ?</strong>\r\n          <p id=\"2\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>What does \"8/7 Email Support\" mean ?</strong>\r\n          <p id=\"3\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>What does \"8/5 Phone Support\" mean ?</strong>\r\n          <p id=\"4\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>What does \"Long term Quote Data Storage\" mean, and what benefits does it offer me ?</strong>\r\n          <p id=\"5\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Where can I add Add my Company Logo, as offered in the Professional Plan ?</strong>\r\n          <p id=\"6\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem. \r\n          </p>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n  <div class=\"faqCon faqCon2\">\r\n    <div class=\"container\">\r\n      <div class=\"heading\">General Questions</div>\r\n      <ul>\r\n        <li>\r\n          <strong>How do I Upload Floorplans ?</strong>\r\n          <p id=\"7\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry text of the printing industry ?</strong>\r\n          <p id=\"8\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"9\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"10\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text ?</strong>\r\n          <p id=\"11\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n        <li>\r\n          <strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry ?</strong>\r\n          <p id=\"12\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.Nec viverra felis finibus id. Nullam sapien elit, aliquam ut enim et, semper rutrum lorem. Vivamus suscipit congue tellus ut condimentum. Nulla a venenatis diam, ac porta felis. Donec mi est, facilisis nec massa eget, volutpat pretium dolor.\r\n            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus nec fringilla purus. Sed non tortor dolor. Fusce in sapien nec nisl finibus lobortis non ut sem. Donec consectetur, risus non tincidunt interdum, elit sapien porta arcu, vel tincidunt magna lorem quis augue. Aliquam consectetur gravida lobortis. In et imperdiet lectus, nec ullamcorper elit. Integer elementum vulputate lorem.\r\n          </p>\r\n        </li>\r\n      </ul>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--End Frequently Asked Questions container-->", privacy_policy: "<div class=\"innermainCon2\">\r\n  <div class=\"banrHd\">\r\n    <h1 align=\"left\" class=\"quick_start_h1\">Privacy Policy</h1>\r\n  </div>\r\n  <div class=\"privacyCon\">\r\n    <p align=\"left\">This Privacy Policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information' (PII) is being used online. PII, as used in US privacy law and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an individual in context. Please read our Privacy Policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>\r\n\r\n<h2 align=\"left\">1. What personal information do we collect from visitors to our website?</h2>\r\n    <p align=\"left\">When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, Company Name or other details to help you with your experience.</p>\r\n   \r\n<h2 align=\"left\">2. Do we use 'cookies'?</h2>\r\n    <p align=\"left\">Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.\r\n<p align=\"left\">We use cookies to:<br>\r\n■  Help remember and process the items in the shopping cart.<br>\r\n■  Understand and save user's preferences for future visits.<br>\r\n■  Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third party services that track this information on our behalf.\r\n</p>\r\n\r\n    <p align=\"left\">You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser (like Internet Explorer) settings. Each browser is a little different, so look at your browser's Help menu to learn the correct way to modify your cookies.\r\n    <p align=\"left\">If you disable cookies off, some features will be disabled. It won't affect the users experience that make your site experience more efficient and some of our services will not function properly.\r\nHowever, you can still place orders.</p>\r\n\r\n<h2 align=\"left\">3. How we use the Information we gather</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n\r\n<h2 align=\"left\">4. Accessing, Reviewing and Changing your Personal Information</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n    <p align=\"left\">Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum. </p>\r\n  </div>\r\n</div>", terms_and_conditions: "<div class=\"innermainCon2\">\r\n  <div class=\"banrHd banrTerms\">\r\n    <h1 align=\"left\" class=\"quick_start_h1\">Terms and Conditions</h1>\r\n<br>\r\n <strong>The following is a listing of the terms and conditions that govern the use of this service and this website.</strong>\r\n </div>\r\n  <div class=\"privacyCon\">\r\n\r\n<h2 align=\"left\">1. Terms of Use</h2>\r\n<p align=\"left\">By accessing this web site, you are agreeing to be bound by these web site Terms and Conditions of Use, all applicable laws and regulations, and agree that you are responsible for compliance with any applicable local laws. If you do not agree with any of these terms, you are prohibited from using or accessing this site. The materials contained in this web site are protected by applicable copyright and trade mark law.</p>\r\n\r\n<h3 align=\"left\">2. Intellectual Property Rights<h3>\r\n<p align=\"left\">All copyrights, trademarks, patents and other intellectual property rights in and on our website and all content and software located on the site shall remain the sole property of our company or its licensors. The use of our trademarks, content and intellectual property is forbidden without the express written consent from the owner of this website.</p>\r\n<p style=\"text-align:left\">You must not:<br>\r\n■ Republish material from our website without prior written consent<br>\r\n■ Sell or rent material from our website<br>\r\n■ Reproduce, duplicate, create derivative, copy or otherwise exploit material on our website for any purpose<br>\r\n■ Redistribute any content from our website, including onto another website\r\n</p>\r\n\r\n<h3 align=\"left\">3. Acceptable Use<h3>\r\n<p align=\"left\">You agree to use our website only for lawful purposes, and in a way that does not infringe the rights of, restrict or inhibit anyone else’s use and enjoyment of the website. Prohibited behavior includes harassing or causing distress or inconvenience to any other user, transmitting obscene or offensive content or disrupting the normal flow of dialogue within our website.You must not use our website to send unsolicited commercial communications.You must not use the content on our website for any marketing related purpose without our express written consent.</p>\r\n\r\n<h3 align=\"left\">4. Restricted Access<h3>\r\n<p align=\"left\">We may in the future need to restrict access to parts (or all) of our website and reserve full rights to do so. If, at any point, we provide you with a username and password for you to access restricted areas of our website, you must ensure that both your username and password are kept confidential.</p>\r\n\r\n<h3 align=\"left\">5. Revisions<h3>\r\n<p align=\"left\">The owner of this website may change these terms from time to time and so you should check these terms regularly. Your continued use of our website will be deemed acceptance of the updated or amended terms. If you do not agree to the changes, you should cease using our website immediately. If any of these terms are determined to be illegal, invalid or otherwise unenforceable, it shall be severed and deleted from these terms and the remaining terms shall survive and continue to be binding and enforceable.</p>\r\n\r\n<h3 align=\"left\">6. Limitation of Liability<h3>\r\n<p align=\"left\">THE MATERIALS AT THIS SITE ARE PROVIDED “AS IS” WITHOUT ANY EXPRESS OR IMPLIED WARRANTY OF ANY KIND INCLUDING WARRANTIES OF MERCHANTABILITY, NONINFRINGEMENT OF INTELLECTUAL PROPERTY, OR FITNESS FOR ANY PARTICULAR PURPOSE. IN NO EVENT SHALL THE OWNER OF THIS WEBSITE OR ITS AGENTS OR OFFICERS BE LIABLE FOR ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF INFORMATION, INJURY OR DEATH) ARISING OUT OF THE USE OF OR INABILITY TO USE THE MATERIALS, EVEN IF THE OWNER OF THIS WEBSITE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS OR DAMAGES.</p>\r\n \r\n<h2 align=\"left\">7. Pricing and Marketing Terms and Conditions</h2>\r\n     <p align=\"left\">The pricing provided for quoted projects by Leads Nexus Corporation in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. Leads Nexus Corporation reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with Leads Nexus Corporation</p>\r\n\r\n    <h2 align=\"left\">8. Registering with You</h2>\r\n    <p align=\"left\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam nec lectus gravida, fringilla nunc id, cursus orci. Nulla efficitur ut tortor ac dapibus. Phasellus dignissim elementum diam eget tempor. Donec massa eros, tristique sit amet varius sit amet, placerat vel mi. Curabitur tincidunt, tortor eget feugiat malesuada, elit nibh venenatis lacus, et tristique dolor lacus ut sapien. Morbi pharetra sodales lectus ac scelerisque. Nam sit amet quam in turpis efficitur ullamcorper. Sed tristique lorem lacus, rhoncus cursus nisl tempus vel.</p>\r\n\r\n\r\n    <h2 align=\"left\">9. Services Not Intended For Children</h2>\r\n    <p align=\"left\">This website does not provide services or sell products to children under the age of 18.If we discover we have received any information from a child under the age of 18 in violation of this policy, we will delete that information immediately. If you believe we have received any information from or about anyone under the age of 18, please contact us at the address listed below.</p>\r\n\r\n  </div>\r\n</div>", about_us: "<!--Start About us-->\r\n<div class=\"aboutCon\">\r\n  <div class=\"aboutUsCon\">\r\n    <div class=\"innerContainer\">\r\n      <h1 class=\"quick_start_h1\">About Us</h1>\r\n      <p>In line with our philosophy of seeking to dramatically improve the efficiency of fundamental processes involved in the typical DAS-based business, we have developed a unique, time-tested, Rapid System Design Assessment and Quote (RSDAQ™) method, practically realized as a scaleable software platform known as our RSDAQ™ Engine. This web-based software enables rapid delivery of a rough order-of-magnitude (ROM) estimate of the Bill of Materials (BOM) for a Distributed Antenna System (DAS) used for wireless signal coverage extension within Buildings or Tunnels. </p>\r\n      <p>Any of our subscribers can now gain the incredible benefits of lightning-fast DAS Equipment Cost Estimates and BOMs generated simultaneously for up to 10 Buildings or Tunnels (and more) in less than 15 minutes - in 3 easy steps! </p>\r\n      <br>\r\n      <p><strong>RSDAQ™ Engine Version 2.0.5.1      Patent Pending </strong></p>\r\n      <br>\r\n      <br>\r\n      <p>This software and its user interface are protected by copyright laws, trademarks and other pending or existing intellectual property rights </p>\r\n      <p>RSDAQ is a trademark of its respective owners used under exclusive license. </p>\r\n      <p>iDASQuick is a trademark of  Leads Nexus Corporation. </p>\r\n      <p>Copyright © 2015-2016. All Rights Reserved. </p>\r\n      <p>This software is made available subject to the terms of the license information set forth in the EULA and Terms and Conditions of use . </p>\r\n     <p> All internal contributors and developers are acknowledged. </p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--End About us-->", quick_start: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <h1 class=\"clearfix tac boldHD quick_start_h1\"> Quick-Start Guide</h1>\r\n      <h2 class=\"grayMainGD\">On this page you will find all the necessary information to help you get started on your way to successful project execution in the shortest time possible.  <br>For maximum benefit, we highly recommend that you watch the videos below in the order listed. \r\n      </h2>\r\n      <p>So, ignore the temptation to skip this section - you’ll be glad you did - and you’ll benefit immensely in the long run</p>\r\n<!--\r\n      <iframe  src=\"https://www.youtube.com/embed/6v2L2UGZJAM\" allowfullscreen class=\"videoFrame\" ></iframe>\r\n -->\r\n<iframe src=\"https://www.youtube.com/embed/D_GpVwPt_pQ?rel=0\" allowfullscreen class=\"videoFrame\"></iframe>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD quick_start_h1\"> How it Works</div>\r\n      <p>The software produces and delivers a rough order of magnitude (ROM) estimate of the Bill of Materials (BOM) for wireless signal coverage extension systems within Buildings or Tunnels in 3 easy steps.</p>\r\n      <div class=\"row mt40\">\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box1\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/1a.png\" alt=\"\"> <img src=\"/assets/1.png\" class=\"numbers\" alt=\"\"/> </div>\r\n            </span>\r\n            <div class=\"bold\">Step 1</div>\r\n            <div>Input Building or Tunnel data.</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box2\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/2b.png\" alt=\"\"> <img src=\"/assets/2.png\" class=\"numbers\" alt=\"\"> </div>\r\n            </span>\r\n            <div class=\"bold\">Step 2</div>\r\n            <div>Input system and target performance data as required.</div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-4\">\r\n          <div class=\"box box3\">\r\n            <span>\r\n              <div class=\"imageIcon\"> <img src=\"/assets/3c.png\" alt=\"\"> <img src=\"/assets/3.png\" class=\"numbers\" alt=\"\"></div>\r\n            </span>\r\n            <div class=\"bold\">Step 3</div>\r\n            <div>Acknowledge the Disclaimer and hit the \"FINISH\" button to get your report!</div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--PageBreak-->\r\n<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"clearfix tac boldHD quick_start_h1\"> Step 1</div>\r\n<br>\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2Gray.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 1 - the first of three steps toward completion of your project. <br>\r\n      </h1>\r\n      <p>In this step we enter the following information:</p>\r\n<div align=\"left\"; style=\"margin-left:37%\";>\r\n<ul>\r\n  <li>■  Project Details</li>\r\n  <li>■  Building Data (including Area covered)</li>\r\n  <li>■  Frequency Band of Operation</li>\r\n  <li>■  Number of Services, and</li>\r\n  <li>■  Number of Channels</li>\r\n</ul> \r\n</div>\r\n<iframe src=\"https://www.youtube.com/embed/D_GpVwPt_pQ?rel=0\" allowfullscreen class=\"videoFrame\"></iframe>\r\n<br>\r\n <p><strong>HELPFUL TIP:</strong> Make sure you have the necessary information at hand before beginning Step 1.</p> \r\n      <p></p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--PageBreak-->\r\n<div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"clearfix tac boldHD quick_start_h1\"> Step 2</div>\r\n<br>\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2GrnL.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 2 - the second of three steps toward completion of your project. <br>\r\n      </h1>\r\n <p>In this step we specify the following information:</p>\r\n<div align=\"left\"; style=\"margin-left:37%\";>\r\n<ul>\r\n  <li>■  System Feed Method (Off-Air, or Direct-Feed)</li>\r\n  <li>■  System Architecture (Fiber or Coaxial Cable)</li>\r\n</ul> \r\n</div>\r\n<br>\r\n<iframe src=\"https://www.youtube.com/embed/D_GpVwPt_pQ?rel=0\" allowfullscreen class=\"videoFrame\"></iframe>\r\n<br>\r\n</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!--PageBreak-->\r\n<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"clearfix tac boldHD quick_start_h1\"> Step 3</div>\r\n<br>\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img alt=\"Step 1\" src=\"/assets/s1g.png\">\r\n        <img alt=\"Step 2\" class=\"middleImg\" src=\"/assets/s2GrnL.png\">\r\n        <img alt=\"Step 3\" src=\"/assets/s3orange.png\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 3 - the third and final step toward completion of your project. The actions required of you are extremely simple: Read and acknowledge the Disclaimer, and click the FINISH button to proceed with generating your Report.\r\n      </h1>\r\n<iframe src=\"https://www.youtube.com/embed/D_GpVwPt_pQ?rel=0\" allowfullscreen class=\"videoFrame\"></iframe>\r\n<br>\r\n      <p></p>\r\n    </div>\r\n  </div>\r\n</div>\r\n", quick_start_step1: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2Gray.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 1 - the first of three steps toward completion of your project. <br>Let's get started!\r\n      </h1>\r\n      <p>In this step we enter the following information:</p>\r\n<div align=\"left\"; style=\"margin-left:37%\";>\r\n<ul>\r\n  <li>■  Project Details</li>\r\n  <li>■  Building Data (including Area covered)</li>\r\n  <li>■  Frequency Band of Operation</li>\r\n  <li>■  Number of Services, and</li>\r\n  <li>■  Number of Channels</li>\r\n</ul> \r\n</div>\r\n<br>\r\n <p>Make sure you have the information at hand before proceeding to the next page.</p> \r\n      <p>Once you are ready, click the <strong>NEXT</strong> button to continue.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_step2: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img src=\"/assets/s1g.png\" alt=\"Step 1\">\r\n        <img src=\"/assets/s2GrnL.png\" class=\"middleImg\" alt=\"Step 2\">\r\n        <img src=\"/assets/s3Gray.png\" alt=\"Step 3\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 2 - the second of three steps toward completion of your project. <br>\r\n      </h1>\r\n      <p>In this step we enter the System Feed Method (Off-Air, or Direct-Feed) and Architecture (Fiber or Coaxial Cable). <br>After your selections are made, click the <strong>NEXT</strong> button to continue.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_step3: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"stepOuter\">\r\n        <span>\r\n        <img alt=\"Step 1\" src=\"/assets/s1g.png\">\r\n        <img alt=\"Step 2\" class=\"middleImg\" src=\"/assets/s2GrnL.png\">\r\n        <img alt=\"Step 3\" src=\"/assets/s3orange.png\">\r\n        </span>\r\n      </div>\r\n      <h1 class=\"grayMainGD\">This is Step 3 - the third and final step toward completion of your project. Please acknowledge the Disclaimer below and click the FINISH button to proceed with generating your report.\r\n      </h1>\r\n      <p>This is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quotes but may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to variance standards for Budget Estimates of -10% to +25% at best case and for ROM Estimates of -25% to +75% at worst case.</p>\r\n    </div>\r\n  </div>\r\n</div>", quick_start_download: "<div class=\"grayOuter\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt\">\r\n      <div class=\"clearfix tac boldHD\"> Here is your Report !</div>\r\n      <h1 class=\"grayMainGD\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is  <br>\r\n        simply dummy text of the printing and typesetting industry.\r\n      </h1>\r\n      <p>This service is provided as-is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk for using this service. Leads Nexus Corporation provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by Leads Nexus Corporation Including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. Leads Nexus Corporation shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory.  \r\nIn the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless Leads Nexus Corporation, its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury  of whatsoever kind or nature, including bodily injury of death, relating to the use of this service.</p>\r\n\r\n <p>The pricing provided for quoted projects by Leads Nexus Corporation in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. Leads Nexus Corporation reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with Leads Nexus Corporation</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD\"> Notice</div>\r\n      <p>The downloadable file below is providing for your use only, as a subscriber to this site and the service offered item herein. Sharing of this file is strictly prohibited according to the terms of your subscription agreement.</p>\r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"stepOuterNext stepOuterLast\">\r\n  <div class=\"container\">\r\n    Your download should start automatically after countdown. If not, click on the Download button.\r\n    <div class=\"nextBtn mt20\">\r\n      <div class=\"countdown styled\"></div>\r\n    </div>\r\n  </div>\r\n</div>", header_logged_in: "<div class=\"col-sm-3 logoOuter\">\r\n  <a href=\"/\" data-remote=\"true\" class=\"logo\">\r\n  <img src=\"/assets/logo.png\" alt=\"Logo\">\r\n  </a>\r\n</div>\r\n<div class=\"col-sm-9\">\r\n  <div class=\"text-right alreadyMEm logoutOuter\"> <a href=\"/users/sign_out\" data-method=\"delete\" rel=\"nofollow\" class=\"signIn logout\">Logout</a></div>\r\n  <div class=\"nav\">\r\n    <ul>\r\n      <li class=\"i1\"> <a href=\"/\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i2\"> <a href=\"/projects\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i3\"> <a href=\"/tunnel_projects\" data-remote=\"true\"> </a> </li>\r\n      <li class=\"i4\"> <a href=\"/users/edit#myplan\" data-remote=\"true\"> </a> </li>\r\n    </ul>\r\n  </div>\r\n</div>", header_not_logged_in: "<div class=\"col-sm-3 logoOuter\">\r\n  <a href=\"/\" data-remote=\"true\" class=\"logo\">\r\n  <img src=\"/assets/logo.png\" alt=\"Logo\">\r\n  </a>\r\n</div>\r\n<div class=\"col-sm-9\">\r\n  <div class=\"text-right alreadyMEm\">Already a Member ? <a href=\"javascript:;\" data-target=\"#login\" data-toggle=\"modal\" class=\"signIn\">Login</a></div>\r\n</div>", footer: "<!-- Start of PopChat Script -->\r\n<script type=\"text/javascript\" src=\"http://chat.popchat.us/Chat/MasterServer/Public/ScriptProvider.php\"> </script>\r\n<!-- End of PopChat Script -->\r\n<div class=\"clearfix\"></div>\r\n<footer class=\"clearfix\">\r\n  <div class=\"container\">\r\n    <span>\r\n    <a href=\"/\" data-remote=\"true\">Home</a>\r\n    <a href=\"/about_us\" data-remote=\"true\">About Us</a>\r\n    <a href=\"/terms_and_conditions\" data-remote=\"true\">Terms of Use</a>\r\n    <a href=\"/privacy_policy\" data-remote=\"true\">Privacy Policy</a>\r\n<!--    \r\n<a href=\"/faq\" data-remote=\"true\">FAQ</a>\r\n-->\r\n    <a href=\"/contact_us\" data-remote=\"true\">Contact Us</a>\r\n    <a href=\"https://idasquick.freshdesk.com/\" target=\"_blank\">Help Desk</a>\r\n    </span>\r\n    <div class=\"copy\"> Copyright &copy; 2015-2016 </div>\r\n  </div>\r\n</footer>\r\n<div class=\"ajax-loading\">\r\n  <span>\r\n    <p id=\"please_wait\"><strong>Please wait...</strong></p>\r\n    <p><i class=\"glyphicon glyphicon-refresh gly-spin\" aria-hidden=\"true\"></i></p>\r\n  </span>\r\n</div>", home_page: "<div class=\"grayOuter padding_50\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt dashboard\">\r\n      <div class=\"clearfix tac boldHD my-acc-edit\"> Welcome to your Dashboard !</div><br>\r\n      <p class=\"dash_para m-plan\">On this page you may access your Account to review details, change password, review and upgrade your subscription, payment details, and more. You also have access to all completed project report files available for download (subject to limitations, if any, of your subscription level). You also have access to the Quick-Start Guide, available for review whenever you need it. <br><br> <span>Click to access any of the areas below, or Click the Start Button to begin a new project </span> </p>\r\n      <div class=\"row fastStart\">\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"dash_start new_start\">\r\n            <p>Click <a href=\"/quick_start/step1?first=1\" data-remote=\"true\">Start</a> Button to begin a <br> new project with help screens</p>\r\n            <a href=\"/quick_start/step1?first=1\" data-remote=\"true\" class=\"btn_start\">Start</a> \r\n          </div>\r\n        </div>\r\n        <div class=\"col-sm-6\">\r\n          <div class=\"dash_start fast_start\">\r\n            <p>Click <a href=\"/projects/new\" data-remote=\"true\">fast-start</a> Button to Begin A <br>new project without help screens</p>\r\n            <a href=\"/projects/new\" data-remote=\"true\" class=\"btn_fast_start\">fast-Start</a> \r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"row\">\r\n      <div class=\"col-sm-3 col-xs-6 padd_rgt0\">\r\n        <div class=\"project_box border-credits bg-dashboard\">\r\n          <div class=\"dash_bg1\"> <a href=\"/projects\" data-remote=\"true\" class=\"projct\"> <span></span>projects completed</a> <a href=\"/projects_pending\" data-remote=\"true\" class=\"p_pending\"> <span></span>projects pending </a> </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 padd_rgt0 col-xs-6\">\r\n        <div class=\"project_box border-credits bg-dashboard\">\r\n          <div class=\"dash_bg2 center_text bg-padding\">\r\n            <a href=\"/users/edit\" data-remote=\"true\" class=\"icn_account\"> <span></span>My Account </a> \r\n            <p></p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 padd_rgt0 col-xs-6\">\r\n        <div class=\"project_box border-credits bg-dashboard\">\r\n          <div class=\"dash_bg3\"> <a href=\"/quick_start\" data-remote=\"true\" class=\"q_start\"> <span></span>Quick-Start Guide </a> <a href=\"/help\" data-remote=\"true\" class=\"h_support\"> <span></span>FAQ </a> </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-sm-3 col-xs-6 padd_rgt0\">\r\n        <div class=\"project_box border-credits bg-dashboard\">\r\n          <div class=\"dash_bg4 center_text\"> <span class=\"icn_report\"></span> <a href=\"javascript:;\" data-tooltip=\"AVAILABLE IN PRO ACCOUNT ONLY\" class=\"report_cust\"> Report<br>customization </a> </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", disclaimer_acknowledgement: " <div class=\"Disclaimer whiteBkg\">\r\n  <div class=\"container\">\r\n    <div class=\"max960width\">\r\n      <div class=\"clearfix tac boldHD quick_start_h1\"> Disclaimer</div>\r\n\r\n      <p align=\"left\">The output file is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quotes but may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to variance standards for Budget Estimates of -10% to +25% at best case and for ROM Estimates of -25% to +75% at worst case.</p>\r\n      <p align=\"left\">This service is provided as is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk in using this service. <strong>Leads Nexus Corporation</strong> provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by <strong>Leads Nexus Corporation</strong>, including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. <strong>Leads Nexus Corporation</strong> shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory.  </p>\r\n<p align=\"left\">In the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless <strong>Leads Nexus Corporation</strong>, its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury of whatsoever kind or nature, including bodily injury or death, relating to the use of this service.\r\n</p>\r\n\r\n <p align=\"left\">The pricing provided for quoted projects by Leads Nexus Corporation in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. Leads Nexus Corporation reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with Leads Nexus Corporation</p>\r\n\r\n      <label><input type=\"checkbox\" name=\"acknowledge\" id=\"disclaimer_acknowledge\"> I have read and acknowledge the above disclaimer.</label>\r\n    </div>\r\n  </div>\r\n</div>\r\n<!-- Disclaimer Acknowledge Modal -->\r\n<div class=\"modal fade\" id=\"acknowledge_show\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"DisclaimerShowLabel\">\r\n  <div class=\"modal-dialog modal-sm small-modal-margin\">\r\n    <div class=\"modal-content text-center\">\r\n      <div class=\"modal-header\">\r\n        <button aria-label=\"Close\" data-dismiss=\"modal\" class=\"close\" type=\"button\"><span aria-hidden=\"true\">×</span></button>\r\n        <h4 id=\"DisclaimerShowLabel\" class=\"modal-title\">You must acknowledge the above disclaimer.</h4>\r\n      </div>\r\n      <div class=\"modal-footer text-align-center btn_center margin-top-0\">\r\n        <a href=\"javascript:;\" data-dismiss=\"modal\" class=\"btn_start padding-10 margin-right-0\">OK</a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", download_warning: "<div class=\"Disclaimer\">\r\n  <div class=\"clearfix tac boldHD\"> Notice</div>\r\n  <p>The downloadable report below is provided for your use, as a subscriber to this service. Subletting of this account or resale of this report or service is strictly prohibited according to the terms of your subscription agreement.</p>\r\n  <p>Violation of this agreement will be subject to account termination.</p>\r\n</div>", report_disclaimer: " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-top:30px;\">\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:15px;font-size:17px;color:#4f4f54;\">QUOTE NOTES</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>This is a Material-only ROM/Budgetary Price Quotation. This quote represents our best assessment of the ROM material categories and quantities required to develop the DAS solution per minimum viable inputs (MVI) stated by the user. This quote may be subject to variations due to factors beyond our control. The accuracy of this ROM quote is limited to the confines of the accuracy and industry best practices relating to variance standards for Budget Estimates of -10% to +25% at best case and for ROM Estimates of -25% to +75% at worst case.</p>\r\n      <p>1. This Quote does not Include Backup Battery or UPS Power Systems. AC Power Assumed for all active BDA equipment. </p> \r\n      <p>2. System Engineering Services for RF Site Survey, System Design, System Activation, Optimization, and Coverage confirmation for all extended services are not quoted. </p>\r\n      <p>3. This Quote does not include pricing for travel and expenses, System Closeout Package consisting of: System Description, As-built Documentation, System Settings, Coverage Benchmark Results, Equipment Manuals and Training. </p> \r\n      <p>4. Additional Services such as Alarms reporting via NMS are not quoted. </p>   \r\n      <p>5. This Quote does not include system Installation services. </p>\r\n      <p>6. This Quote assumes that all services lie within the bandwidth of the BDAs based on the basic frequency band entered. </p>\r\n      <p>7. This Quote assumes an all wall-mounted active/passive equipment installation. No floor-standing racks or cabinet assemblies are quoted. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">PRICING TERMS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>1. The Pricing shown on this quote is valid for 30 Days from the quote preparation date shown. </p> \r\n      <p>2. All prices are in US Dollars</p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">QUOTE DISCLAIMERS AND ASSUMPTIONS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>1. Provision of more details such as actual frequencies, floorplans or floor-by-floor dimensions and propagation profile and donor site details, when produced may warrant a re-estimation and re-quote for greater accuracy. </p>\r\n      <p>2. The DAS shall deliver coverage throughout 95% of all occupied building spaces. </p>\r\n      <p>3. It is assumed that any undesired channels occurring within the passband of the Headend are a minimum of 10dB lower in amplitude than any of the desired channels. </p>\r\n      <p>4. The proposed estimate assumes that portable radios are capable of transmitting at a minimum of +30 dBm and mobile phones at +26dBm. </p>\r\n      <p>5. The system is estimated to provide an average ‐ 95 dBm signal level throughout the buildings for Public Safety and other two-way radio Services, and ‐ 85 dBm for commercial wireless carrier services. </p> \r\n      <p>6. The Signal Boosters/BDAs are assumed to be located a Telco Closet with access to a stacked riser. </p>\r\n      <p>7. When Off-Air donor systems are selected, the Radio site BTS is assumed to be located off‐ site and an off‐ air feed solution is assumed. </p>\r\n      <p>8. All facility walls are assumed to be free of metal/mesh shielding. </p>\r\n      <p>9. Areas outside customer-specific indicated coverage zones are not necessarily estimated to have coverage enhancement, however RF signal may radiate into these areas at a level below the average signal level estimated. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      Sed mauris quam, facilisis id lacus sed, fringilla pellentesque erat. Sed neque ipsum, lacinia in commodo vel, consequat in sem. Fusce id libero ac ante vehicula lobortis id sed tellus. Aliquam nulla nulla, tristique vitae suscipit a, ullamcorper at quam. In eu nibh maximus lorem mollis rhoncus. Sed feugiat, turpis et faucibus scelerisque, lorem lorem interdum ligula, vitae consequat urna eros pharetra nisl. Praesent sodales pulvinar magna. Nam eget metus ut purus porta vulputate vulputate a ligula. Ut et eros sem. Vivamus facilisis odio eget metus ornare, et varius augue bibendum.\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"font-weight:bold;padding-bottom:35px;font-size:17px;color:#4f4f54;padding-top:25px;\">QUOTE TERMS AND CONDITIONS</td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n       <p>This service is provided as-is, and is to be used at the subscriber’s discretion. The subscriber assumes all risk of using this service. Leads Nexus Corporation provides no guarantees or warranty of any kind, expressed or implied, with respect to any goods, parts  and service provided by Leads Nexus Corporation, including, but not limited to, the implied guarantees, warranties or merchantability and fitness for a particular purpose. Leads Nexus Corporation shall not in any event be liable for any damages including, but not limited to, any indirect, special or consequential damages arising out of or in connection with furnishing of goods, parts or service, or the performance, use of, or inability to use any goods, parts or service, or otherwise, whether based on contract, tort or any other legal theory. </p>  \r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\">\r\n      <p>In the course of using the output report from this service to respond to or satisfy the conditions of any RFP or other situations relating to the output report from this service, the subscriber agrees to indemnify and hold harmless Leads Nexus Corporation, its officers, agents and employees from and against any liabilities, damages, costs, expenses, and attorneys' fees for any claim of loss, damage, cost, or injury  of whatsoever kind or nature, including bodily injury of death, relating to the use of this service. </p>\r\n    </td>\r\n  </tr>\r\n  <tr>\r\n    <td style=\"color:#727272;font-weight:bold;line-height:20px;padding-bottom:20px;\"> \r\n       <p>The pricing provided for quoted projects by Leads Nexus Corporation in the output report from this service represents a best average market price as offered by preferred vendors, OEMs and equipment manufacturers. Leads Nexus Corporation reserves the right to further offer, promote and/or make suggestions to the subscriber to opt-into participating in external granular, accurate and competitive pricing offers from preferred vendors, OEMs and equipment manufacturers for the subscriber’s quoted projects, at the subscriber’s discretion. The subscriber acknowledges and agrees to these terms and conditions of pricing by registering an account with Leads Nexus Corporation.</p>\r\n</td>\r\n  </tr>\r\n</table>", home_slider: " <div class=\"col-sm-7\">\r\n  <div class=\"sliderInner\">\r\n    <h1 class=\"mainHD\">NEVER WAIT</h1>  \r\n    <div class=\"subHD\">for another DAS Quote Estimate - Ever Again !</div>\r\n    <img src=\"/assets/arrow.png\" alt=\"Arrow\">\r\n    <div data-ride=\"carousel\" class=\"carousel slide\" id=\"carousel-example-generic\">\r\n      <div role=\"listbox\" class=\"carousel-inner\">\r\n        <!-- Slide 1 Start -->\r\n        <div class=\"item active\">\r\n          <div class=\"italic\">Welcome to the world's first implementation of our time-tested Rapid System Design Assessment and Quote (RSDAQ™) method. </div>\r\n        </div>\r\n        <!-- Slide 1 End -->\r\n        <!-- Slide 2 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">Use our RSDAQ™ Engine and get the benefits of lightning-fast DAS Equipment Cost Estimates and BOMs generated simultaneously for up to 10 Buildings (and more) in less than 15 minutes - in 3 Easy Steps!</div>\r\n        </div>\r\n        <!-- Slide 2 End -->\r\n        <!-- Slide 3 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">12,185 Fast DAS ROM Quotes generated by the RSDAQ™ Engine to date, and counting!</div>\r\n        </div>\r\n        <!-- Slide 3 End -->\r\n        <!-- Slide 4 Start -->\r\n        <div class=\"item\">\r\n          <div class=\"italic\">Extreme DAS ROM Quote ROI is now a reality !  The RSDAQ™ engine is unlike anything else available today for generating DAS ROM Quotes. Prove it for yourself !  Get your subscription and watch it drive your project ROI through the roof ! </div>\r\n        </div>\r\n        <!-- Slide 4 End -->\r\n      </div>\r\n      <!-- Controls -->\r\n      <a href=\"#carousel-example-generic\" data-slide=\"prev\" role=\"button\" class=\"left carousel-control\">\r\n        <span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-left\"></span>\r\n      </a>\r\n      <a href=\"#carousel-example-generic\" data-slide=\"next\" role=\"button\" class=\"right carousel-control\">\r\n        <span aria-hidden=\"true\" class=\"glyphicon glyphicon-menu-right\"></span>\r\n      </a>\r\n    </div>\r\n  </div>\r\n</div>", building_projects: "<div class=\"clearfix tac boldHD quick_start_h1\"> Building Projects Completed </div><br>\r\n<p class=\"dash_para\">Below is a list of completed Building-based projects. The files are available for download or may be kept here for short-term storage. <br>Be sure to verify your storage time allowance as corresponds to your subscription level.</p>", building_pending_projects: "<div class=\"clearfix tac boldHD quick_start_h1\"> Building Projects Pending </div><br>\r\n<p class=\"dash_para\">The following is a list of Building-based projects that are 'works-in-process'. <br>Any item in the list may be opened for further editing and eventual completion.</p>", tunnel_projects: "<div class=\"clearfix tac boldHD quick_start_h1\"> Tunnel Projects Completed</div><br>\r\n<p class=\"dash_para\">Below is a list of completed Tunnel-based projects. The files are available for download or may be kept here for short-term storage. <br>Be sure to verify your storage time allowance as corresponds to your subscription level.</p>", tunnel_pending_projects: "<div class=\"clearfix tac boldHD quick_start_h1\"> Tunnel Projects Pending </div><br>\r\n<p class=\"dash_para\">The following is a list of Tunnel-based projects that are 'works-in-process'. <br>Any item in the list may be opened for further editing and eventual completion.</p>", left_image: "high-rise-building_photo.png", right_image: "Tunnel_photo.png", report_header_pdf: " <td width=\"64%\" style=\"color:#b6b6b6;text-align:center;font-size:24px;\">\r\n  Changing The Way You Do DAS ROM Quotes\r\n  <span style=\"display:block;font-size:13px;font-weight:600;\">Contact: iDASQuick. PH:+1-555-432-6789 , info@idasquick.net</span>\r\n</td>", help_user_name: "  <a href=\"/faq?id=1\" target=\"_blank\" data-tooltip=\"Your User Name will be pre-populated here. It is highly recommended that you leave this as-is. However you may change it if desired.\" class=\"question\">?</a>", help_project_name: "  <a href=\"/faq?id=2\" target=\"_blank\" data-tooltip=\"Enter the Project Name here\" class=\"question\">?</a>", help_company: "  <a href=\"/faq?id=3\" target=\"_blank\" data-tooltip=\"Enter your Company Name here\" class=\"question\">?</a>", help_name: "  <a href=\"/faq?id=4\" target=\"_blank\" data-tooltip=\"Enter the Building Name here.\" class=\"question\">?</a>", help_number_of_buildings: "  <a href=\"/faq?id=5\" target=\"_blank\" data-tooltip=\"Select the number (1 to 10) from the drop-down list of buildings in your project.\" class=\"question\">?</a>", help_number_of_tunnels: "  <a href=\"/faq?id=5\" target=\"_blank\" data-tooltip=\"For the number of tunnels, note that the number of bores is irrelevant. Thus, a dual-bore tunnel would be counted as '1' tunnel.\" class=\"question\">?</a>", help_have_floorplan: "  <a href=\"/faq?id=6\" target=\"_blank\" data-tooltip=\"If you have project Floor Plans, select Yes; otherwise select No.\" class=\"question\">?</a>", help_number_of_services: "  <a href=\"/faq?id=7\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_highest_frequency_band: "<a href=\"/faq?id=8\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>  ", help_frequency_channels: "  <a href=\"/faq?id=9\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_excepted_rssi_at_mobile: "  <a href=\"/faq?id=11\" target=\"_blank\" data-tooltip=\"Enter the desired target RSSI level at Mobile\" class=\"question\">?</a>", help_materials_sourcing: "  <a href=\"/faq?id=11\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", credits: "<div class=\"creaditBoxCon\">\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597979231326/1?test=1\">\r\n    <div class=\"creaditBox creaditBox1 border-credits\">\r\n      <span>10</span>\r\n      <strong>10 Credits</strong>\r\n      <p>10 Credits For $12</p>\r\n    </div>\r\n  </a>\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597900231327/1?test=1\">\r\n    <div class=\"creaditBox creaditBox2 border-credits\">\r\n      <span>50</span>\r\n      <strong>50 Credits</strong>\r\n      <p>50 Credits For $54</p>\r\n    </div>\r\n  </a>\r\n  <div class=\"clearfix mt20\"></div>\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597973231132/1?test=1\">\r\n    <div class=\"creaditBox creaditBox1 border-credits\">\r\n      <span>100</span>\r\n      <strong>100 Credits</strong>\r\n      <p>100 Credits For $100</p>\r\n    </div>\r\n  </a>\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597902631133/1?test=1\">\r\n    <div class=\"creaditBox creaditBox2 border-credits\">\r\n      <span>200</span>\r\n      <strong>200 Credits</strong>\r\n      <p>200 Credits For $160</p>\r\n    </div>\r\n  </a>\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597914331134/1?test=1\">\r\n    <div class=\"creaditBox creaditBox3 border-credits\">\r\n      <span>500</span>\r\n      <strong>500 Credits</strong>\r\n      <p>500 Credits For $350</p>\r\n    </div>\r\n  </a>\r\n  <a href=\"javascript:;\" class=\"credit_box\" data-href=\"https://ritmo.zaxaa.com/o/12597986431136/1?test=1\">\r\n    <div class=\"creaditBox creaditBox4 border-credits margin-t20\">\r\n      <span>1000</span>\r\n      <strong>1000 Credits</strong>\r\n      <p>1000 Credits For $497</p>\r\n    </div>\r\n  </a>\r\n</div>", transaction_history: "<div class=\"clearfix tac boldHD my-acc-edit\"> transactions history  </div>\r\n<p class=\"dash_para m-plan\">For your convenience, a summary of your purchase transactions is displayed below.</p>", buy_credits: "<div class=\"clearfix tac boldHD my-acc-edit\"> Buy Credits</div>\r\n<p class=\"dash_para m-plan\">On this page you may purchase Usage Credits for running the RSDAQ™ Engine. Credits may be purchased<br> \r\nin any of the bundled amounts shown by clicking on the corresponding button. <br>\r\nTake advantage of bulk purchase discounted pricing and stock up while you can. <br>\r\nPrices may change, but the Usage Credits do not expire.<br></p>\r\n<div class=\"clearfix\"></div>", edit_your_account: "<div class=\"clearfix tac boldHD my-acc-edit\"> Edit your Account</div>\r\n<p class=\"dash_para font-padding\">On this page you may edit your Account details, Upgrade your Subscription, and Purchase Usage Credits.</p>", personal_information: " <h1 class=\"my-acc\">Personal&nbsp;Information</h1>", change_password: " <h1 class=\"my-acc\">Change&nbsp;Password</h1>", my_plan: " <h1 class=\"my-acc\">My&nbsp;Plan</h1>", building_project_details: " <h3 class=\"building_hd heading-hd\"><span>Project&nbsp;Details</span></h3>\r\n", building_facility_option: "<h3 class=\"building_hd heading-hd\"><span>FacIlity&nbsp;Option</span></h3>", building_data: " <h3 class=\"building_hd heading-hd\"><span>Building&nbsp;Data</span></h3>", building_area_data_entry: " <h3 class=\"building_hd heading-hd\"><span>Data&nbsp;Entry</span></h3>", building_search_option: " <div class=\"area_entry data_entry\">\r\n  <h3 class=\"building_hd heading-hd\"><span>DATA&nbsp;OPTIONS</span></h3>\r\n  <div class=\"inline_area fl\">\r\n    <div class=\"area1 fl\">\r\n      <a href=\"javascript:;\">UPLOAD BATCH DATA FILE</a>\r\n      <input type=\"file\" style=\"display: none;\" id=\"my_file\">\r\n    </div>\r\n  </div>\r\n  <div class=\"inline_area fr\">\r\n    <div class=\"area1\">\r\n      <a href=\"http://www.gravoplex.com/Planimeter/GMapPlanimeter.html\" target=\"_blank\">ONLINE BUILDING SEARCH AND PLANIMETER</a>\r\n    </div>\r\n  </div>\r\n  <div class=\"clearfix\"></div>\r\n</div>", building_band_and_services: "<h3 class=\"building_hd heading-hd\"><span>Band&nbsp;&&nbsp;Services</span></h3> ", building_system_feed_method: "<h3 class=\"building_hd ml-108 heading-hd sysFeedMethod\"><span>System&nbsp;Feed&nbsp;Method</span></h3> ", building_system_architecture: "<h3 class=\"building_hd ml-108 heading-hd sysFeedMethod\"><span>System&nbsp;Architecture</span></h3> ", building_preferred_materials_sourcing: " <h3 class=\"building_hd heading-hd\"><span>preferred materials sourcing</span></h3>\r\n<div class=\"text-center\">Select your favourite Distribution Partner <br>\r\n                or Authorized Reseller</div>\r\n", tunnel_project_details: "<h3 class=\"building_hd heading-hd\"><span>Project&nbsp;Details</span></h3>", tunnel_facility_option: "<h3 class=\"building_hd heading-hd\"><span>FacIlity&nbsp;Option</span></h3> ", tunnel_data: "<h3 class=\"building_hd heading-hd\"><span>Tunnel&nbsp;Data</span></h3> ", tunnel_area_data_entry: " <h3 class=\"building_hd heading-hd\"><span>Data&nbsp;Entry</span></h3>", tunnel_search_option: "<div class=\"area_entry data_entry\">\r\n  <h3 class=\"building_hd heading-hd\"><span>DATA&nbsp;OPTIONS</span></h3>\r\n  <div class=\"inline_area fl\">\r\n    <div class=\"area1 fl\">\r\n      <a href=\"javascript:;\">UPLOAD BATCH DATA FILE</a>\r\n      <input type=\"file\" style=\"display: none;\" id=\"my_file\">\r\n    </div>\r\n  </div>\r\n  <div class=\"inline_area fr\">\r\n    <div class=\"area1\">\r\n      <a href=\"http://www.gravoplex.com/Planimeter/GMapPlanimeter.html\" target=\"_blank\">ONLINE TUNNEL SEARCH AND PLANIMETER</a>\r\n    </div>\r\n  </div>\r\n  <div class=\"clearfix\"></div>\r\n</div> ", tunnel_band_and_services: " <h3 class=\"building_hd heading-hd\"><span>Band&nbsp;&amp;&nbsp;services</span></h3>", tunnel_system_feed_method: "<h3 class=\"building_hd ml-108 heading-hd sysFeedMethod\"><span>System&nbsp;Feed&nbsp;Method</span></h3> ", tunnel_system_architecture: "<h3 class=\"building_hd ml-108 heading-hd sysFeedMethod\"><span>System&nbsp;Architecture</span></h3> ", expected_rssi_at_mobile: "<h3 class=\"building_hd heading-hd\"><span>Expected&nbsp;RSSI&nbsp;at&nbsp;Mobile</span></h3> ", tunnel_preferred_materials_sourcing: "<h3 class=\"building_hd heading-hd\"><span>preferred materials sourcing</span></h3>\r\n<div class=\"text-center\">Select your favourite Distribution Partner <br>\r\n                or Authorized Reseller</div> ", technology_type: "<h3 class=\"building_hd ml115 heading-hd\"><span>Technology&nbsp;Type</span></h3>\r\n<div class=\"text-center\">Select Technology of the system with the <br>\r\n                Highest Frequency Band</div>", communication_type: " <h3 class=\"building_hd ml130 heading-hd\"><span>Communication&nbsp;Type</span></h3>", admin_email: "admin@idasquick.net", internal_server_error: "<p class=\"\">Sorry, but we seem to be experiencing some technical difficulties. We apologize for the inconvenience. <br>Please be assured that if you were in the middle of a Project, your data will be automatically saved, <br>and you may resume exactly where you left off once service has been restored. <br>Please try again in a few minutes. </p><a class=\"btn grayBtn\" href=\"/\" data-remote=\"true\">Back to Home</a> ", not_found: "<p class=\"mt30\">If you can see this page, it's a fair bet that you've lost your way somehow. <br> \r\nThe best advice for you is to return to the Home Page and try navigating again. <br>\r\nIf this page keeps appearing, please contact us at the link shown in the Footer at the bottom of this page. <br>\r\nWe'll do our best to make sure you can reach your intended destination on this site.</p>\r\n<br>\r\n<a class=\"btn grayBtn\" href=\"/\">Back to Home</a>  ", show_report_heading: "<div class=\"clearfix tac boldHD my-acc-edit\"> DAS ROM MATERIAL QUOTE</div>", buttons_report_page: "<div class=\"nextBtn mt20 btn_center\">\r\n  <a href=\"http://www.idasquick.com/wpdq/coming-soon/\" class=\"orangeBtn bg-color-green padding-4-20\" target=\"_blank\">\r\n    B2B Portal Access &nbsp;<i class=\"fa fa-building def-icon\"></i>\r\n  </a> &nbsp;\r\n  <a href=\"https://www.draw.io/\" class=\"orangeBtn bg-color-green padding-4-20\" target=\"_blank\">\r\n    Diagramming Tool &nbsp;<i class=\"fa fa-pencil-square-o def-icon\"></i>\r\n\t</a>\r\n</div>\r\n\r\n<div class=\"nextBtn mt20 btn_center\">\r\n  <a href=\"http://www.idasquick.com/wpdq/coming-soon/\" class=\"orangeBtn bg-color-green padding-4-20\" target=\"_blank\">\r\n    Active Material Spares &nbsp;<i class=\"fa fa-plus-square def-icon\"></i>\r\n  </a> &nbsp;\r\n  <a href=\"http://www.idasquick.com/wpdq/coming-soon/\" class=\"orangeBtn bg-color-green padding-4-20\" target=\"_blank\">\r\n    Filter Builder &nbsp;<i class=\"fa fa-cubes def-icon\"></i> \r\n  </a> &nbsp;\r\n  <a href=\"http://www.idasquick.com/wpdq/coming-soon/\" class=\"orangeBtn bg-color-green padding-4-20 margin-t20\" target=\"_blank\">\r\n    Buy Quoted Materials Now &nbsp;<i class=\"fa fa-credit-card def-icon\"></i> \r\n\t</a>\r\n</div>", building_data_entry: "<div class=\"upperCase blackHD heading-hd\">Building Data Entry</div>", tunnel_data_entry: "<div class=\"upperCase blackHD heading-hd\">Tunnel Data Entry</div>", help_building_name: "  <a href=\"/faq?id=1\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_building_number: "  <a href=\"/faq?id=2\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_number_of_floors: "  <a href=\"/faq?id=3\" target=\"_blank\" data-tooltip=\"Select the number of floors for the corresponding building no.\" class=\"question\">?</a>", help_quick_area_data_entry: "  <a href=\"/faq?id=4\" target=\"_blank\" data-tooltip=\"Checkbox selection allows you to obtain an estimate for the entire building treated as a single floor\" class=\"question\">?</a>", help_building_environment: "  <a href=\"/faq?id=5\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_tunnel_name: "  <a href=\"/faq?id=6\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_tunnel_number: "  <a href=\"/faq?id=7\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_number_of_bores: "  <a href=\"/faq?id=8\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_number_of_segments: "<a href=\"/faq?id=9\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_this_tunnel_bore: "  <a href=\"/faq?id=10\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_this_tunnel_segment: "  <a href=\"/faq?id=11\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_tunnel_environment: "  <a href=\"/faq?id=1\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", help_number_of_stations: "  <a href=\"/faq?id=2\" target=\"_blank\" data-tooltip=\"Lorem Ipsum is simply\" class=\"question\">?</a>", support_link: "<br/><a href=\"mailto:support@idasquick.com\" class=\"font-weight-600\">support@idasquick.com</a>", heading_change_your_password: "<div class=\"blackHD\">Change your password</div> ", heading_forgot_your_password: "<div class=\"blackHD\">Forgot your password?<br> (Password Change Request)</div>  ", heading_register_account: "<div class=\"blackHD\">Register Below <br>To Create Your Personal Account</div> ", heading_login_details: "<div class=\"blackHD\">Enter Your login details <br> Below and click the submit button</div> ", heading_retrieve_forgotten_password: "<div class=\"blackHD\">Retrieve Forgotten Password</div> ", heading_resend_confirmation_instructions: "<div class=\"blackHD\">Resend confirmation instructions</div>", deleted_at: nil, building_pricing: "<div class=\"planOuter\">\r\n  <table class=\"planTable\">\r\n    <tr class=\"planRow\">\r\n      <th></th>\r\n      <th class=\"greenColor\">\r\n        <b>Basic</b><br/>\r\n        <div class=\"fs18\">(Building Only)</div>\r\n      </th>\r\n      <th class=\"blueColor\">\r\n        <b>Standard</b><br/>\r\n        <div class=\"fs18\">(Building Only)</div>\r\n      </th>\r\n      <th class=\"orangeColor\">\r\n        <b>Professional</b><br/>\r\n        <div class=\"fs18\">(Building Only)</div>\r\n      </th>\r\n    </tr>\r\n    <tr class=\"priceRow\">\r\n      <td>\r\n        <div class=\"bold\" align=\"left\"> Subscription (billed on signup as Annual payment) </div>\r\n      </td>\r\n      <td> $ <span class=\"price\">197<span class=\"month\">/mth.</span> </span></td>\r\n      <td> $ <span class=\"price\">297<span class=\"month\">/mth.</span></span></td>\r\n      <td> $ <span class=\"price\">397<span class=\"month\">/mth.</span></span></td>\r\n    </tr>\r\n    <tr class=\"monthPriceRow\">\r\n      <td>\r\n        <div class=\"bold\" align=\"left\"> Subscription (billed on signup as Monthly payment) </div>\r\n      </td>\r\n      <td> $ <span class=\"price\">247<span class=\"month\">/mth.</span> </span></td>\r\n      <td> $ <span class=\"price\">347<span class=\"month\">/mth.</span></span></td>\r\n      <td> $ <span class=\"price\">447<span class=\"month\">/mth.</span></span></td>\r\n    </tr>\r\n  </table>\r\n  <table class=\"planDetails\" cellspacing=\"5\" cellpadding=\"5\">\r\n    <tr>\r\n      <td>Ideal for</td>\r\n      <td>Occasional/Casual Use</td>\r\n      <td>Everyday Quotation Use</td>\r\n      <td>Enterprise Quotation Use</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Max Number of (Simultaneous) Quotes per instance</td>\r\n      <td>10</td>\r\n      <td>10</td>\r\n      <td>100</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Easy, Drag‐and‐Drop Floor Plan Tool</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Usage Credit</td>\r\n      <td>25 Free Credits, One‐time</td>\r\n      <td>50 Free Per Mth.</td>\r\n      <td>100 Free per Mth.</td>\r\n    </tr>\r\n    <tr>\r\n      <td>8/7 Email Support</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>8/5 Phone Support</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Real-time Chat Support</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Long term Quote Data Storage</td>\r\n      <td>3 Mths.</td>\r\n      <td>6 Mths.</td>\r\n      <td>12 Mths.</td>\r\n    </tr>\r\n    <tr>\r\n      <td> Add your Company Logo</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Download Report as XLS or PDF</td>\r\n      <td>PDF Only</td>\r\n      <td>PDF Only</td>\r\n      <td>XLS and PDF</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Upload Floorplans</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>B2B Portal Access</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Membership Site and Forum Access</td>\r\n      <td>Basic Members Area Only</td>\r\n      <td>Standard Members Area Only</td>\r\n      <td>Full Members Areas Access</td>\r\n    </tr>\r\n    <tr>\r\n      <td>DAS Installation Layout Feature</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Report Output Details</td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/cross.png\" alt=\"\"> <br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/cross.png\" alt=\"\"></div>\r\n      </td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/cross.png\" alt=\"\"></div>\r\n      </td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/tic.png\" alt=\"\"></div>\r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td>Pricing Comparisons</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <!---\r\n      <tr>\r\n        <td>Potential Supplier List and Contact Info.</td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      </tr>\r\n      -->\r\n    <tr>\r\n      <td>XLS file upload for Batch Processing</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Online Help Documentation</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Spare Parts Quote: BOM, Qty and Material Description</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n  </table>\r\n</div>", tunnel_pricing: "<div class=\"planOuter\">\r\n  <table class=\"planTable\">\r\n    <tr class=\"planRow\">\r\n      <th></th>\r\n      <th class=\"greenColor\">\r\n        <b>Basic</b><br/>\r\n        <div class=\"fs18\">(Tunnel Only)</div>\r\n      </th>\r\n      <th class=\"blueColor\">\r\n        <b>Standard</b><br/>\r\n        <div class=\"fs18\">(Tunnel Only)</div>\r\n      </th>\r\n      <th class=\"orangeColor\">\r\n        <b>Professional</b><br/>\r\n        <div class=\"fs18\">(Tunnel Only)</div>\r\n      </th>\r\n    </tr>\r\n    <tr class=\"priceRow\">\r\n      <td>\r\n        <div class=\"bold\" align=\"left\"> Subscription (billed on signup as Annual payment) </div>\r\n      </td>\r\n      <td> $ <span class=\"price\">197<span class=\"month\">/mth.</span> </span></td>\r\n      <td> $ <span class=\"price\">297<span class=\"month\">/mth.</span></span></td>\r\n      <td> $ <span class=\"price\">397<span class=\"month\">/mth.</span></span></td>\r\n    </tr>\r\n    <tr class=\"monthPriceRow\">\r\n      <td>\r\n        <div class=\"bold\" align=\"left\"> Subscription (billed on signup as Monthly payment) </div>\r\n      </td>\r\n      <td> $ <span class=\"price\">247<span class=\"month\">/mth.</span> </span></td>\r\n      <td> $ <span class=\"price\">347<span class=\"month\">/mth.</span></span></td>\r\n      <td> $ <span class=\"price\">447<span class=\"month\">/mth.</span></span></td>\r\n    </tr>\r\n  </table>\r\n  <table class=\"planDetails\" cellspacing=\"5\" cellpadding=\"5\">\r\n    <tr>\r\n      <td>Ideal for</td>\r\n      <td>Occasional/Casual Use</td>\r\n      <td>Everyday Quotation Use</td>\r\n      <td>Enterprise Quotation Use</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Max Number of (Simultaneous) Quotes per instance</td>\r\n      <td>10</td>\r\n      <td>10</td>\r\n      <td>100</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Easy, Drag‐and‐Drop Floor Plan Tool</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Usage Credit</td>\r\n      <td>25 Free Credits, One‐time</td>\r\n      <td>50 Free Per Mth.</td>\r\n      <td>100 Free per Mth.</td>\r\n    </tr>\r\n    <tr>\r\n      <td>8/7 Email Support</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>8/5 Phone Support</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Real-time Chat Support</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Long term Quote Data Storage</td>\r\n      <td>3 Mths.</td>\r\n      <td>6 Mths.</td>\r\n      <td>12 Mths.</td>\r\n    </tr>\r\n    <tr>\r\n      <td> Add your Company Logo</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Download Report as XLS or PDF</td>\r\n      <td>PDF Only</td>\r\n      <td>PDF Only</td>\r\n      <td>XLS and PDF</td>\r\n    </tr>\r\n    <tr>\r\n      <td>Upload Floorplans</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>B2B Portal Access</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Membership Site and Forum Access</td>\r\n      <td>Basic Members Area Only</td>\r\n      <td>Standard Members Area Only</td>\r\n      <td>Full Members Areas Access</td>\r\n    </tr>\r\n    <tr>\r\n      <td>DAS Installation Layout Feature</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Report Output Details</td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/cross.png\" alt=\"\"> <br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/cross.png\" alt=\"\"></div>\r\n      </td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/cross.png\" alt=\"\"></div>\r\n      </td>\r\n      <td>\r\n        <div  align=\"left\">■ Line Item Description  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Total Pricing per Report  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Line-item Pricing  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Material Qty  <img src=\"/assets/tic.png\" alt=\"\"><br>■ Part Numbers  <img src=\"/assets/tic.png\" alt=\"\"></div>\r\n      </td>\r\n    </tr>\r\n    <tr>\r\n      <td>Pricing Comparisons</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <!---\r\n      <tr>\r\n        <td>Potential Supplier List and Contact Info.</td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n        <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      </tr>\r\n      -->\r\n    <tr>\r\n      <td>XLS file upload for Batch Processing</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Online Help Documentation</td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n    <tr>\r\n      <td>Spare Parts Quote: BOM, Qty and Material Description</td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/cross.png\" alt=\"\"></td>\r\n      <td><img src=\"/assets/tic.png\" alt=\"\"></td>\r\n    </tr>\r\n  </table>\r\n</div>", pricing: "<div class=\"clearfix tac boldHD my-acc-edit\"> Choose Your Plan</div>\r\n<p class=\"mt20\">Great decision! You are probably here because you're seriously thinking of Upgrading your subscription, right? Well, here are a few tips to help you make the best possible decision. Note that there are two independent options available - Building and Tunnel. You may choose from either, or both, with a few limitations.  For example you could choose the Basic (Building Only) plan alone, or the Basic (Building Only) plan along with the Basic (Tunnel Only) plan, or vice versa. You could also choose the Basic (Building Only) plan along with the Standard (Tunnel Only) plan.<br>\r\n<br>\r\nNote that 'Downgrades' are not possible, except at anniversary dates on Annual Payment plans. in such a case, one possible scenario might be a change from Standard (Building Only) to Basic (Building Only). \r\n</p>", building_basic_plan_link: "https://ritmo.zaxaa.com/o/12597968730528/1", building_standard_plan_link: "https://ritmo.zaxaa.com/o/12597959431128/1", building_professional_plan_link: "https://ritmo.zaxaa.com/o/12597909231130/1", tunnel_basic_plan_link: "https://ritmo.zaxaa.com/o/12597968731191/1", tunnel_standard_plan_link: "https://ritmo.zaxaa.com/o/12597955131474/1", tunnel_professional_plan_link: "https://ritmo.zaxaa.com/o/12597939231475/1", welcome_page: "<div class=\"grayOuter padding40\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt padding40\">\r\n      <div class=\"clearfix tac boldHD my-acc-edit\"> Welcome !</div>\r\n      <p class=\"dash_para m-plan\">Welcome to iDAS Quick, home of the RSDAQ&trade; Engine, and thank you for joining us as a new subscriber! On this page we have <br>\r\nsummarized for you the critical information needed to navigate around the site and make full use of all the resources at your disposal.<br>\r\n        <span>Please take a few minutes now to view the video below.</span>\r\n      </p>\r\n      <iframe width=\"580\" height=\"320\" allowfullscreen=\"\" src=\"https://www.youtube.com/embed/6v2L2UGZJAM\" class=\"videoWelcome\"></iframe> \r\n    </div>\r\n  </div>\r\n</div>\r\n<div class=\"whiteBkg padding40\">\r\n  <div class=\"container\">\r\n    <div class=\"stepInnerContnt \">\r\n      <h2 class=\"clearfix tac boldHD my-acc\"> How to find what you need</h2>\r\n      <p class=\"dash_para m-plan\">The main resources available to you can be accessed from the Dashboard which serves as your 'Home' base. <br/>Help and Support are also directly accessible from this page. Click the button below.</p>\r\n      <div class=\"grayBoxOuter\">\r\n        <div class=\"grayBox\">\r\n          <span class=\"font22 color-lgreen\"> Dashboard</span>\r\n          From your Dashboard, Access many resources and areas of activity, including Projects and Project Archive, Your Account details, and more. This is the starting point for any new Project.\r\n          <a href=\"/\" data-remote=\"true\" class=\"btn_start\">Dashboard</a>\r\n        </div>\r\n        <div class=\"grayBox\">\r\n          <span class=\"font22 color-lgreen\"> Subscribe Now</span>\r\n          If you're already convinced of the extreme value offered by iDASQuick and the the RSDAQ™ Engine, and want to skip the trial period and Subscribe immediately, then click on the button below now.\r\n          <a href=\"/pricing\" data-remote=\"true\" class=\"btn_start\">Subscribe</a>\r\n        </div>\r\n        <div class=\"grayBox\">\r\n          <span class=\"font22 color-lgreen\"> Help/Support</span>\r\n          In-Depth Help and Support are provided at our Help Desk, accessible by clicking the button below. Context-based help is also available in the form of Pop-up Tool tips.<br/><br/>\r\n          <a href=\"https://idasquick.freshdesk.com\" target=\"_blank\" class=\"btn_start\">Help/Support</a>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>", verification_email: "<div>Congratulations, you have successfully registered for a personal account at our DAS Quote Engine site idasquick.net. You will receive an email containing your activation link shortly.</div>", confirmation_email: "<p>You can confirm your account email through the link below:</p>", reset_password_email: "<p>Someone has requested a link to change your password. If you didn't request this, please ignore this email.</p>\r\n<p>Your password won't change until you access the link above and create a new one.</p>\r\n<p>You can do this through the link below.</p>", unlock_email: "<p>Your account has been locked due to an excessive number of unsuccessful sign in attempts.</p>\r\n<p>Click the link below to unlock your account:</p>", contact_email: "info@idasquick.net", building_basic_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597901332315/1", building_standard_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597914632338/1", building_professional_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597993532350/1", tunnel_basic_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597978532316/1", tunnel_standard_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597924532339/1", tunnel_professional_plan_monthly_link: "https://ritmo.zaxaa.com/o/12597911732351/1", quick_start_footer: "Now that you've gone through the quick-start introductory training, you're ready to rock!<br>Click the button and let's get started!", data_storage_reminder_email: "<p>This is a courtesy reminder regarding your Project Files currently stored on the system at iDASQuick. Please be advised that there is one (1) week remaining for you to secure these files by removing them from the iDASQuick site. After this time all such files will be deleted.</p>\r\n<p>\r\n\tSincerely,<br/>\r\n\tAdmin, iDASQuick<br/>\r\n\t(Home of the <b>RSDAQ™ Engine</b>)\r\n</p>", unit_conversion_link: "<a href=\"http://www.digitaldutch.com/unitconverter/area.htm\" onclick=\"javascript:void window.open('http://www.digitaldutch.com/unitconverter/area.htm','1455721248479','width=700,height=500,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=0,top=0');return false;\" target=\"_blank\">Unit Conversion</a>"}
])
puts "Done"

puts "Creating Design Information Admin"
DesignInformationAdmin.create!([
  {distance_between_each_floor: 25.0, maximum_antennas_per_bda: 14}
])
puts "Done"

puts "Creating Communication Types"
CommunicationType.create!([
  {communication: "Simplex", communication_assigned: false, default_selected: false},
  {communication: "Duplex", communication_assigned: false, default_selected: true},
  {communication: "Auto Select", communication_assigned: true, default_selected: false}
])
puts "Done"

puts "Creating Product Price Lists"
ProductPriceList.create!([
  {report_material_list_id: 1, vendor: "vendor1", manufacturer: "manufacturer1", mfg_pn: "mfg_pn1", our_pn: "our_pn1", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2.37},
  {report_material_list_id: 2, vendor: "vendor2", manufacturer: "manufacturer2", mfg_pn: "mfg_pn2", our_pn: "our_pn2", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 75.5},
  {report_material_list_id: 3, vendor: "vendor3", manufacturer: "manufacturer3", mfg_pn: "mfg_pn3", our_pn: "our_pn3", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 95.9},
  {report_material_list_id: 4, vendor: "vendor4", manufacturer: "manufacturer4", mfg_pn: "mfg_pn4", our_pn: "our_pn4", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 244.53},
  {report_material_list_id: 5, vendor: "vendor5", manufacturer: "manufacturer5", mfg_pn: "mfg_pn5", our_pn: "our_pn5", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 235.52},
  {report_material_list_id: 6, vendor: "vendor6", manufacturer: "manufacturer6", mfg_pn: "mfg_pn6", our_pn: "our_pn6", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 850.0},
  {report_material_list_id: 7, vendor: "vendor7", manufacturer: "manufacturer7", mfg_pn: "mfg_pn7", our_pn: "our_pn7", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 350.0},
  {report_material_list_id: 8, vendor: "vendor8", manufacturer: "manufacturer8", mfg_pn: "mfg_pn8", our_pn: "our_pn8", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 3800.0},
  {report_material_list_id: 9, vendor: "vendor9", manufacturer: "manufacturer9", mfg_pn: "mfg_pn9", our_pn: "our_pn9", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 46.76},
  {report_material_list_id: 10, vendor: "vendor10", manufacturer: "manufacturer10", mfg_pn: "mfg_pn10", our_pn: "our_pn10", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.24},
  {report_material_list_id: 11, vendor: "vendor11", manufacturer: "manufacturer11", mfg_pn: "mfg_pn11", our_pn: "our_pn11", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 228.8},
  {report_material_list_id: 12, vendor: "vendor12", manufacturer: "manufacturer12", mfg_pn: "mfg_pn12", our_pn: "our_pn12", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 9790.29},
  {report_material_list_id: 13, vendor: "vendor13", manufacturer: "manufacturer13", mfg_pn: "mfg_pn13", our_pn: "our_pn13", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 14547.39},
  {report_material_list_id: 14, vendor: "vendor14", manufacturer: "manufacturer14", mfg_pn: "mfg_pn14", our_pn: "our_pn14", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 19305.0},
  {report_material_list_id: 15, vendor: "vendor15", manufacturer: "manufacturer15", mfg_pn: "mfg_pn15", our_pn: "our_pn15", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 14547.39},
  {report_material_list_id: 16, vendor: "vendor16", manufacturer: "manufacturer16", mfg_pn: "mfg_pn16", our_pn: "our_pn16", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2064.92},
  {report_material_list_id: 17, vendor: "vendor17", manufacturer: "manufacturer17", mfg_pn: "mfg_pn17", our_pn: "our_pn17", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 350.0},
  {report_material_list_id: 18, vendor: "vendor18", manufacturer: "manufacturer18", mfg_pn: "mfg_pn18", our_pn: "our_pn18", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 3800.0},
  {report_material_list_id: 21, vendor: "vendor21", manufacturer: "manufacturer21", mfg_pn: "mfg_pn21", our_pn: "our_pn21", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2819.96},
  {report_material_list_id: 19, vendor: "vendor19", manufacturer: "manufacturer19", mfg_pn: "mfg_pn19", our_pn: "our_pn19", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 5639.92},
  {report_material_list_id: 20, vendor: "vendor20", manufacturer: "manufacturer20", mfg_pn: "mfg_pn20", our_pn: "our_pn20", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 8000.0},
  {report_material_list_id: 22, vendor: "vendor22", manufacturer: "manufacturer22", mfg_pn: "mfg_pn22", our_pn: "our_pn22", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 18.0},
  {report_material_list_id: 23, vendor: "vendor23", manufacturer: "manufacturer23", mfg_pn: "mfg_pn23", our_pn: "our_pn23", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 0.5},
  {report_material_list_id: 24, vendor: "vendor24", manufacturer: "manufacturer24", mfg_pn: "mfg_pn24", our_pn: "our_pn24", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 227.87},
  {report_material_list_id: 25, vendor: "vendor25", manufacturer: "manufacturer25", mfg_pn: "mfg_pn25", our_pn: "our_pn25", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 73.86},
  {report_material_list_id: 26, vendor: "vendor26", manufacturer: "manufacturer26", mfg_pn: "mfg_pn26", our_pn: "our_pn26", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 2.83},
  {report_material_list_id: 27, vendor: "vendor27", manufacturer: "manufacturer27", mfg_pn: "mfg_pn27", our_pn: "our_pn27", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.24},
  {report_material_list_id: 28, vendor: "vendor28", manufacturer: "manufacturer28", mfg_pn: "mfg_pn28", our_pn: "our_pn28", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 107.25},
  {report_material_list_id: 29, vendor: "vendor29", manufacturer: "manufacturer29", mfg_pn: "mfg_pn29", our_pn: "our_pn29", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 28.25},
  {report_material_list_id: 30, vendor: "vendor30", manufacturer: "manufacturer30", mfg_pn: "mfg_pn30", our_pn: "our_pn30", description: nil, mfg_list_price: 1.0, our_cost: 1.0, discount_off_list: 1.0, our_sell_price: 190.0}
])
puts "Done"

puts "Creating Tunnel Passive Component Losses"
TunnelPassiveComponentLoss.create!([
  {jumper_loss: 0.5, connector_loss: 0.1, way2_splitter_loss: 3.5, way3_splitter_loss: 5.5, way4_splitter_loss: 6.5, way6_splitter_loss: 8.2, way8_splitter_loss: 9.5, directional_coupler_loss: 0.8, hybrid_coupler_loss: 3.5, quantity_of_splitters: 1, type_of_splitter: "way2_splitter_loss"}
])
puts "Done"

puts "Creating Fiber Material Quantities"
FiberMaterialQuantity.create!([
  {report_material_list_id: 19, description: nil, quantity: 1.0},
  {report_material_list_id: 20, description: nil, quantity: 0.0},
  {report_material_list_id: 21, description: nil, quantity: 1.0},
  {report_material_list_id: 22, description: nil, quantity: 1.0},
  {report_material_list_id: 23, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Donor Direct Feed Quantities"
DonorDirectFeedQuantity.create!([
  {report_material_list_id: 16, description: nil, quantity: 1.0},
  {report_material_list_id: 17, description: nil, quantity: 0.0},
  {report_material_list_id: 18, description: nil, quantity: 0.0},
  {report_material_list_id: 24, description: nil, quantity: 1.0},
  {report_material_list_id: 25, description: nil, quantity: 1.0},
  {report_material_list_id: 26, description: nil, quantity: 250.0},
  {report_material_list_id: 27, description: nil, quantity: 2.0},
  {report_material_list_id: 28, description: nil, quantity: 1.0},
  {report_material_list_id: 29, description: nil, quantity: 1.0},
  {report_material_list_id: 30, description: nil, quantity: 1.0}
])
puts "Done"

puts "Creating Tunnel Environments"
TunnelEnvironment.create!([
  {environment: "Coal Mine Shaft", tunnel_indoor_factor: 3.6},
  {environment: "Concrete Tunnel", tunnel_indoor_factor: 2.5},
  {environment: "Crawl Space (Small Tunnel)", tunnel_indoor_factor: 2.0},
  {environment: "DAM Tunnel", tunnel_indoor_factor: 2.6},
  {environment: "Densely Cluttered Tunnel", tunnel_indoor_factor: 3.9},
  {environment: "Emergency Escape Tunnel", tunnel_indoor_factor: 2.2},
  {environment: "Free Space (No Tunnel Clutter)", tunnel_indoor_factor: 1.7},
  {environment: "Highway Underpass", tunnel_indoor_factor: 2.5},
  {environment: "Limestone or Other Type of Rock Tunnel", tunnel_indoor_factor: 2.8},
  {environment: "Medium Cluttered Tunnel", tunnel_indoor_factor: 2.8},
  {environment: "Other Mine Shaft", tunnel_indoor_factor: 2.6},
  {environment: "Other Utility / Public Works T unnels (Gas,Water,Electricity)", tunnel_indoor_factor: 3.8},
  {environment: "Pedestrian Tunnel (Under Building or City)", tunnel_indoor_factor: 2.3},
  {environment: "Service Tunnel (With Metal Piping or Other Metal Clutter)", tunnel_indoor_factor: 3.8},
  {environment: "Service Tunnel (Without Metal Piping or Other Metal Clutter)", tunnel_indoor_factor: 3.3},
  {environment: "Sewage Service Tunnels ", tunnel_indoor_factor: 2.0},
  {environment: "Train Tunnel (Terrain Bore-Concrete)", tunnel_indoor_factor: 2.8},
  {environment: "Train Tunnel (Underground Transit or Subway)", tunnel_indoor_factor: 3.5},
  {environment: "Tunnel With Metal-Clad Interior ", tunnel_indoor_factor: 2.4},
  {environment: "Vehicle Tunnel ", tunnel_indoor_factor: 2.5},
  {environment: "Under Lake or Under Sea Tunnel", tunnel_indoor_factor: 2.7}
])
puts "Done"

puts "Creating Manage Credits"
ManageCredit.create!([
  {default_credits: 10, credits_required: 1}
])
puts "Done"

puts "Creating Credits"
Credit.create!([
  {credits: 1000, amount: 497.0, product_number: "12597986431136"},
  {credits: 500, amount: 350.0, product_number: "12597914331134"},
  {credits: 200, amount: 160.0, product_number: "12597902631133"},
  {credits: 100, amount: 100.0, product_number: "12597973231132"},
  {credits: 50, amount: 54.0, product_number: "12597900231327"},
  {credits: 10, amount: 12.0, product_number: "12597979231326"}
])
puts "Done"

puts "Creating Equalization Multiplier"
EqualizationMultiplier.create!([
  {multiplied_factor: 1.2}
])
puts "Done"

puts "Creating Trial Period"
TrialPeriod.create!([
  {period: 7}
])
puts "Done"

puts "Creating Plan"
Plan.create!([
  {name: "Basic (Building Only)", amount: 2364.0, credits: 25, usage_credits_one_time: true, product_number: "12597968730528", yearly: true, quote_data_storage: 3},
  {name: "Standard (Building Only)", amount: 3564.0, credits: 50, usage_credits_one_time: false, product_number: "12597959431128", yearly: true, quote_data_storage: 6},
  {name: "Professional (Building Only)", amount: 4764.0, credits: 100, usage_credits_one_time: false, product_number: "12597909231130", yearly: true, quote_data_storage: 12},
  {name: "Basic (Tunnel Only)", amount: 2364.0, credits: 25, usage_credits_one_time: true, product_number: "12597968731191", yearly: true, quote_data_storage: 3},
  {name: "Standard (Tunnel Only)", amount: 3564.0, credits: 50, usage_credits_one_time: false, product_number: "12597955131474", yearly: true, quote_data_storage: 6},
  {name: "Professional (Tunnel Only)", amount: 4764.0, credits: 100, usage_credits_one_time: false, product_number: "12597939231475", yearly: true, quote_data_storage: 12},
  {name: "Basic (Building Only)", amount: 247.0, credits: 25, usage_credits_one_time: true, product_number: "12597901332315", yearly: false, quote_data_storage: 3},
  {name: "Standard (Building Only)", amount: 347.0, credits: 50, usage_credits_one_time: false, product_number: "12597914632338", yearly: false, quote_data_storage: 6},
  {name: "Professional (Building Only)", amount: 447.0, credits: 100, usage_credits_one_time: false, product_number: "12597993532350", yearly: false, quote_data_storage: 12},
  {name: "Basic (Tunnel Only)", amount: 247.0, credits: 25, usage_credits_one_time: true, product_number: "12597978532316", yearly: false, quote_data_storage: 3},
  {name: "Standard (Tunnel Only)", amount: 347.0, credits: 50, usage_credits_one_time: false, product_number: "12597924532339", yearly: false, quote_data_storage: 6},
  {name: "Professional (Tunnel Only)", amount: 447.0, credits: 100, usage_credits_one_time: false, product_number: "12597911732351", yearly: false, quote_data_storage: 12}
])
puts "Done"