namespace :check_long_term do
	desc "Check Long Term Data Storage Based on Subscription (Basic, Standard, Professional)"
	task data_storage: :environment do
		begin
			static_page = StaticPage.last
			users = User.where(active: true)
			users.each do |user|
				building_plan = user.building_plan
				tunnel_plan = user.tunnel_plan
				if building_plan.present?
					if (Date.today == (user.building_quote_storage_date + building_plan.quote_data_storage.months - 7.days).to_date)
						ProjectMailer.delay.storage_reminder(user, "Building", static_page)
					end
					if (Date.today == (user.building_quote_storage_date + building_plan.quote_data_storage.months).to_date)
						projects = Project.where("DATE(created_at) <= ?", (Date.today - building_plan.quote_data_storage.months).to_date).destroy_all
						user.update(building_quote_storage_date: Time.now)
					end
				end
				if tunnel_plan.present?
					if (Date.today == (user.tunnel_quote_storage_date + tunnel_plan.quote_data_storage.months - 7.days).to_date)
						ProjectMailer.delay.storage_reminder(user, "Tunnel", static_page)
					end
					if (Date.today == (user.tunnel_quote_storage_date + tunnel_plan.quote_data_storage.months).to_date)
						projects = TunnelProject.where("DATE(created_at) <= ?", (Date.today - tunnel_plan.quote_data_storage.months).to_date).destroy_all
						user.update(tunnel_quote_storage_date: Time.now)
					end
				end
			end
		rescue => e
			Rails.logger.info "Check Long Term Data Storage: #{e}"
		end
	end
end