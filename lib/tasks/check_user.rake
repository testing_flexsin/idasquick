namespace :check_user do
	desc "Add Credits in User Account Based on Subscription (Basic, Standard, Professional) and Check User Subscription of 1 year"
	task credits_and_subscription: :environment do
		begin
			users = User.where(active: true)
			users.each do |user|
				building_plan = user.building_plan
				tunnel_plan = user.tunnel_plan
				if building_plan.yearly? && user.building_last_transaction_date.present? && (Date.today > (user.building_last_transaction_date + 1.year).to_date)
					user.update(building_plan_id: nil, building_last_transaction_date: nil, building_last_credits_date: nil)
				elsif !building_plan.yearly? && user.building_last_transaction_date.present? && (Date.today > (user.building_last_transaction_date + 1.month).to_date)
					user.update(building_plan_id: nil, building_last_transaction_date: nil, building_last_credits_date: nil)
				end
				if tunnel_plan.yearly? && user.tunnel_last_transaction_date.present? && (Date.today > (user.tunnel_last_transaction_date + 1.year).to_date)
					user.update(tunnel_plan_id: nil, tunnel_last_transaction_date: nil, tunnel_last_credits_date: nil)
				elsif !tunnel_plan.yearly? && user.tunnel_last_transaction_date.present? && (Date.today > (user.tunnel_last_transaction_date + 1.month).to_date)
					user.update(tunnel_plan_id: nil, tunnel_last_transaction_date: nil, tunnel_last_credits_date: nil)
				end
				unless building_plan.present? && tunnel_plan.present?
					user.update(active: false)
				end
				if user.active?
					if building_plan.present? && !building_plan.usage_credits_one_time? && building_plan.yearly? && user.building_last_credits_date.present? && (Date.today == (user.building_last_credits_date + 1.month).to_date)
						user.update(building_last_credits_date: Time.now, credits: user.credits + building_plan.credits)
					end
					if tunnel_plan.present? && !tunnel_plan.usage_credits_one_time? && tunnel_plan.yearly? && user.tunnel_last_credits_date.present? && (Date.today == (user.tunnel_last_credits_date + 1.month).to_date)
						user.update(tunnel_last_credits_date: Time.now, credits: user.credits + tunnel_plan.credits)
					end
				end
			end
		rescue => e
			Rails.logger.info "Add Credits and Check User Subscription: #{e}"
		end
	end
end