require 'rails_helper'

describe ProjectsController do
  describe 'POST #create' do
    context 'json' do
      context 'with valid attributes' do
        it 'creates the project' do
          post :create, project: attributes_for(:project), format: :json
          expect(Project.count).to eq(1)
        end

        it 'responds with 201' do
          post :create, project: attributes_for(:project), format: :json
          expect(response).to have_http_status(201)
        end
      end

      context 'with invalid attributes' do
        it 'does not create the project' do
          post :create, project: attributes_for(:project, user_id: nil), format: :json
          expect(Project.count).to eq(0)
        end

        it 'responds with 422' do
          post :create, project: attributes_for(:project, user_id: nil), format: :json
          expect(response).to have_http_status(422)
        end
      end
    end
  end
end