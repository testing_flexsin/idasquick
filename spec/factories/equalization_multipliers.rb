FactoryGirl.define do
  factory :equalization_multiplier do
    multiplied_factor 1.5
  end

end
