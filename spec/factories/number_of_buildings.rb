FactoryGirl.define do
  factory :number_of_building do
    buildings   { FFaker::Address.building_number }
  end
end